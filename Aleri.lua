
--[==[

WELCOME TO ALERI

]==]--


local _, Aleri = ...

--main addon tables
--Aleri = {}
--Aleri.PrintColour = '|cff00FF96' 

Aleri.Merchant = {}
Aleri.Mail = {}
Aleri.Chat = {}
Aleri.DataTables = {}
Aleri.Guild = {}
Aleri.Guild.Members = {}
Aleri.UI = {}
Aleri.UI.MemberListPop = {}
Aleri.UI.ProfessionFrame = {}
--these should be condensed into a db table rather than arr tables
Aleri.Guild.ClassCountMaxLevel = { Total = 0, WARRIOR = 0, PALADIN = 0, PRIEST = 0, HUNTER = 0, MAGE = 0, WARLOCK = 0, DEATHKNIGHT = 0, SHAMAN = 0, DRUID = 0, ROGUE = 0 }
Aleri.Guild.ClassCountNorthrend = { Total = 0, WARRIOR = 0, PALADIN = 0, PRIEST = 0, HUNTER = 0, MAGE = 0, WARLOCK = 0, DEATHKNIGHT = 0, SHAMAN = 0, DRUID = 0, ROGUE = 0 }
Aleri.Guild.ClassCountOutland = { Total = 0, WARRIOR = 0, PALADIN = 0, PRIEST = 0, HUNTER = 0, MAGE = 0, WARLOCK = 0, DEATHKNIGHT = 0, SHAMAN = 0, DRUID = 0, ROGUE = 0 }
Aleri.Guild.ClassCountVanilla = { Total = 0, WARRIOR = 0, PALADIN = 0, PRIEST = 0, HUNTER = 0, MAGE = 0, WARLOCK = 0, DEATHKNIGHT = 0, SHAMAN = 0, DRUID = 0, ROGUE = 0 }
Aleri.Guild.ClassCountEveryone = { Total = 0, WARRIOR = 0, PALADIN = 0, PRIEST = 0, HUNTER = 0, MAGE = 0, WARLOCK = 0, DEATHKNIGHT = 0, SHAMAN = 0, DRUID = 0, ROGUE = 0 }
Aleri.Guild.ShowDataRange = 'max'
Aleri.Guild.RaidStatsSort = 'MainSpecGearScore'
Aleri.Guild.CurrentEditingMemberName = nil
Aleri.Guild.ListenForBroadcastUpdates = true
Aleri.ThisPlayer = {}
Aleri.ThisPlayer.RankIndex = 99

Aleri.Class = {}
Aleri.Class.IDs = { 'DEATHKNIGHT', 'WARRIOR', 'DRUID', 'PALADIN', 'PRIEST', 'SHAMAN', 'HUNTER', 'MAGE', 'ROGUE', 'WARLOCK', 'Total' }
Aleri.Class.Colours = {
	DEATHKNIGHT = { r = 0.77, g = 0.12, b = 0.23 },
	DRUID = { r = 1.00, g = 0.49, b = 0.04 },
	HUNTER = { r = 0.67, g = 0.83, b = 0.45 },
	MAGE = { r = 0.25, g = 0.78, b = 0.92 },
	PALADIN = { r = 0.96, g = 0.55, b = 0.73 },
	PRIEST = { r = 1.00, g = 1.00, b = 1.00 },
	ROGUE = { r = 1.00, g = 0.96, b = 0.41 },
	SHAMAN = { r = 0.00, g = 0.44, b = 0.87 },
	WARLOCK = { r = 0.53, g = 0.53, b =	0.93 },
	WARRIOR = { r = 0.78, g = 0.61, b = 0.43 },
	Total = { r = 1, g = 1, b = 1 }
}
Aleri.Class.HealerIDs = { 'PRIEST', 'DRUID', 'SHAMAN', 'PALADIN', 'Total' }
Aleri.Class.TankIDs = { 'DEATHKNIGHT', 'WARRIOR', 'DRUID', 'PALADIN', 'Total' }
Aleri.Class.MeleeDpsIDs = { 'DEATHKNIGHT', 'WARRIOR', 'DRUID', 'PALADIN', 'SHAMAN', 'ROGUE', 'Total' }
Aleri.Class.RangeDpsIDs = { 'PRIEST', 'DRUID', 'SHAMAN', 'HUNTER', 'MAGE', 'WARLOCK', 'Total' }
Aleri.Class.SpecRoles = {
	DRUID = { boom = 'Range', resto = 'Healer', feral = 'Melee', bear = 'Tank' },
	SHAMAN = { ele = 'Range', enh = 'Melee', resto = 'Healer' },
	HUNTER = { mm = 'Range', bm = 'Range', surv = 'Range' },
	PALADIN = { holy = 'Healer', prot = 'Tank', ret = 'Melee' },
	WARRIOR = { arms = 'Melee', fury = 'Melee', prot = 'Tank' },
	ROGUE = { ass = 'Melee', comb = 'Melee', subt = 'Melee' },
	PRIEST = { holy = 'Healer', disc = 'Healer', shad = 'Range' },
	WARLOCK = { demo = 'Range', aff = 'Range', dest = 'Range' },
	MAGE = { frost = 'Range', fire = 'Range', arc = 'Range' },
	DEATHKNIGHT = { frost = 'Tank', blood = 'Tank', unholy = 'Melee' },
}
Aleri.Class.SpecToDisplayText = {
	DRUID = { boom = 'Boomkin', resto = 'Restoration', feral = 'Feral Cat', bear = 'Feral Bear', unknown = 'Unknown', pvp = 'PvP' },
	SHAMAN = { ele = 'Elemental', enh = 'Enhance', resto = 'Restoration', unknown = 'Unknown', pvp = 'PvP' },
	HUNTER = { mm = 'Marksmanship', bm = 'Beast Master', surv = 'Survival', unknown = 'Unknown', pvp = 'PvP' },
	PALADIN = { holy = 'Holy', prot = 'Protection', ret = 'Retribution', unknown = 'Unknown', pvp = 'PvP' },
	WARRIOR = { arms = 'Arms', fury = 'Fury', prot = 'Protection', unknown = 'Unknown', pvp = 'PvP' },
	ROGUE = { ass = 'Assassination', comb = 'Combat', subt = 'Subtlety', unknown = 'Unknown', pvp = 'PvP' },
	PRIEST = { holy = 'Holy', disc = 'Discipline', shad = 'Shadow', unknown = 'Unknown', pvp = 'PvP' },
	WARLOCK = { demo = 'Demonology', aff = 'Affliction', dest = 'Destruction', unknown = 'Unknown', pvp = 'PvP' },
	MAGE = { frost = 'Frost', fire = 'Fire', arc = 'Arcane', unknown = 'Unknown', pvp = 'PvP' },
	DEATHKNIGHT = { frost = 'Frost', blood = 'Blood', unholy = 'Unholy', unknown = 'Unknown', pvp = 'PvP' },
}

--used to find incorrectly entered info in guild member notes
Aleri.GearScoreCheck = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.' }
Aleri.LetterCheck = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }

--main frame to register events
Aleri.MainFrame = CreateFrame('FRAME', 'AleriMainFrame', UIParent)
Aleri.MainFrame:RegisterEvent("CHAT_MSG_GUILD")
Aleri.MainFrame:RegisterEvent("CHAT_MSG_WHISPER")
Aleri.MainFrame:RegisterEvent("ADDON_LOADED")
Aleri.MainFrame:RegisterEvent("GUILD_ROSTER_UPDATE")
Aleri.MainFrame:RegisterEvent("CHAT_MSG_ADDON")
Aleri.MainFrame:RegisterEvent("CHAT_MSG_GUILD")
Aleri.MainFrame:RegisterEvent("CHAT_MSG_SKILL")
Aleri.MainFrame:RegisterEvent("GET_ITEM_INFO_RECEIVED")
Aleri.MainFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
Aleri.MainFrame:RegisterEvent("PLAYER_LEAVING_WORLD")

-------------------------------------------------------------------------------------------------------
-- Helper tools
-------------------------------------------------------------------------------------------------------


function Aleri.MakeFrameMove(frame)
	frame:SetMovable(true)
	frame:EnableMouse(true)
	frame:RegisterForDrag("LeftButton")
	frame:SetScript("OnDragStart", frame.StartMoving)
	frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
end

function Aleri.LockFramePos(frame)
	frame:SetMovable(false)
end
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- Player functions
-------------------------------------------------------------------------------------------------------
function Aleri.PrintPlayerXP()
	local xp = UnitXP('player')
	local xpmx = UnitXPMax('player')
	local xpr = GetXPExhaustion()
	if xpr == nil then xpr = 0 end
	return ('This Level: '..string.format("%.2f", (xp/xpmx) * 100)..'%'..' XP To Ding: '..tonumber(xpmx-xp)..' Rested XP: '..tonumber(xpr))
end

function Aleri.PrintGuildPlayerInfo(guid)
	local class, _, race, _, gender, name, _ = GetPlayerInfoByGUID(guid)
	if gender == 2 then gender = 'male' else gender = 'female' end
	for i = 1, GetNumGuildMembers() do
		if select(1, GetGuildRosterInfo(i)) == name then
			local level = select(4, GetGuildRosterInfo(i))
			print(name, '>', race, '>', class, '>', gender, '>', level)
		end
	end
end
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
-- Update class role summary panel
-------------------------------------------------------------------------------------------------------



Aleri.Guild.AddonChatMessage = ''

-- this will loop through the players local database and send an addon message for each player which contains data
function Aleri.Guild.BroadcastDatabase()
	SendAddonMessage('aleri-update-S', UnitName('player'), 'GUILD')
	for k, member in ipairs(AleriCharacterSettings.GuildDatabase) do
		Aleri.Guild.AddonChatMessage = tostring(member.Name..':'..member.Class..':'..member.Level..':'..member.MainSpec..':'..member.MainSpecGearScore..':'..member.OffSpec..':'..member.OffSpecGearScore..':'..member.PrimaryProf..':'..member.PrimaryProfLevel..':'..member.SecondaryProf..':'..member.SecondaryProfLevel..':'..member.DKP..':'..member.Race..':'..member.Gender)
		SendAddonMessage('aleri-update', Aleri.Guild.AddonChatMessage, 'GUILD')
	end
	SendAddonMessage('aleri-update-E', 'Aleri-broadcast-completed', 'GUILD')
end



Aleri.Guild.BroadcastMembersUpdated = 0
Aleri.Guild.BroadcastMembersAdded = 0
function Aleri.Guild.UpdateDatabaseFromBroadcast(info)
	local i = 1
	local data_sent = {}
	for data in string.gmatch(info, '[^:]+') do
		data_sent[i] = data
		i = i + 1
	end
	
	local player_exist = false

	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		if player.Name == data_sent[1] then -- and player.Class == data_sent[2] then
			
			if tonumber(player.Level) < tonumber(data_sent[3]) then player.Level = tonumber(data_sent[3]) end
			
			player.MainSpec = data_sent[4]
			if tonumber(player.MainSpecGearScore) < tonumber(data_sent[5]) then player.MainSpecGearScore = tonumber(data_sent[5]) end
			--player.MainSpecGearScore = tonumber(data_sent[5])
			
			player.OffSpec = data_sent[6]
			if tonumber(player.OffSpecGearScore) < tonumber(data_sent[7]) then player.OffSpecGearScore = tonumber(data_sent[7]) end
			--player.OffSpecGearScore = tonumber(data_sent[7])
			
			player.PrimaryProf = data_sent[8]
			if tonumber(player.PrimaryProfLevel) < tonumber(data_sent[9]) then player.PrimaryProfLevel = tonumber(data_sent[9]) end
			
			player.SecondaryProf = data_sent[10]
			if tonumber(player.SecondaryProfLevel) < tonumber(data_sent[11]) then player.SecondaryProfLevel = tonumber(data_sent[11]) end
			
			player.DKP = tonumber(data_sent[12])
			player.Race = data_sent[13]
			player.Gender = data_sent[14]
			
			player_exist = true

			Aleri.Guild.BroadcastMembersUpdated = Aleri.Guild.BroadcastMembersUpdated + 1
		end
	end
	if player_exist == false then
		table.insert(AleriCharacterSettings.GuildDatabase, { Name = data_sent[1], Class = data_sent[2], Level = tonumber(data_sent[3]), MainSpec = data_sent[4], MainSpecGearScore = tonumber(data_sent[5]), OffSpec = data_sent[6], OffSpecGearScore = tonumber(data_sent[7]), PrimaryProf = data_sent[8], PrimaryProfLevel = tonumber(data_sent[9]), SecondaryProf = data_sent[10], SecondaryProfLevel = tonumber(data_sent[11]), DKP = tonumber(data_sent[12]), Race = data_sent[13], Gender = data_sent[14] } )
		--print(Aleri.PrintColour..'Aleri - '..data_sent[1]..' has been added to local database')
		Aleri.Guild.BroadcastMembersAdded = Aleri.Guild.BroadcastMembersAdded + 1
	end
	
	-- print(Aleri.PrintColour..'Aleri - '..members_updated..' crazed owlkins updated')
	-- print(Aleri.PrintColour..'Aleri - '..members_added..' wild boars added')
	
end

-------------------------------------------------------------------------------------------------------
-- Main guild frame update function - need to restructure this and move some parts into their own functions
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------


function Aleri.UpdateEditFrame(player)
	for k, p in ipairs(AleriCharacterSettings.GuildDatabase) do
		if p.Name == player then
			UIDropDownMenu_SetText(Aleri.Guild.MemberDetailFrame.MainSpecDropDown, p.MainSpec)
			UIDropDownMenu_SetText(Aleri.Guild.MemberDetailFrame.OffSpecDropDown, p.OffSpec)
			Aleri.Guild.CurrentEditingMemberMainSpec = tonumber(p.MainSpecGearScore)
			Aleri.Guild.MemberDetailFrame.MainSpecGearScore:SetText(string.format('%.1f', p.MainSpecGearScore))
			Aleri.Guild.CurrentEditingMemberOffSpec = tonumber(p.OffSpecGearScore)
			Aleri.Guild.MemberDetailFrame.OffSpecGearScore:SetText(string.format('%.1f', p.OffSpecGearScore))
		end
	end
end

Aleri.ScanGuildRosterButton = CreateFrame('BUTTON', 'AleriScanGuildRosterButton', GuildFrame, 'UIPanelButtonTemplate')
Aleri.ScanGuildRosterButton:SetPoint('TOPLEFT', 70, -40)
Aleri.ScanGuildRosterButton:SetSize(50, 22)
Aleri.ScanGuildRosterButton:SetText('Scan')
Aleri.ScanGuildRosterButton:SetScript('OnClick', function()
	if GuildFrameLFGButton:GetChecked() == 1 then
		Aleri.Guild.ScanGuildRoster()
	else
		StaticPopup_Show ('ScanGuildRosterDialog')
	end
end)


function Aleri.Guild.ScanGuildRoster()

	local player_missing, missing_count = true, 0
	local player_remove, remove_count = true, 0
	local temp_roster = {}
	local db_count_after, temp_roster_count = 0, 0
	local db_count_before = #AleriCharacterSettings.GuildDatabase
	local TotalMembers, OnlineMemebrs, _ = GetNumGuildMembers()

	--loop members
	for i = 1, TotalMembers do
		player_missing = true
		local name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)

		local alt, net, tot, hrs = '-', 0, 0, 0
		if officerNote ~= nil then
			local i = 1
			for data in string.gmatch(officerNote, "%S+") do
				if i == 1 then
					if string.find(data, 'Net:') then
						net = string.sub(data, 5)
					else
						alt = data --if the first string in officer note doesn't match 'Net:' then its assumed to be an alt so we set the alt/main here
					end
				end
				if i == 2 then --tot = string.sub(data, 5)
					if string.find(data, 'Tot:') then
						tot = string.sub(data, 5)
					end
				end
				if i == 3 then --hrs = string.sub(data, 5)
					if string.find(data, 'Hrs:') then
						hrs = string.sub(data, 5)
					end
				end
				i = i + 1
			end
		end
		table.insert(temp_roster, { Name = name, Class = class } )
		temp_roster_count = temp_roster_count + 1
		for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
			if name == player.Name and class == player.Class then
				--check dkp update permissions
				if select(3, GetGuildInfo('player')) < 3 then
					player.DKP = tonumber(tot)
				end
				player.Level = level
				player_missing = false
			end
		end
		if player_missing == true then
			table.insert(AleriCharacterSettings.GuildDatabase, { Name = name, Level = tonumber(level), Class = class, Race = nil, CurrentSpec = 'z', ilvl = 0, Alt = alt, MainSpec = 'Unknown', SecondarySpec = 'Unknown', PrimaryProf = 'z', PrimaryProfLevel = tonumber(0), SecondaryProf = 'z', SecondaryProfLevel = tonumber(0), AchievementPoints = tonumber(0) } )
			--print(Aleri.PrintColour..'Aleri - '..name..' has been added to local database')
			missing_count = missing_count + 1
		end
	end
	
	
	for k, member in ipairs(AleriCharacterSettings.GuildDatabase) do
		player_remove = true
		
		for k, v in ipairs(temp_roster) do
			if v.Name == member.Name and v.Class == member.Class then
				player_remove = false
			end
		end
		
		if player_remove == true then
			table.remove(AleriCharacterSettings.GuildDatabase, k)
			--print(Aleri.PrintColour..'Aleri - '..member.Name..' has been removed from local database')
			remove_count = remove_count + 1
		end
	end
	
	db_count_after = #AleriCharacterSettings.GuildDatabase
	
	print(Aleri.PrintColour..'Aleri - local database count before scan '..db_count_before)
	print(Aleri.PrintColour..'Aleri - local database count after scan '..db_count_after)
	print(Aleri.PrintColour..'Aleri - scanned '..temp_roster_count..' guild members')
	print(Aleri.PrintColour..'Aleri - '..remove_count..' worthless souls exiled from local database')	
	print(Aleri.PrintColour..'Aleri - '..missing_count..' lazy peons summoned to local database')
	
	--Aleri.Guild.CheckDatabase()
	temp_roster = nil
		
end



function Aleri.GetMyInfo()
	local data = {}
	for skillIndex = 1, GetNumSkillLines() do
		local prof, _, _, level, _, _, _, _, _, _, _, _, _ = GetSkillLineInfo(skillIndex)
		for k, v in ipairs(Aleri.Database.Professions) do
			if v.Name == prof then
				table.insert(data, { Prof = prof, Level = level } )
				--print('insert temp prof table', prof, level)
			end
		end
	end
	
	local myName = UnitName('player')
	--local myClass = UnitClass('player')
	
	if data ~= nil then
		--print('data has data')
		for k, v in ipairs(data) do
			--print('data', k, v.Prof)
			for i, player in ipairs(AleriCharacterSettings.GuildDatabase) do
				if player.Name == UnitName('player') then
					if player.PrimaryProf == z then
						--print('player pri prof = - updating to ', v.Prof)
						player.PrimaryProf = v.Prof
						player.PrimaryProfLevel = v.Level
					elseif player.PrimaryProf == v.Prof then
						--print('player prof found', player.PrimaryProf, v.Prof)
						player.PrimaryProfLevel = v.Level
					elseif player.SecondaryProf == 'z' then
						--print('player sec prof = - updating to ', v.Prof)
						player.SecondaryProf = v.Prof
						player.SecondaryProfLevel = v.Level
					elseif player.SecondaryProf == v.Prof then
						--print('player prof found', player.SecondaryProf, v.Prof)
						player.SecondaryProfLevel = v.Level
					else
						--print('error')
					end
				end
			end
		end
	end
	
end


function Aleri.SetAleriRankLimits()
	_, Aleri.ThisPlayer.RankName, Aleri.ThisPlayer.RankIndex = GetGuildInfo('player')
	print(Aleri.PrintColour..'Aleri - setting permissions for '..UnitName('player')..' with the rank of '..Aleri.ThisPlayer.RankName)
	if Aleri.ThisPlayer.RankIndex > 2 then
		Aleri.Guild.PlayerFrame.SettingsFrame.BroadcastDatabaseButton:Disable()
		Aleri.Guild.PlayerFrame.SettingsFrame.ListenForUpdatesCheckbox:Disable()
		Aleri.Guild.PlayerFrame.SettingsFrame.SendUpdatesCheckbox:Disable()
	end
end


-------------------------------------------------------------------------------------------------------
-- wait function - FOUND ONLINE AND WORK ONCE BUT NEVER SINCE WILL KEEP ON RESEARCHING IT
-------------------------------------------------------------------------------------------------------
local waitTable = {}
local waitFrame = nil

function Aleri.Wait(delay, func, ...)
  if(type(delay) ~= "number" or type(func) ~= "function") then
    return false
  end
  if not waitFrame then
    waitFrame = CreateFrame("Frame", nil, UIParent)
    waitFrame:SetScript("OnUpdate", function (self, elapse)
      for i = 1, #waitTable do
        local waitRecord = tremove(waitTable, i)
        local d = tremove(waitRecord, 1)
        local f = tremove(waitRecord, 1)
        local p = tremove(waitRecord, 1)
        if d > elapse then
          tinsert(waitTable, i, {d - elapse, f, p})
          i = i + 1
        else
          count = count - 1
          f(unpack(p))
        end
      end
    end)
  end
  tinsert(waitTable, {delay, func, {...}})
  return true
end
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- Guild Info Frame - MAIN GUILD FRAME
-------------------------------------------------------------------------------------------------------
Aleri.Guild.PlayerFrame = CreateFrame('FRAME', 'AleriGuildPlayerFrame', GuildFrame)
Aleri.Guild.PlayerFrame:SetPoint("TOPLEFT", 350, -12)
Aleri.Guild.PlayerFrame:SetBackdrop({ --bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
                                            edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
                                            tile = true, tileSize = 16, edgeSize = 20, 
                                            insets = { left = 4, right = 4, top = 4, bottom = 4 }});
--Aleri.Guild.PlayerFrame:SetBackdropColor(0,0,0,1)
Aleri.Guild.PlayerFrame:SetSize(800, 450)
Aleri.Guild.PlayerFrame.Texture = Aleri.Guild.PlayerFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.Guild.PlayerFrame.Texture:SetAllPoints(Aleri.Guild.PlayerFrame)
Aleri.Guild.PlayerFrame.Texture:SetTexture(0,0,0,1)
Aleri.Guild.PlayerFrame.Texture:SetPoint('TOPLEFT', 4, -4)
Aleri.Guild.PlayerFrame.Texture:SetPoint('BOTTOMRIGHT', -4, 4)

Aleri.Guild.ClassStatsFrame = CreateFrame('FRAME', 'AleriGuildClassStatsFrame', Aleri.Guild.PlayerFrame)
Aleri.Guild.ClassStatsFrame:SetPoint('TOPLEFT', 0, 0)
Aleri.Guild.ClassStatsFrame:SetSize(400, 320)

Aleri.Guild.ClassStatsFrame.Title = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTitle', 'OVERLAY', 'GameFontNormal')
Aleri.Guild.ClassStatsFrame.Title:SetPoint('TOPLEFT', 20, -20)
Aleri.Guild.ClassStatsFrame.Title:SetText('Summary of Class counts')
Aleri.Guild.ClassStatsFrame.Title:SetTextColor(1,1,1)
Aleri.Guild.ClassStatsFrame.Title:SetFont("Fonts\\FRIZQT__.TTF", 18)

Aleri.Guild.PlayerFrameFontStrings_ColumnOne = {}
Aleri.Guild.PlayerFrameFontStrings_ColumnTwo = {}
Aleri.Guild.PlayerFrameFontStrings_ColumnThree = {}
Aleri.Guild.PlayerFrameClassStatusBars = {}

Aleri.UI.ClassSummaryFontSize = 12
Aleri.UI.ClassSummaryStatusBarWidth = 100
Aleri.UI.ClassSummaryStatusBarHeight = 18
Aleri.UI.ClassSummaryVerticalOffset = 175
Aleri.UI.ClassSummaryRowHeight = -20

for k, v in ipairs(Aleri.Class.IDs) do
	Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k] = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameFontString_'..k, 'OVERLAY', 'GameFontNormal')
	Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetPoint('TOPLEFT', 30, ((k-1) * Aleri.UI.ClassSummaryRowHeight) - Aleri.UI.ClassSummaryVerticalOffset)
	Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetText(v)
	Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.ClassSummaryFontSize)
	Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetTextColor(Aleri.Class.Colours[v].r, Aleri.Class.Colours[v].g, Aleri.Class.Colours[v].b)
	
	Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k] = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameFontString_'..k, 'OVERLAY', 'GameFontNormal')
	Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetPoint('TOPLEFT', 150, ((k-1) * Aleri.UI.ClassSummaryRowHeight) - Aleri.UI.ClassSummaryVerticalOffset)
	--Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetText(v)
	Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.ClassSummaryFontSize)
	Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetTextColor(Aleri.Class.Colours[v].r, Aleri.Class.Colours[v].g, Aleri.Class.Colours[v].b)
	
	Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k] = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameFontString_'..k, 'OVERLAY', 'GameFontNormal')
	Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetPoint('TOPLEFT', 190, ((k-1) * Aleri.UI.ClassSummaryRowHeight) - Aleri.UI.ClassSummaryVerticalOffset)
	--Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetText(v)
	Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.ClassSummaryFontSize)
	Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetTextColor(Aleri.Class.Colours[v].r, Aleri.Class.Colours[v].g, Aleri.Class.Colours[v].b)
	
	Aleri.Guild.PlayerFrameClassStatusBars[k] = CreateFrame("StatusBar", k..'_StatusBar', Aleri.Guild.ClassStatsFrame)
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
	Aleri.Guild.PlayerFrameClassStatusBars[k]:GetStatusBarTexture():SetHorizTile(false)
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetMinMaxValues(0, 100)
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetValue(0)
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetSize(Aleri.UI.ClassSummaryStatusBarWidth, Aleri.UI.ClassSummaryStatusBarHeight)
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetPoint('TOPLEFT', 270, ((k-1) * Aleri.UI.ClassSummaryRowHeight) - (Aleri.UI.ClassSummaryVerticalOffset - 2))
	Aleri.Guild.PlayerFrameClassStatusBars[k]:SetStatusBarColor(Aleri.Class.Colours[v].r, Aleri.Class.Colours[v].g, Aleri.Class.Colours[v].b)	
end


--level check slider
Aleri.Guild.PlayerFrame.LevelLimitSlider = CreateFrame("Slider", "MySliderGlobalNameAgain", Aleri.Guild.ClassStatsFrame, "OptionsSliderTemplate")
-- Aleri.Guild.PlayerFrame.SettingsFrame.LevelLimitSlider:SetBackdrop({bgFile = "Interface\\Buttons\\UI-SliderBar-Background", 
                                            -- edgeFile = "Interface\\Buttons\\UI-SliderBar-Border", 
                                            -- tile = true, tileSize = 8, edgeSize = 8, 
                                            -- insets = { left = 3, right = 3, top = 6, bottom = 6 }})
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetThumbTexture("Interface\\Buttons\\UI-ScrollBar-Knob")		--ScrollBar-Knob SliderBar-Button-Horizontal")
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetSize(125, 20)
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetOrientation('HORIZONTAL')
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetPoint('TOPLEFT', 45, -75)
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetValue(80)
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetMinMaxValues(1, 80)
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetValueStep(1)
--get value when changed
Aleri.Guild.PlayerFrame.LevelLimitSlider:SetScript('OnValueChanged', function(self) 
	getglobal(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetName() .. 'Text'):SetText('Min Level '..tostring(self:GetValue()))
	Aleri.Functions.ResetClassCountTable()
	Aleri.Functions.RefreshClassCountTable(self:GetValue(), Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetValue())
end)
Aleri.Guild.PlayerFrame.LevelLimitSlider.tooltipText = 'Click to show slider button\nSets the min level to use for class count' --Creates a tooltip on mouseover.
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetName() .. 'Low'):SetText('1'); --Sets the left-side slider text (default is "Low").
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetName() .. 'High'):SetText('80'); --Sets the right-side slider text (default is "High").
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetName() .. 'Text'):SetText('Min Level');


--level check slider
Aleri.Guild.PlayerFrame.LevelLimitSliderMax = CreateFrame("Slider", "AleriClassSummaryLevelSliderMax", Aleri.Guild.ClassStatsFrame, "OptionsSliderTemplate")
-- Aleri.Guild.PlayerFrame.SettingsFrame.LevelLimitSlider:SetBackdrop({bgFile = "Interface\\Buttons\\UI-SliderBar-Background", 
                                            -- edgeFile = "Interface\\Buttons\\UI-SliderBar-Border", 
                                            -- tile = true, tileSize = 8, edgeSize = 8, 
                                            -- insets = { left = 3, right = 3, top = 6, bottom = 6 }})
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetThumbTexture("Interface\\Buttons\\UI-ScrollBar-Knob")		--ScrollBar-Knob SliderBar-Button-Horizontal")
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetSize(125, 20)
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetOrientation('HORIZONTAL')
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetPoint('TOPLEFT', 200, -75)
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetValue(80)
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetMinMaxValues(1, 80)
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetValueStep(1)
--get value when changed
Aleri.Guild.PlayerFrame.LevelLimitSliderMax:SetScript('OnValueChanged', function(self) 
	getglobal(Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetName() .. 'Text'):SetText('Max Level '..tostring(self:GetValue()))
	Aleri.Functions.ResetClassCountTable()
	Aleri.Functions.RefreshClassCountTable(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetValue(), self:GetValue())
end)
Aleri.Guild.PlayerFrame.LevelLimitSliderMax.tooltipText = 'Click to show slider button\nSets the man level to use for class count' --Creates a tooltip on mouseover.
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetName() .. 'Low'):SetText('1'); --Sets the left-side slider text (default is "Low").
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetName() .. 'High'):SetText('80'); --Sets the right-side slider text (default is "High").
getglobal(Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetName() .. 'Text'):SetText('Max Level');


-------------------------------------------------------------------------------------------------------
-- ROLE SUMMARY
-------------------------------------------------------------------------------------------------------

Aleri.UI.RoleFrameFontSize = 16
Aleri.UI.RoleFrameVerticalOffset = 175
Aleri.UI.RoleFrameVerticalSpacing = 25
Aleri.UI.RoleFrameIconSize = 30
Aleri.UI.RoleFrameLeftMargin = 400
Aleri.UI.RoleFrameHorizontalSpacing = 50
Aleri.UI.RoleFrameIconTextOffset = 30
Aleri.UI.RoleFrameTankVerticalPos = -190
Aleri.UI.RoleFrameMeleeVerticalPos = -250
Aleri.UI.RoleFrameHealerVerticalPos = -310
Aleri.UI.RoleFrameRangeVerticalPos = -370

Aleri.TankIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES.blp:25:25:0:0:64:64:0:19:22:41|t"
Aleri.HealerIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES.blp:25:25:0:0:64:64:20:39:1:20|t"
Aleri.DamageIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES.blp:25:25:0:0:64:64:20:39:22:41|t"

Aleri.Guild.ClassStatsFrame.TankIcon = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTankIcon', 'OVERLAY', 'GameFontNormal')
Aleri.Guild.ClassStatsFrame.TankIcon:SetText('Tanks') --(Aleri.TankIcon)
Aleri.Guild.ClassStatsFrame.TankIcon:SetTextColor(1,1,1)
Aleri.Guild.ClassStatsFrame.TankIcon:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin, (Aleri.UI.RoleFrameTankVerticalPos) + 20)
Aleri.Guild.ClassStatsFrame.TankIcon:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize - 2)

Aleri.Guild.ClassStatsFrame.MeleeDamageIcon = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameMeleeDamageIcon', 'OVERLAY', 'GameFontNormal')
Aleri.Guild.ClassStatsFrame.MeleeDamageIcon:SetText('Melee DPS') --(Aleri.DamageIcon)
Aleri.Guild.ClassStatsFrame.MeleeDamageIcon:SetTextColor(1,1,1)
Aleri.Guild.ClassStatsFrame.MeleeDamageIcon:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin, (Aleri.UI.RoleFrameMeleeVerticalPos) + 20)
Aleri.Guild.ClassStatsFrame.MeleeDamageIcon:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize - 2)

Aleri.Guild.ClassStatsFrame.HealerIcon = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameHealerIcon', 'OVERLAY', 'GameFontNormal')
Aleri.Guild.ClassStatsFrame.HealerIcon:SetText('Healers') --(Aleri.HealerIcon)
Aleri.Guild.ClassStatsFrame.HealerIcon:SetTextColor(1,1,1)
Aleri.Guild.ClassStatsFrame.HealerIcon:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin, (Aleri.UI.RoleFrameHealerVerticalPos) + 20)
Aleri.Guild.ClassStatsFrame.HealerIcon:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize - 2)

Aleri.Guild.ClassStatsFrame.RangeDamageIcon = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameRangeDamageIcon', 'OVERLAY', 'GameFontNormal')
Aleri.Guild.ClassStatsFrame.RangeDamageIcon:SetText('Range DPS') --(Aleri.DamageIcon)
Aleri.Guild.ClassStatsFrame.RangeDamageIcon:SetTextColor(1,1,1)
Aleri.Guild.ClassStatsFrame.RangeDamageIcon:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin, (Aleri.UI.RoleFrameRangeVerticalPos) + 20)
Aleri.Guild.ClassStatsFrame.RangeDamageIcon:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize - 2)

Aleri.Guild.SpecRoleCountFontString = { 
	Tank =  { DEATHKNIGHT = nil, WARRIOR = nil, DRUID = nil, PALADIN = nil },
	Melee =  { DEATHKNIGHT = nil, WARRIOR = nil, DRUID = nil, PALADIN = nil, SHAMAN = nil, ROGUE = nil },
	Healer = { PRIEST = nil, DRUID = nil, SHAMAN = nil, PALADIN = nil },
	Range = { PRIEST = nil, DRUID = nil, SHAMAN = nil, HUNTER = nil, MAGE = nil, WARLOCK = nil }
}

for k, v in ipairs(Aleri.Class.TankIDs) do
	local f = CreateFrame('FRAME', 'AleriGuildPlayerFrameRoleFrameTankRoleClassIcon'..k, Aleri.Guild.ClassStatsFrame)
	f:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing), Aleri.UI.RoleFrameTankVerticalPos)
	f:SetSize(Aleri.UI.RoleFrameIconSize, Aleri.UI.RoleFrameIconSize)
	local t = f:CreateTexture('$parent_Texture', 'BACKGROUND')
	t:SetAllPoints(f)
	t:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..v)
	
	if v ~= 'Total' then
		local fs = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTankCountFontString'..v, 'OVERLAY', 'GameFontNormal')
		fs:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing) + Aleri.UI.RoleFrameIconTextOffset, Aleri.UI.RoleFrameTankVerticalPos - 8)
		fs:SetText(0)
		fs:SetTextColor(1,1,1)
		fs:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize)
		Aleri.Guild.SpecRoleCountFontString.Tank[v] = fs
	end
	
end
for k, v in ipairs(Aleri.Class.MeleeDpsIDs) do
	local f = CreateFrame('FRAME', 'AleriGuildPlayerFrameRoleFrameTankRoleClassIcon'..k, Aleri.Guild.ClassStatsFrame)
	f:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing), Aleri.UI.RoleFrameMeleeVerticalPos)
	f:SetSize(Aleri.UI.RoleFrameIconSize, Aleri.UI.RoleFrameIconSize)
	local t = f:CreateTexture('$parent_Texture', 'BACKGROUND')
	t:SetAllPoints(f)
	t:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..v)
	
	if v ~= 'Total' then
		local fs = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTankCountFontString'..v, 'OVERLAY', 'GameFontNormal')
		fs:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing) + Aleri.UI.RoleFrameIconTextOffset, Aleri.UI.RoleFrameMeleeVerticalPos - 8)
		fs:SetText(0)
		fs:SetTextColor(1,1,1)
		fs:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize)
		Aleri.Guild.SpecRoleCountFontString.Melee[v] = fs
	end
end
for k, v in ipairs(Aleri.Class.HealerIDs) do
	local f = CreateFrame('FRAME', 'AleriGuildPlayerFrameRoleFrameTankRoleClassIcon'..k, Aleri.Guild.ClassStatsFrame)
	f:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing), Aleri.UI.RoleFrameHealerVerticalPos)
	f:SetSize(Aleri.UI.RoleFrameIconSize, Aleri.UI.RoleFrameIconSize)
	local t = f:CreateTexture('$parent_Texture', 'BACKGROUND')
	t:SetAllPoints(f)
	t:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..v)
	
	if v ~= 'Total' then
		local fs = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTankCountFontString'..v, 'OVERLAY', 'GameFontNormal')
		fs:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing) + Aleri.UI.RoleFrameIconTextOffset, Aleri.UI.RoleFrameHealerVerticalPos - 8)
		fs:SetText(0)
		fs:SetTextColor(1,1,1)
		fs:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize)
		Aleri.Guild.SpecRoleCountFontString.Healer[v] = fs
	end
end
for k, v in ipairs(Aleri.Class.RangeDpsIDs) do
	local f = CreateFrame('FRAME', 'AleriGuildPlayerFrameRoleFrameTankRoleClassIcon'..k, Aleri.Guild.ClassStatsFrame)
	f:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing), Aleri.UI.RoleFrameRangeVerticalPos)
	f:SetSize(Aleri.UI.RoleFrameIconSize, Aleri.UI.RoleFrameIconSize)
	local t = f:CreateTexture('$parent_Texture', 'BACKGROUND')
	t:SetAllPoints(f)
	t:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..v)

	if v ~= 'Total' then
		local fs = Aleri.Guild.ClassStatsFrame:CreateFontString('AleriGuildPlayerFrameRoleFrameTankCountFontString'..v, 'OVERLAY', 'GameFontNormal')
		fs:SetPoint('TOPLEFT', Aleri.UI.RoleFrameLeftMargin + ((k-1) * Aleri.UI.RoleFrameHorizontalSpacing) + Aleri.UI.RoleFrameIconTextOffset, Aleri.UI.RoleFrameRangeVerticalPos - 8)
		fs:SetText(0)
		fs:SetTextColor(1,1,1)
		fs:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.RoleFrameFontSize)
		Aleri.Guild.SpecRoleCountFontString.Range[v] = fs
	end
end


-------------------------------------------------------------------------------------------------------
--RAID STATS SCROLL FRAME
-------------------------------------------------------------------------------------------------------
Aleri.Guild.PlayerFrame.RaidStatsFrame = CreateFrame('FRAME', 'AleriGuildPlayerFrameRaidStatsFrame', Aleri.Guild.PlayerFrame)
Aleri.Guild.PlayerFrame.RaidStatsFrame:SetPoint('TOPLEFT', 8, 8)
Aleri.Guild.PlayerFrame.RaidStatsFrame:SetSize(810, 440)
-- Aleri.Guild.PlayerFrame.RaidStatsFrame:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
                                            -- edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
                                            -- tile = true, tileSize = 8, edgeSize = 8, 
                                            -- insets = { left = 2, right = 2, top = 2, bottom = 2 }});
-- Aleri.Guild.PlayerFrame.RaidStatsFrame:SetBackdropColor(1,1,1,0.1)

Aleri.Guild.PlayerFrame.RaidStatsGridView = {}
Aleri.Guild.PlayerFrame.RaidStatsColumns = { 'Level', 'ClassIcon', 'ClassIconTexture', 'Name', 'MainSpec', 'SecondarySpec', 'PrimaryProf', 'PrimaryProfLevel', 'SecondaryProf', 'SecondaryProfLevel' }


Aleri.Guild.PlayerFrame.RaidStatsFrame.LevelSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameLevelSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.LevelSortButton:SetPoint('TOPLEFT', 0, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.LevelSortButton:SetSize(40, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.LevelSortButton:SetText('Level')
Aleri.Guild.PlayerFrame.RaidStatsFrame.LevelSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = self:GetText()	
	Aleri.Functions.RefreshRaidStats(self:GetText(), 1)
end)


Aleri.Guild.PlayerFrame.RaidStatsFrame.NameSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameNameSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.NameSortButton:SetPoint('TOPLEFT', 40, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.NameSortButton:SetSize(110, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.NameSortButton:SetText('Name')
Aleri.Guild.PlayerFrame.RaidStatsFrame.NameSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = self:GetText()	
	Aleri.Functions.RefreshRaidStats(self:GetText(), 1)
end)

Aleri.Guild.PlayerFrame.RaidStatsFrame.ClassSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameGearScoreSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.ClassSortButton:SetPoint('TOPLEFT', 150, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.ClassSortButton:SetSize(40, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.ClassSortButton:SetText('Class')
Aleri.Guild.PlayerFrame.RaidStatsFrame.ClassSortButton:SetScript('OnClick', function(self) 
	Aleri.Functions.RefreshRaidStats(self:GetText(), 1)
end)

Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameMainSpecSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecSortButton:SetPoint('TOPLEFT', 190, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecSortButton:SetSize(100, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecSortButton:SetText('Main Spec')
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = 'MainSpec' 
	Aleri.Functions.RefreshRaidStats('MainSpec', 1)
end)

Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecGearScoreSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameSecSpecGearScoreSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecGearScoreSortButton:SetPoint('TOPLEFT', 290, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecGearScoreSortButton:SetSize(100, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecGearScoreSortButton:SetText('Off Spec')
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainSpecGearScoreSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = 'SecondarySpec' 
	Aleri.Functions.RefreshRaidStats('SecondarySpec', 1)
end)

Aleri.Guild.PlayerFrame.RaidStatsFrame.MainProfSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameMainProfSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainProfSortButton:SetPoint('TOPLEFT', 390, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainProfSortButton:SetSize(150, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainProfSortButton:SetText('Profession 1')
Aleri.Guild.PlayerFrame.RaidStatsFrame.MainProfSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = 'PrimaryProf' 
	Aleri.Functions.RefreshRaidStats('PrimaryProf', 1)
end)

Aleri.Guild.PlayerFrame.RaidStatsFrame.SecondProfSortButton = CreateFrame("BUTTON", "AleriGuildPlayerFrameRaidStatsFrameSecondProfSortButton", Aleri.Guild.PlayerFrame.RaidStatsFrame, "UIPanelButtonTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrame.SecondProfSortButton:SetPoint('TOPLEFT', 540, -24)
Aleri.Guild.PlayerFrame.RaidStatsFrame.SecondProfSortButton:SetSize(150, 20)
Aleri.Guild.PlayerFrame.RaidStatsFrame.SecondProfSortButton:SetText('Profession 2')
Aleri.Guild.PlayerFrame.RaidStatsFrame.SecondProfSortButton:SetScript('OnClick', function(self) 
	Aleri.Guild.RaidStatsSort = 'SecondaryProf' 
	Aleri.Functions.RefreshRaidStats('SecondaryProf', 1)
end)


--SCROLL FRAME
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame = CreateFrame('ScrollFrame', 'AleriGuildPlayerFrameRaidStatsFrameScrollFrame', Aleri.Guild.PlayerFrame.RaidStatsFrame)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame:SetPoint('TOPLEFT', 4, -44)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame:SetPoint('BOTTOMRIGHT', -60, 4)

Aleri.Guild.PlayerFrame.RaidStatsFrame.scrollframe = Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame

--SCROLLBAR
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar = CreateFrame("Slider", 'AleriGuildPlayerFrameRaidStatsFrameScrollBar', Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame, "UIPanelScrollBarTemplate")
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetPoint("TOPLEFT", Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame, "TOPRIGHT", -12,-12) 
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetPoint("BOTTOMLEFT", Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame, "BOTTOMRIGHT", -12,4)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetMinMaxValues(1, 100)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetValueStep(1)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar.scrollStep = 1
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetValue(1)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetWidth(16)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetScript('OnValueChanged', function(self, value) 
	self:GetParent():SetVerticalScroll((Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent:GetHeight() / 100) * value)
	--print('scroll bar value', value)
	if value == 1 then
		self:GetParent():SetVerticalScroll(1)
	elseif value == 100 then
		self:GetParent():SetVerticalScroll(Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent:GetHeight())
	end
end )
--mouse scroll wheel function
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame:EnableMouseWheel(1)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame:SetScript('OnMouseWheel', function(self, value) 
local v_mouse_scroll = Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:GetValue()
	if value == 1 then
		Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetValue(v_mouse_scroll - 1)
		--print(self:GetVerticalScroll())
	elseif value == -1 then
		Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:SetValue(v_mouse_scroll + 1)
		--print(self:GetVerticalScroll())
	end
end)


local scrollBarTexture = Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar:CreateTexture('AleriGuildPlayerFrameRaidStatsFrameScrollBarTexture', 'BACKGROUND')
scrollBarTexture:SetAllPoints(Aleri.Guild.PlayerFrame.RaidStatsFrameScrollBar)
scrollBarTexture:SetTexture(0, 0, 0, 0.4)
Aleri.Guild.PlayerFrame.RaidStatsFrame.scrollbar = scrollBarTexture

--SCROLL FRAME CONTENT FRAME
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent = CreateFrame('FRAME', 'AleriGuildPlayerFrameRaidStatsFrameScrollFrameContent', Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame)
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent:SetSize(430, 460)

Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame.content = Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent
Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrame:SetScrollChild(Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent)
-------------------------------------------------------------------------------------------------------


Aleri.UI.MemberFrame = CreateFrame('FRAME', AleriUIMemberrame, Aleri.Guild.PlayerFrame)
Aleri.UI.MemberFrame:SetPoint('TOPLEFT', 5, -5)
Aleri.UI.MemberFrame:SetPoint('BOTTOMRIGHT', -5, 5)
--Aleri.UI.MemberFrame:SetSize(750, 425)
Aleri.UI.MemberFrame.Texture = Aleri.UI.MemberFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.Texture:SetAllPoints(Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.Texture:SetTexture(0,0,0,1)

Aleri.UI.MemberFrame.OnlineCount = Aleri.UI.MemberFrame:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.OnlineCount:SetPoint('TOPLEFT', 20, -20)

Aleri.UI.MemberFrame.RefreshButton = CreateFrame('BUTTON', 'AleriUIMemberFrameRefreshButton', Aleri.UI.MemberFrame, 'UIPanelButtonTemplate')
Aleri.UI.MemberFrame.RefreshButton:SetPoint('TOPLEFT', 20, -50)
Aleri.UI.MemberFrame.RefreshButton:SetText('Refresh List')
Aleri.UI.MemberFrame.RefreshButton:SetSize(100, 22)
Aleri.UI.MemberFrame.RefreshButton:SetScript('OnClick', function(self)
	Aleri.Functions.PopulateMemberList()
end)

--SCROLL FRAME
Aleri.UI.MemberFrame.MemberListScrollFrame = CreateFrame('ScrollFrame', 'AleriGuildPlayerFrameRaidStatsFrameScrollFrame', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.MemberListScrollFrame:SetPoint('TOPLEFT', 4, -95)
--Aleri.Guild.PlayerFrame.ProfessionsFrameScrollFrame:SetPoint('BOTTOMLEFT', 400, 8)
Aleri.UI.MemberFrame.MemberListScrollFrame:SetSize(180, 325)

Aleri.UI.MemberFrame.MemberListScrollFrame.scrollframe = Aleri.Guild.PlayerFrame.ProfessionsFrameScrollFrame

--SCROLLBAR
Aleri.UI.MemberFrame.MemberListScrollBar = CreateFrame("Slider", 'Aleri.UI.MemberFrame.MemberListScrollBar', Aleri.UI.MemberFrame.MemberListScrollFrame, "UIPanelScrollBarTemplate")
Aleri.UI.MemberFrame.MemberListScrollBar:SetPoint("TOPLEFT", Aleri.UI.MemberFrame.MemberListScrollFrame, "TOPRIGHT", -12,-12) 
Aleri.UI.MemberFrame.MemberListScrollBar:SetPoint("BOTTOMLEFT", Aleri.UI.MemberFrame.MemberListScrollFrame, "BOTTOMRIGHT", -12,16)
Aleri.UI.MemberFrame.MemberListScrollBar:SetMinMaxValues(1, 100)
Aleri.UI.MemberFrame.MemberListScrollBar:SetValueStep(1)
Aleri.UI.MemberFrame.MemberListScrollBar.scrollStep = 1
Aleri.UI.MemberFrame.MemberListScrollBar:SetValue(1)
Aleri.UI.MemberFrame.MemberListScrollBar:SetWidth(16)
Aleri.UI.MemberFrame.MemberListScrollBar:SetScript('OnValueChanged', function(self, value) 
	self:GetParent():SetVerticalScroll((Aleri.UI.MemberFrame.MemberListScrollFrameContent:GetHeight() / 100) * value)
	--print('scroll bar value', value)
	if value == 1 then
		self:GetParent():SetVerticalScroll(1)
	elseif value == 100 then
		self:GetParent():SetVerticalScroll(Aleri.UI.MemberFrame.MemberListScrollFrameContent:GetHeight())
	end
end )
--mouse scroll wheel function
Aleri.UI.MemberFrame.MemberListScrollFrame:EnableMouseWheel(1)
Aleri.UI.MemberFrame.MemberListScrollFrame:SetScript('OnMouseWheel', function(self, value) 
local v_mouse_scroll = Aleri.UI.MemberFrame.MemberListScrollBar:GetValue()
	if value == 1 then
		Aleri.UI.MemberFrame.MemberListScrollBar:SetValue(v_mouse_scroll - 1)
		--print(self:GetVerticalScroll())
	elseif value == -1 then
		Aleri.UI.MemberFrame.MemberListScrollBar:SetValue(v_mouse_scroll + 1)
		--print(self:GetVerticalScroll())
	end
end)


local memberScrollBarTexture = Aleri.UI.MemberFrame.MemberListScrollBar:CreateTexture('Aleri.UI.MemberFrame.MemberListScrollBarTexture', 'BACKGROUND')
memberScrollBarTexture:SetAllPoints(Aleri.UI.MemberFrame.MemberListScrollBar)
memberScrollBarTexture:SetTexture(0, 0, 0, 0.4)
Aleri.UI.MemberFrame.MemberListScrollFrame.scrollbar = memberScrollBarTexture

--SCROLL FRAM ECONTENT FRAME
Aleri.UI.MemberFrame.MemberListScrollFrameContent = CreateFrame('FRAME', 'Aleri.UI.MemberFrame.MemberListScrollFrameContent', Aleri.UI.MemberFrame.MemberListScrollFrame)
Aleri.UI.MemberFrame.MemberListScrollFrameContent:SetSize(430, 460)

Aleri.UI.MemberFrame.MemberListScrollFrame.content = Aleri.UI.MemberFrame.MemberListScrollFrameContent
Aleri.UI.MemberFrame.MemberListScrollFrame:SetScrollChild(Aleri.UI.MemberFrame.MemberListScrollFrameContent)

Aleri.UI.MemberFrame.RaceIcon = CreateFrame('FRAME', 'AleriUIMemberFrameRaceIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.RaceIcon:SetPoint('TOPLEFT', 210, -12)
Aleri.UI.MemberFrame.RaceIcon:SetSize(60, 60)
Aleri.UI.MemberFrame.RaceIcon.Texture = Aleri.UI.MemberFrame.RaceIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.RaceIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.RaceIcon)

Aleri.UI.MemberFrame.PlayerName = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePlayerName', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PlayerName:SetPoint('TOPLEFT', 350, -25)
Aleri.UI.MemberFrame.PlayerName:SetFont("Fonts\\FRIZQT__.TTF", 20)

Aleri.UI.MemberFrame.PlayerAchievePoints = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePlayerAchievePoints', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PlayerAchievePoints:SetPoint('TOPLEFT', 350, -60)
Aleri.UI.MemberFrame.PlayerAchievePoints:SetFont("Fonts\\FRIZQT__.TTF", 14)
Aleri.UI.MemberFrame.PlayerAchievePoints:SetTextColor(1, 1, 1, 1)

Aleri.UI.MemberFrame.PlayerLevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePlayerLevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PlayerLevel:SetPoint('TOPLEFT', 550, -25)
Aleri.UI.MemberFrame.PlayerLevel:SetFont("Fonts\\FRIZQT__.TTF", 20)
Aleri.UI.MemberFrame.PlayerLevel:SetTextColor(1, 1, 1, 1)

Aleri.UI.MemberFrame.Playerilvl = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePlayerilvl', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.Playerilvl:SetPoint('TOPLEFT', 530, -390)
Aleri.UI.MemberFrame.Playerilvl:SetFont("Fonts\\FRIZQT__.TTF", 14)
Aleri.UI.MemberFrame.Playerilvl:SetTextColor(1, 1, 1, 1)

Aleri.UI.MemberFrame.PlayerGS = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePlayerGS', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PlayerGS:SetPoint('TOPLEFT', 530, -415)
Aleri.UI.MemberFrame.PlayerGS:SetFont("Fonts\\FRIZQT__.TTF", 14)
Aleri.UI.MemberFrame.PlayerGS:SetTextColor(1, 1, 1, 1)

Aleri.UI.MemberFrame.PlayerClassIcon = CreateFrame('FRAME', 'Aleri.UI.MemberFrame.PlayerClassIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.PlayerClassIcon:SetPoint('TOPRIGHT', -4, -8)
Aleri.UI.MemberFrame.PlayerClassIcon:SetSize(70, 70)
Aleri.UI.MemberFrame.PlayerClassIcon.Texture = Aleri.UI.MemberFrame.PlayerClassIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.PlayerClassIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.PlayerClassIcon)

Aleri.UI.MemberFrameSpecIconSize = 25
Aleri.UI.MemberFrameProfIconSize = 20
Aleri.UI.MemberFrameProfFontSize = 15
Aleri.UI.MemberFrameProfTopMargin = -100
Aleri.UI.MemberFrameSecProfTopMargin = -170
Aleri.UI.MemberFrameSecProfIconHorizontal = 200
Aleri.UI.MemberFrameSecProfNameHorizontal = 230
Aleri.UI.MemberFrameSecProfLevelHorizontal = 315
Aleri.UI.MemberFramePriProfIconHorizontal = 225
Aleri.UI.MemberFramePriProfNameHorizontal = 250
Aleri.UI.MemberFramePriProfLevelHorizontal = 400

-- SPEC 
Aleri.UI.MemberFrame.PrimarySpec = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePrimarySpec', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PrimarySpec:SetPoint('TOPLEFT', 475, -100)
Aleri.UI.MemberFrame.PrimarySpec:SetText('Current Spec') -- USING CURRENT SPEC INFO HERE BUT LOOK TO ADD BOTH IN FUTURE
Aleri.UI.MemberFrame.PrimarySpec:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.PrimarySpec:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.PrimarySpecName = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFramePrimarySpecName', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.PrimarySpecName:SetPoint('TOPLEFT', 600, -100)
Aleri.UI.MemberFrame.PrimarySpecName:SetText(' ')
Aleri.UI.MemberFrame.PrimarySpecName:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.PrimarySpecName:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.PrimarySpecIcon = CreateFrame('FRAME', 'AleriUIMemberFramePrimarySpecIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.PrimarySpecIcon:SetPoint('TOPLEFT', 745, -95)
Aleri.UI.MemberFrame.PrimarySpecIcon:SetSize(Aleri.UI.MemberFrameSpecIconSize, Aleri.UI.MemberFrameSpecIconSize)
Aleri.UI.MemberFrame.PrimarySpecIcon.Texture = Aleri.UI.MemberFrame.PrimarySpecIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.PrimarySpecIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.PrimarySpecIcon)
--Aleri.UI.MemberFrame.PrimarySpecIcon.Texure:SetTexture('Interface\\Icons\\Spell_Nature_MagicImmunity')

-- COOKING
Aleri.UI.MemberFrame.CookingIcon = CreateFrame('FRAME', 'AleriUIMemberFrameCookingIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.CookingIcon:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfIconHorizontal, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.CookingIcon:SetSize(Aleri.UI.MemberFrameProfIconSize, Aleri.UI.MemberFrameProfIconSize)
Aleri.UI.MemberFrame.CookingIcon.Texture = Aleri.UI.MemberFrame.CookingIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.CookingIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.CookingIcon)
Aleri.UI.MemberFrame.CookingIcon.Texture:SetTexture('Interface\\Icons\\INV_MISC_FOOD_15')

Aleri.UI.MemberFrame.Cooking = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameCooking', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.Cooking:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfNameHorizontal, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.Cooking:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.Cooking:SetText('Cooking')
Aleri.UI.MemberFrame.Cooking:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.CookingLevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameCookingLevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.CookingLevel:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfLevelHorizontal, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.CookingLevel:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.CookingLevel:SetText('-')
Aleri.UI.MemberFrame.CookingLevel:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

-- FIRST AID
Aleri.UI.MemberFrame.FirstAidIcon = CreateFrame('FRAME', 'AleriUIMemberFrameFirstAid', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.FirstAidIcon:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfIconHorizontal + 200, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.FirstAidIcon:SetSize(Aleri.UI.MemberFrameProfIconSize, Aleri.UI.MemberFrameProfIconSize)
Aleri.UI.MemberFrame.FirstAidIcon.Texture = Aleri.UI.MemberFrame.FirstAidIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.FirstAidIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.FirstAidIcon)
Aleri.UI.MemberFrame.FirstAidIcon.Texture:SetTexture('Interface\\Icons\\Spell_Holy_SealOfSacrifice')

Aleri.UI.MemberFrame.FirstAid = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameFirstAid', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.FirstAid:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfNameHorizontal + 200, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.FirstAid:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.FirstAid:SetText('First Aid')
Aleri.UI.MemberFrame.FirstAid:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.FirstAidLevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameFirstAidLevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.FirstAidLevel:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfLevelHorizontal + 200, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.FirstAidLevel:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.FirstAidLevel:SetText('-')
Aleri.UI.MemberFrame.FirstAidLevel:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

--FISHING
Aleri.UI.MemberFrame.FishingIcon = CreateFrame('FRAME', 'AleriUIMemberFrameFishingIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.FishingIcon:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfIconHorizontal + 400, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.FishingIcon:SetSize(Aleri.UI.MemberFrameProfIconSize, Aleri.UI.MemberFrameProfIconSize)
Aleri.UI.MemberFrame.FishingIcon.Texture = Aleri.UI.MemberFrame.FishingIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.FishingIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.FishingIcon)
Aleri.UI.MemberFrame.FishingIcon.Texture:SetTexture('Interface\\Icons\\Trade_Fishing')

Aleri.UI.MemberFrame.Fishing = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameFishing', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.Fishing:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfNameHorizontal + 400, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.Fishing:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.Fishing:SetText('Fishing')
Aleri.UI.MemberFrame.Fishing:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.FishingLevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameFishingLevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.FishingLevel:SetPoint('TOPLEFT', Aleri.UI.MemberFrameSecProfLevelHorizontal + 400, Aleri.UI.MemberFrameSecProfTopMargin)
Aleri.UI.MemberFrame.FishingLevel:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.FishingLevel:SetText('-')
Aleri.UI.MemberFrame.FishingLevel:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

--PROF A
Aleri.UI.MemberFrame.ProfessionAIcon = CreateFrame('FRAME', 'AleriUIMemberFrameProfessionA', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.ProfessionAIcon:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfIconHorizontal, Aleri.UI.MemberFrameProfTopMargin)
Aleri.UI.MemberFrame.ProfessionAIcon:SetSize(Aleri.UI.MemberFrameProfIconSize, Aleri.UI.MemberFrameProfIconSize)
Aleri.UI.MemberFrame.ProfessionAIcon.Texture = Aleri.UI.MemberFrame.ProfessionAIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.ProfessionAIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.ProfessionAIcon)

Aleri.UI.MemberFrame.ProfessionA = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameProfessionA', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.ProfessionA:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfNameHorizontal, Aleri.UI.MemberFrameProfTopMargin)
Aleri.UI.MemberFrame.ProfessionA:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.ProfessionA:SetText('Profession')
Aleri.UI.MemberFrame.ProfessionA:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.ProfessionALevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameProfessionALevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.ProfessionALevel:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfLevelHorizontal, Aleri.UI.MemberFrameProfTopMargin)
Aleri.UI.MemberFrame.ProfessionALevel:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.ProfessionALevel:SetText('-')
Aleri.UI.MemberFrame.ProfessionALevel:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

-- PROF B
Aleri.UI.MemberFrame.ProfessionBIcon = CreateFrame('FRAME', 'AleriUIMemberFrameProfessionBIcon', Aleri.UI.MemberFrame)
Aleri.UI.MemberFrame.ProfessionBIcon:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfIconHorizontal, Aleri.UI.MemberFrameProfTopMargin - 30)
Aleri.UI.MemberFrame.ProfessionBIcon:SetSize(Aleri.UI.MemberFrameProfIconSize, Aleri.UI.MemberFrameProfIconSize)
Aleri.UI.MemberFrame.ProfessionBIcon.Texture = Aleri.UI.MemberFrame.ProfessionBIcon:CreateTexture('$parent_Texture', 'BACKGROUND')
Aleri.UI.MemberFrame.ProfessionBIcon.Texture:SetAllPoints(Aleri.UI.MemberFrame.ProfessionBIcon)

Aleri.UI.MemberFrame.ProfessionB = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameProfessionB', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.ProfessionB:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfNameHorizontal, Aleri.UI.MemberFrameProfTopMargin - 30)
Aleri.UI.MemberFrame.ProfessionB:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.ProfessionB:SetText('Profession')
Aleri.UI.MemberFrame.ProfessionB:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)

Aleri.UI.MemberFrame.ProfessionBLevel = Aleri.UI.MemberFrame:CreateFontString('AleriUIMemberFrameProfessionBLevel', 'OVERLAY', 'GameFontNormal')
Aleri.UI.MemberFrame.ProfessionBLevel:SetPoint('TOPLEFT', Aleri.UI.MemberFramePriProfLevelHorizontal, Aleri.UI.MemberFrameProfTopMargin - 30)
Aleri.UI.MemberFrame.ProfessionBLevel:SetTextColor(1,1,1,1)
Aleri.UI.MemberFrame.ProfessionBLevel:SetText('-')
Aleri.UI.MemberFrame.ProfessionBLevel:SetFont("Fonts\\FRIZQT__.TTF", Aleri.UI.MemberFrameProfFontSize)



Aleri.UI.MemberFrame.EquipemntLinks = {}
Aleri.UI.MemberFrame.EquipmentListVertOffset = 190

local equip_slots_l, equip_slots_r = 1, 1
for k, s in ipairs(Aleri.Database.InventorySlots) do
	local fs = Aleri.UI.MemberFrame:CreateFontString('Equipment'..k, 'OVERLAY', 'GameFontNormal')
	local fs_link = Aleri.UI.MemberFrame:CreateFontString(s.Name, 'OVERLAY', 'GameFontNormal')
	if s.Pos == 0 then
		fs:SetPoint('TOPLEFT', 200, ((equip_slots_l * 22) * -1) - Aleri.UI.MemberFrame.EquipmentListVertOffset)
		fs_link:SetPoint('TOPLEFT', 280, ((equip_slots_l * 22) * -1) - Aleri.UI.MemberFrame.EquipmentListVertOffset)
		table.insert(Aleri.UI.MemberFrame.EquipemntLinks, { Slot = s.Name, FontString = fs_link } )
		equip_slots_l = equip_slots_l + 1
	else
		fs:SetPoint('TOPLEFT', 530, ((equip_slots_r * 22) * -1) - Aleri.UI.MemberFrame.EquipmentListVertOffset)
		fs_link:SetPoint('TOPLEFT', 600, ((equip_slots_r * 22) * -1) - Aleri.UI.MemberFrame.EquipmentListVertOffset)
		table.insert(Aleri.UI.MemberFrame.EquipemntLinks, { Slot = s.Name, FontString = fs_link } )
		equip_slots_r = equip_slots_r + 1
	end
	fs:SetText(s.Display)
end


-------------------------------------------------------------------------------------------------------
-- Aleri Guild Player frame Tab Set Up
-------------------------------------------------------------------------------------------------------
Aleri.Guild.PlayerFrame.TabButtons = {}

Aleri.Guild.PlayerFrame.ClassSummaryTabButton = CreateFrame('BUTTON', '$parentTab1', Aleri.Guild.PlayerFrame, 'CharacterFrameTabButtonTemplate')
Aleri.Guild.PlayerFrame.ClassSummaryTabButton:SetPoint('BOTTOMLEFT', 0, -30)
Aleri.Guild.PlayerFrame.ClassSummaryTabButton:SetText('Class Summary')
Aleri.Guild.PlayerFrame.ClassSummaryTabButton:SetFrameStrata('BACKGROUND')
Aleri.Guild.PlayerFrame.ClassSummaryTabButton:SetScript('OnClick', function(self) 
	Aleri.Guild.PlayerFrame.TabButtonsUpdateUI(self) 
	--Aleri.Guild.PlayerFrameDropDown:Show()
	Aleri.Functions.ResetRoleTable()
	Aleri.Functions.RefreshRoleTable()
	Aleri.Functions.ResetClassCountTable()
	Aleri.Functions.RefreshClassCountTable(Aleri.Guild.PlayerFrame.LevelLimitSlider:GetValue(), Aleri.Guild.PlayerFrame.LevelLimitSliderMax:GetValue())
end)
table.insert(Aleri.Guild.PlayerFrame.TabButtons, { Id = 1, Button = Aleri.Guild.PlayerFrame.ClassSummaryTabButton, Frame = Aleri.Guild.ClassStatsFrame } )

Aleri.Guild.PlayerFrame.RoleSummaryTabButton = CreateFrame('BUTTON', '$parentTab2', Aleri.Guild.PlayerFrame, 'CharacterFrameTabButtonTemplate')
Aleri.Guild.PlayerFrame.RoleSummaryTabButton:SetPoint('BOTTOMLEFT', 105, -30)
Aleri.Guild.PlayerFrame.RoleSummaryTabButton:SetText('Details')
Aleri.Guild.PlayerFrame.RoleSummaryTabButton:SetFrameStrata('BACKGROUND')
Aleri.Guild.PlayerFrame.RoleSummaryTabButton:SetScript('OnClick', function(self) 
	Aleri.Guild.PlayerFrame.TabButtonsUpdateUI(self)
	Aleri.Functions.PopulateMemberList()
	--Aleri.Guild.PlayerFrameDropDown:Show()
 --(Aleri.Guild.PlayerFrame.RoleFrame.LevelLimitSlider:GetValue(), Aleri.Guild.PlayerFrame.RoleFrame.LevelLimitSliderMax:GetValue())
end)
table.insert(Aleri.Guild.PlayerFrame.TabButtons, { Id = 2, Button = Aleri.Guild.PlayerFrame.RoleSummaryTabButton, Frame = Aleri.UI.MemberFrame } )

Aleri.Guild.PlayerFrame.RaidStatsTabButton = CreateFrame('BUTTON', '$parentTab3', Aleri.Guild.PlayerFrame, 'CharacterFrameTabButtonTemplate')
Aleri.Guild.PlayerFrame.RaidStatsTabButton:SetPoint('BOTTOMLEFT', 180, -30)
Aleri.Guild.PlayerFrame.RaidStatsTabButton:SetText('Roster')
Aleri.Guild.PlayerFrame.RaidStatsTabButton:SetFrameStrata('BACKGROUND')
Aleri.Guild.PlayerFrame.RaidStatsTabButton:SetScript('OnClick', function(self) 
	Aleri.Guild.PlayerFrame.TabButtonsUpdateUI(self) 
	Aleri.Functions.RefreshRaidStats('none', 1)
end)
table.insert(Aleri.Guild.PlayerFrame.TabButtons, { Id = 3, Button = Aleri.Guild.PlayerFrame.RaidStatsTabButton, Frame = Aleri.Guild.PlayerFrame.RaidStatsFrame } )


function Aleri.Guild.PlayerFrame.TabButtonsUpdateUI(active_button)
	for k, button in ipairs(Aleri.Guild.PlayerFrame.TabButtons) do
		if button.Button ~= active_button then
			button.Button:SetFrameStrata('BACKGROUND')
			button.Frame:Hide()
		else
			button.Button:SetFrameStrata('HIGH')
			button.Frame:Show()
		end
	end
	-- if active_button ~= Aleri.Guild.PlayerFrame.RaidStatsTabButton then
		-- Aleri.Guild.MemberDetailFrame:Hide()
	-- end
end
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- Merchant Functions
-------------------------------------------------------------------------------------------------------
function Aleri.Merchant.SellJunk()
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do 
			local item_link = GetContainerItemLink(bag,slot) 
			if item_link and select(3, GetItemInfo(item_link)) == 0 then
				UseContainerItem(bag,slot)
			end
		end
	end
end

Aleri.Merchant.SellJunkButton = CreateFrame("BUTTON", "AleriMerchantSellJunkButton", MerchantFrame, "UIPanelButtonTemplate")
Aleri.Merchant.SellJunkButton:SetText('|cFFFFFFFFSell Junk|r')
Aleri.Merchant.SellJunkButton:SetPoint("TOPLEFT", 80, -42)
Aleri.Merchant.SellJunkButton:SetSize(80, 25)
Aleri.Merchant.SellJunkButton:SetScript('OnClick', function() Aleri.Merchant.SellJunk() end)
-------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------
-- Mail Functions
-------------------------------------------------------------------------------------------------------
function Aleri.Mail.TakeAuctionSoldGold()
	for mail = GetInboxNumItems(), 1, -1 do
		_, _, Aleri.Mail.Sender, Aleri.Mail.Subject, Aleri.Mail.Currency = GetInboxHeaderInfo(mail)
		if string.find(Aleri.Mail.Subject, 'Auction successful') then
			--print('Sold '..string.sub(Aleri.Mail.Subject, 21)..' for '..GetCoinTextureString(Aleri.Mail.Currency))
			TakeInboxMoney(mail)			
		end
	end			
end

function Aleri.Mail.TakeAuctionWonItems()
	for mail = GetInboxNumItems(), 1, -1 do
		_, _, Aleri.Mail.Sender, Aleri.Mail.Subject, Aleri.Mail.Currency = GetInboxHeaderInfo(mail)
		if string.find(Aleri.Mail.Subject, 'Auction won') then
			--print()
			TakeInboxItem(mail)			
		end
	end			
end

function Aleri.Mail.TakeAuctionExpiredItems()
	for mail = GetInboxNumItems(), 1, -1 do
		_, _, Aleri.Mail.Sender, Aleri.Mail.Subject, Aleri.Mail.Currency = GetInboxHeaderInfo(mail)
		if string.find(Aleri.Mail.Subject, 'Auction expired') then
			--print()
			TakeInboxItem(mail)			
		end
	end			
end

Aleri.Mail.TakeGoldButton = CreateFrame("BUTTON", "AleriMailTakeGoldButton", InboxFrame, "UIPanelButtonTemplate")
Aleri.Mail.TakeGoldButton:SetText('|cFFFFFFFFAH Gold|r')
Aleri.Mail.TakeGoldButton:SetPoint("TOPLEFT", 80, -42)
Aleri.Mail.TakeGoldButton:SetSize(70, 25)
Aleri.Mail.TakeGoldButton:SetScript('OnClick', function() Aleri.Mail.TakeAuctionSoldGold() end)

Aleri.Mail.TakeAuctionWonItemsButton = CreateFrame("BUTTON", "AleriMailTakeAuctionWonItemsButton", InboxFrame, "UIPanelButtonTemplate")
Aleri.Mail.TakeAuctionWonItemsButton:SetText('|cFFFFFFFFAH Items|r')
Aleri.Mail.TakeAuctionWonItemsButton:SetPoint("TOPLEFT", 160, -42)
Aleri.Mail.TakeAuctionWonItemsButton:SetSize(70, 25)
Aleri.Mail.TakeAuctionWonItemsButton:SetScript('OnClick', function() Aleri.Mail.TakeAuctionWonItems() end)

Aleri.Mail.TakeAuctionWonItemsButton = CreateFrame("BUTTON", "AleriMailTakeAuctionWonItemsButton", InboxFrame, "UIPanelButtonTemplate")
Aleri.Mail.TakeAuctionWonItemsButton:SetText('|cFFFFFFFFAH Expired|r')
Aleri.Mail.TakeAuctionWonItemsButton:SetPoint("TOPLEFT", 240, -42)
Aleri.Mail.TakeAuctionWonItemsButton:SetSize(80, 25)
Aleri.Mail.TakeAuctionWonItemsButton:SetScript('OnClick', function() Aleri.Mail.TakeAuctionExpiredItems() end)
-------------------------------------------------------------------------------------------------------

Aleri.Guild.PublicNotes = {}

-------------------------------------------------------------------------------------------------------
-- Popups
-------------------------------------------------------------------------------------------------------
StaticPopupDialogs['UpdatePublicNote'] = {
	text = 'Update Public Note for - %s \n \n %s',
	button1 = 'OK',
	button2 = 'No',
	OnAccept = function() print('yay') end,
	OnCancel = function() print('no') end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}

StaticPopupDialogs['BuildLocalDatabaseDialog'] = {
	text = 'Build new local database? \n\n This will delete the current database!',
	button1 = 'yes',
	button2 = 'No',
	OnAccept = function() print(Aleri.PrintColour..'Aleri - this button does absolutely nothing right now!') end,
	OnCancel = function() print(Aleri.PrintColour..'Aleri - this button does absolutely nothing right now!') end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}

StaticPopupDialogs['ScanGuildRosterDialog'] = {
	text = 'WARNING\n\nYou must have \'Show Offline Members\' checked!',
	button1 = 'OK',
	OnAccept = function() print(Aleri.PrintColour..'Aleri - scan cancelled') end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}

StaticPopupDialogs['BroadcastDatabaseDialog'] = {
	text = 'WARNING\n\nSend current database to all online guild members?',
	button1 = 'Yes',
	OnAccept = function()
		--Aleri.Guild.CheckDatabase()
		Aleri.Guild.BroadcastDatabase()
		--print(Aleri.PrintColour..'Aleri - '..Aleri.ThisPlayer.Name..' is sending their database') 
		--SendChatMessage(Aleri.PrintColour..'Aleri - '..Aleri.ThisPlayer.name..' is sending their database', 'GUILD')  
	end,
	button2 = 'No',
	OnCancel = function() print(Aleri.PrintColour..'Aleri - braodcast cancelled.') end,
	timeout = 0,
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
}


-------------------------------------------------------------------------------------------------------
-- Events
-------------------------------------------------------------------------------------------------------
function Aleri.OnEvent(self, event, ...)

	if event == 'ADDON_LOADED' and select(1, ...) == 'Aleri' then
		print(Aleri.PrintColour..'Aleri - Aleri main file loaded')

		for k, tab in ipairs(Aleri.Guild.PlayerFrame.TabButtons) do 
			if tab.Id == 1 then
				tab.Button:SetFrameStrata('HIGH')
				tab.Frame:Show()
			else
				tab.Button:SetFrameStrata('BACKGROUND')
				tab.Frame:Hide()
			end
		end

		if AleriCharacterSettings == nil then 
			AleriCharacterSettings = { GuildDatabase = {} }
		end

		Aleri.ThisPlayer.Name = UnitName('player')
		
		Aleri.MakeFrameMove(Aleri.Guild.PlayerFrame)
		
		print(Aleri.PrintColour..'Aleri - current version '..GetAddOnMetadata('Aleri', 'Version'))

	end
	
	if event == 'ADDON_LOADED' and select(1, ...) == 'GearScoreLite' then
		Aleri.GearScoreLiteDetected = true
		if Aleri.GearScoreLiteDetected == true then
			local gs = select(1, GearScore_GetScore(UnitName("player"), "player")) -- borrowed from gearscore addon
			--print(Aleri.PrintColour..'Aleri - GearScoreLite detected, current gear score: '..gs)
			Aleri.CurrentGearScore = tonumber(gs)
		end
	end
		
	if event == "GUILD_ROSTER_UPDATE" then
		GuildRoster()
		Aleri.Functions.PopulateMemberList()
	end
		
	--receive request for data -> send it
	if event == 'CHAT_MSG_ADDON' and select(1, ...) == 'aleri-g-p-data' and select(3, ...) == 'GUILD' then
		print('data request got')
		SendAddonMessage('aleri-P-data', Aleri.Functions.GetPlayerData(), 'GUILD')
	end
	
	--recieve data
	if event == 'CHAT_MSG_ADDON' and select(1, ...) == 'aleri-P-data' and select(3, ...) == 'GUILD' then
		msg = select(2, ...)
		Aleri.Functions.ParsePlayerData(msg)
	end
	
	--used when a profression is levelled and provides updates to online players
	if event == "CHAT_MSG_SKILL" then
		SendAddonMessage('aleri-P-data', Aleri.Functions.GetPlayerData(), 'GUILD')
	end
	
	
end

Aleri.MainFrame:SetScript('OnEvent', Aleri.OnEvent)
-------------------------------------------------------------------------------------------------------

FriendsFrameTab3:HookScript('OnClick', function() SendAddonMessage('aleri-g-p-data', 'Data request', 'GUILD') end)

-------------------------------------------------------------------------------------------------------
-- Slash Commands
-------------------------------------------------------------------------------------------------------
SLASH_ALERI1 = '/aleri'
SlashCmdList['ALERI'] = function(msg)
	if string.find(msg, 'help') then
		print(Aleri.PrintColour..'use \\aleri plus any of the following')
		print(Aleri.PrintColour..'-setscaleX.X, this will set Aleri\'s scale to the value of X.X (0.5 - 1.5)')
		print(Aleri.PrintColour..'-getxpinfo, prints out XP info (% of level, XP required, rested XP remaining)')
		print(Aleri.PrintColour..'-getilvl, prints out your current ilvl')
		
	elseif string.find(msg, '-getxpinfo') then
		local xpmx = UnitXPMax('player') 
		local xp = UnitXP('player') 
		local d = GetXPExhaustion() 
		print(Aleri.PrintColour..'Aleri - XP info for '..UnitName('player'))
		print(Aleri.PrintColour..'XP this level '..string.format("%.2f", (xp/xpmx)*100)..'%')
		print(Aleri.PrintColour..'XP required to level up '..(xpmx-xp))
		print(Aleri.PrintColour..'You have '..d..' as rested XP')
		
	elseif string.find(msg, '-getilvl') then
		local total, count = 0, 0
		for i = 1,19 do
			local link = GetInventoryItemLink('player', i)
			if link then
				local ilvl = select(4, GetItemInfo(link))
				total = total + ilvl
				count = count + 1
			end
		end
		print(Aleri.PrintColour..'Aleri - you have '..count..' items equiped, with an average ilvl of '..string.format('%.2f', total/count))

	elseif string.find(msg, '-setscale') then
		local scale  = (string.sub(msg, 10, 12))
		if string.find(scale, ',') then scale = tostring(string.sub(scale, 1, 1)..'.'..string.sub(scale, 3, 3)) end
		scale = tonumber(scale)
		if scale > 1.5 then 
			scale = 2
		elseif scale < 0.5 then
			scale = 0.5
		end
		Aleri.Guild.PlayerFrame:SetScale(scale)
	end
end