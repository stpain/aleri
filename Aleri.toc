## Interface: 30300
## Title: Aleri
## Author: Elementia
## Notes: Guild addon 
## Version: 3.1.2
## SavedVariables: AleriGlobalSettings
## SavedVariablesPerCharacter: AleriCharacterSettings

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDbIcon-1.0\LibDbIcon-1.0.lua

Aleri_Locales.lua
Aleri_Guides.lua
Aleri_DB.lua
Aleri_SDK.lua
Aleri_Core.lua

Aleri.xml
