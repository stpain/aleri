--[==[
    Aleri

    All Rights Reserved 
    
    © stpain 2020



    =================================
    Contents
    =================================

    1. locals and constants
    2. config table
    3. character class
    4. view
    5. model
    6. player
    7. minimap button
    8. slash commands
    9. xml
    10. summary frame
    11. roster frame
    12. profile frame
    13. profession frame
    14. raid info
    15. dropdown init functions
    16. init function
    17. dialogs
    18. OnEvents
    19. OnUpdate



]==]--

local AleriName, Aleri = ...
local L = Aleri.Locales

local tinsert = table.insert
local tsort = table.sort
local tconcat = table.concat

local DEBUG = Aleri.SDK.Util.Debug
local INFO_PRINT = Aleri.SDK.Util.Print

local INIT_DELAY_SECONDS = 7.0
local DB_LOADED = false
local PLAYER_GUID = nil
local PLAYER_NAME = nil
local PLAYER_RACE = nil
local PLAYER_CLASS = nil
local PLAYER_GENDER = nil
local GUILD_NAME = nil
local GEARSCORE_LOADED = false
local MINIMAP_ICON_CREATED = false
local PROFESSION_SHARING = nil
local OPENED_TAB = 1

local ALERI_LOADED = function()
    local l = true
    if DB_LOADED == false then
        l = false
    end
    if GUILD_NAME == nil then
        l = false
    end
    if PLAYER_NAME == nil then
        l = false
    end
    DEBUG(tostring('aleri-loaded-func: '..tostring(l)))
    return l
end

Aleri.Debug = false












--=============================================================================================================================================================
--CONFIG TABLES
--=============================================================================================================================================================
local Config = {
    PrintFontColour = '|cff0070DE',
    ClassSummaryListView = {
        ListViewItem = {
            VerticalOffset = 65.0,
            Height = 22,
            Width = 135,
            IconSize = 22,
            IconPosX = 1,
            StatusBarPosX = 25,
            TextPosX = 0,
            TextFont = "Fonts\\FRIZQT__.TTF",
            TextInherit = "GameFontNormal",
            TextFontSize = 9,
            CountFontStringPosX = 200,
            StatusBarHeight = 18,
            StatusBarWidth = 100,
        },

    },
    GuildMemberListView = {
        ListViewItem = {
            VerticalOffset = 90,
            Height = 20,
            Width = 450,
            ClassIconSize = 20,
            ProfIconSize = 20,
            TextFont = "Fonts\\FRIZQT__.TTF",
            TextInherit = "GameFontNormal",
            TextFontSize = 11,
            ClassIconPosX = 15,
            NamePosX = 50,
            LevelPosX = 175,
            RolePosX = 220,
            InfoPosX = 265,
            MainSpecPosX = 226,
            OffSpecPosX = 334,
        },
    },
    ProfessionsMemberListView = {
        ListViewItem = {
            VerticalOffset = 0,
            Height = 20,
            Width = 140,
            ClassIconSize = 20,
            ProfIconSize = 20,
            TextFont = "Fonts\\FRIZQT__.TTF",
            TextInherit = "GameFontNormal",
            TextFontSize = 12,
            NamePosX = 5,
        },
    },
    ProfessionsRecipeListView = {
        ListViewItem = {
            VerticalOffset = 0,
            Height = 20,
            Width = 260,
            ClassIconSize = 20,
            ProfIconSize = 20,
            TextFont = "Fonts\\FRIZQT__.TTF",
            TextInherit = "GameFontNormal",
            TextFontSize = 11,
            NamePosX = 0,
        },
    },    
}










--=============================================================================================================================================================
--CHARACTER CLASS
--=============================================================================================================================================================
local Character = {
    character = {
        Name = '-', 
        Level = 1, 
        ItemLevel = 0.0, 
        GearScore = 0.0, 
        Class = '-', 
        Race = '-', 
        Gender = '-',
        MainSpec = '-',
        OffSpec = '-',
        Fishing = 0, 
        Cooking = 0, 
        FirstAid = 0, 
        Profession1 = '-', 
        Profession1Level = 0, 
        Profession2 = '-', 
        Profession2Level = 0,
        INVSLOT_HEAD = 0,
        INVSLOT_NECK = 0,
        INVSLOT_SHOULDER = 0,
        INVSLOT_CHEST = 0,
        INVSLOT_WAIST = 0,
        INVSLOT_LEGS = 0,
        INVSLOT_FEET = 0,
        INVSLOT_WRIST = 0,
        INVSLOT_HAND = 0,
        INVSLOT_FINGER1 = 0,
        INVSLOT_FINGER2 = 0,
        INVSLOT_TRINKET1 = 0,
        INVSLOT_TRINKET2 = 0,
        INVSLOT_BACK = 0,
        INVSLOT_MAINHAND = 0,
        INVSLOT_OFFHAND = 0,
        INVSLOT_RANGED = 0,  
        Info = '-',
        MainCharacter = '-',
    },
    New = function(self, name, level, ilvl, gs, class, race, gender, ms, os, fish, cook, fa, prof1, prof1L, prof2, prof2L, invHead, invNeck, invShoulder, invChest, invWaist, invLeg, invFeet, invWrist, invHand, invFinger1, invFinger2, invtrinket1, invTrinket2, invBack, invMainHand, invOffHand, invRanged, info, mainChar)
        local self = {}
        self.Name = tostring(name)
        self.Level = tonumber(level) or 1
        self.ItemLevel = tonumber(ilvl)
        self.GearScore = tonumber(gs)
        self.Class = tostring(class)
        self.Race = tostring(race)
        self.Gender = tostring(gender)
        self.MainSpec = tostring(ms)
        self.OffSpec = tostring(os)
        self.Fishing = tonumber(fish)
        self.Cooking = tonumber(cook)
        self.FirstAid = tonumber(fa)
        self.Profession1 = tostring(prof1)
        self.Profession1Level = tonumber(prof1L)
        self.Profession2 = tostring(prof2)
        self.Profession2Level = tonumber(prof2L)
        self.INVSLOT_HEAD = tonumber(invHead)
        self.INVSLOT_NECK = tonumber(invNeck)
        self.INVSLOT_SHOULDER = tonumber(invShoulder)
        self.INVSLOT_CHEST = tonumber(invChest)
        self.INVSLOT_WAIST = tonumber(invWaist)
        self.INVSLOT_LEGS = tonumber(invLeg)
        self.INVSLOT_FEET = tonumber(invFeet)
        self.INVSLOT_WRIST = tonumber(invWrist)
        self.INVSLOT_HAND = tonumber(invHand)
        self.INVSLOT_FINGER1 = tonumber(invFinger1)
        self.INVSLOT_FINGER2 = tonumber(invFinger2)
        self.INVSLOT_TRINKET1 = tonumber(invtrinket1)
        self.INVSLOT_TRINKET2 = tonumber(invTrinket2)
        self.INVSLOT_BACK = tonumber(invBack)
        self.INVSLOT_MAINHAND = tonumber(invMainHand)
        self.INVSLOT_OFFHAND = tonumber(invOffHand)
        self.INVSLOT_RANGED = tonumber(invRanged)
        self.Info = tostring(info)
        self.MainCharacter = tostring(mainChar)
        DEBUG('created new character instance for '..name)
        return setmetatable(self, character)
    end,
}
Character.character.__index = Character.character










--=============================================================================================================================================================
--VIEW - XML LISTVIEW COLLECTIONS
--=============================================================================================================================================================
local View = {
    SummaryFrameClassListView = {},
    RosterFrameGuildMembersListView = {},
    ProfessionFrameMemberListView = {},
    ProfessionFrameMemberRecipeListView = {},
}










--=============================================================================================================================================================
--MODEL - DATA MANAGER
--=============================================================================================================================================================
local Model = {
    GuildMembers = {},
    ProfessionMembers = {},
    ProfessionRecipeMembers = {},
    ProfessionSelected = nil,
    SharedProfessionRecipesSelected = {},
    SelectedRaidInstanceInfo = {},
    RaidInstanceGuides = nil,
    ClassSummary = {
        { Class = 'DEATHKNIGHT', Count = 0 },
		{ Class = 'DRUID', Count = 0 },
		{ Class = 'HUNTER', Count = 0 },
		{ Class = 'MAGE', Count = 0 },
		{ Class = 'PALADIN', Count = 0 },
		{ Class = 'PRIEST', Count = 0 },
		{ Class = 'ROGUE', Count = 0 },
		{ Class = 'SHAMAN', Count = 0 },
		{ Class = 'WARLOCK', Count = 0},
		{ Class = 'WARRIOR', Count  = 0 },
    },
    ResetClassCount = function(self)
        for k, class in ipairs(self.ClassSummary) do
            class.Count = 0
        end
    end,
    UpdateClassSummary = function(self, minLevel)
        if ALERI_LOADED() then
            if not minLevel then
                minLevel = 1
            end
            self:ResetClassCount()
            local view = GetGuildRosterShowOffline() --keep the players settings
            SetGuildRosterShowOffline(true) --set to all members
            GuildRoster()
            local TotalMembers, OnlineMembers, _ = GetNumGuildMembers()
            for i = 1, TotalMembers do
                local name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, classID, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)
                if tonumber(level) >= tonumber(minLevel) then
                    for k, class in ipairs(self.ClassSummary) do
                        if (class.Class == classID) then
                            class.Count = class.Count + 1
                        end
                    end
                end
            end
            if #self.ClassSummary > 0 then
                table.sort(self.ClassSummary, function(a,b) return a.Count > b.Count end)
                for k, class in ipairs(self.ClassSummary) do
                    View.SummaryFrameClassListView[class.Class]:SetPoint('TOPLEFT', 20, (((k*-1)*Config.ClassSummaryListView.ListViewItem.Height) - Config.ClassSummaryListView.ListViewItem.VerticalOffset))
                    View.SummaryFrameClassListView[class.Class].StatusBar:SetValue((class.Count / TotalMembers) * 100)
                    View.SummaryFrameClassListView[class.Class].Count = tostring(class.Count) --..' ['..string.format('%.1f', ((class.Count / TotalMembers) * 100))..'%]')
                    View.SummaryFrameClassListView[class.Class].Text:SetText(tostring(string.format('%.1f', ((class.Count / TotalMembers) * 100))..'%'))
                end
            end
            SetGuildRosterShowOffline(view) --return to player setting
        end
    end,
    WipeGuildMembers = function(self)
        self.GuildMembers = {}
    end,
    ClearRosterFrameGuildMemberListView = function(self)
        for i = 1, 10 do
            View.RosterFrameGuildMembersListView[i]:Hide()
            View.RosterFrameGuildMembersListView[i].Character = nil
        end
    end,
    UpdateRosterFrameGuildMemberListView = function(self, key, collection)
        if (ALERI_LOADED() == true) and (type(collection) == 'table') then
            self:ClearRosterFrameGuildMemberListView()
            key = math.ceil(key)
            local i, lower, upper = 1, ((key * 10) - 9), ((key * 10) + 1)
            --for k, member in ipairs(collection) do			
            for k, member in ipairs(self.GuildMembers) do
                if k >= lower and k < upper then
                    View.RosterFrameGuildMembersListView[i].Character = member
                    View.RosterFrameGuildMembersListView[i]:Show()
                    i = i + 1
                end
            end     
        else
            DEBUG('update roster frame listview failed - database not loaded')
        end
    end,
    ScanGuildRoster = function(self)
        if ALERI_LOADED() == true then
            DEBUG('db loaded, scanning roster')
            local showOffline = GetGuildRosterShowOffline() --keep the players settings
            SetGuildRosterShowOffline(true)
            self:WipeGuildMembers()
            GuildRoster()
            local TotalMembers, OnlineMembers, _ = GetNumGuildMembers()
            for i = 1, TotalMembers do
                local name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)
                local yearsOffline, monthsOffline, daysOffline, hoursOffline = GetGuildRosterLastOnline(i)
                if monthsOffline ~= nil and tonumber(monthsOffline) >= 1 then
                    --print(name..' has been offline for a month')
                end
                class = string.upper(class)
                if class == 'DEATH KNIGHT' then class = 'DEATHKNIGHT' end
                tinsert(self.GuildMembers, { Name = name, Class = class, Level = tonumber(level), Info = publicNote, Zone = zone, Online = isOnline, AchievementPoints = tonumber(achievementPoints) } )
                --also add to aleri db
                if AleriGlobalSettings[GUILD_NAME].Members[name] == nil then
                    AleriGlobalSettings[GUILD_NAME].Members[name] = Character:New(name, level, 0.0, 0.0, class, '-', '-', '-', '-', 0.0, 0.0, 0.0, '-', 0.0, '-', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, publicNote, '-')
                    DEBUG('added: '..name..' to global db during guild scan')
                else --if exists update public note and level
                    AleriGlobalSettings[GUILD_NAME].Members[name].Level = tonumber(level)
                    AleriGlobalSettings[GUILD_NAME].Members[name].Info = publicNote
                end
            end
            --scan aleri database and load back data in model
            for i, member in ipairs(self.GuildMembers) do
                if AleriGlobalSettings[GUILD_NAME].Members[member.Name] then
                    DEBUG('found:'..member.Name..' in aleri database, checking data against current model data')
                    for k, info in pairs(AleriGlobalSettings[GUILD_NAME].Members[member.Name]) do
                        if type(k) ~= 'table' then
                            if member[k] == nil then
                                member[k] = info
                                DEBUG('found character data in aleri database, adding to model')
                            end
                        elseif type(k) == 'table' then --does this need to be seperated ?
                            member[k] = info
                        end
                    end
                end
            end
            SetGuildRosterShowOffline(showOffline) --return to player setting
            AleriInterfaceRosterFrameScrollBar:SetMinMaxValues(1, math.ceil(TotalMembers / 10))
        else
            DEBUG('scan guild roster failed - database not loaded')
        end
    end,
    PurgeDatabase = function(self)
        local dbStartCount, playersRemoved = 0, 0
        for name, _ in pairs(AleriGlobalSettings[GUILD_NAME].Members) do
            dbStartCount = dbStartCount + 1
            local exists = false
            for k, member in ipairs(self.GuildMembers) do
                if member.Name == name then
                    exists = true
                end
            end
            if exists == false then
                --AleriGlobalSettings[GUILD_NAME].Members[name] = nil
                DEBUG('removed: '..name)
                playersRemoved = playersRemoved + 1
            end
        end
        INFO_PRINT('purge completed, removed: '..playersRemoved..' players from database')
    end,
    GetGlobalSettingsGuildMemberCount = function(self)
        local c = 0.0
        if ALERI_LOADED() == true then
            for k, member in pairs(AleriGlobalSettings[GUILD_NAME].Members) do
                c = c + 1
            end
        end
        return tonumber(c)
    end,
    SortGuildMembers = function(self, sort)
        if ALERI_LOADED() == true then
            DEBUG('sorting roster by '..sort)
            if self.GuildMembers ~= nil then
                if (sort == 'Class') then
                    tsort(self.GuildMembers, function(a,b) 
                        if a[sort] == b[sort] then
                            if a.Level == b.Level then
                                return a.Name < b.Name
                            else
                                return (a.Level > b.Level) 
                            end
                        else 
                            return a[sort] < b[sort] 
                        end 
                    end)
                elseif (sort == 'Name') or (sort == 'Info') then
                    tsort(self.GuildMembers, function(a,b) if a[sort] == b[sort] then return (a.Level > b.Level) else return a[sort] < b[sort] end end)
                elseif (sort == 'Level') then
                    tsort(self.GuildMembers, function(a,b) if a[sort] == b[sort] then return (a.Name < b.Name) else return a[sort] > b[sort] end end)
                elseif (sort == 'Role') then
                    DEBUG('global database members count > 0, scanning for MainSpec data')
                    tsort(self.GuildMembers, function(a,b) 
                        if Aleri.DB.SpecToRole[a.Class][a.MainSpec] == Aleri.DB.SpecToRole[b.Class][b.MainSpec] then 
                            return a.Level > b.Level
                        else
                            return Aleri.DB.SpecToRole[a.Class][a.MainSpec] > Aleri.DB.SpecToRole[b.Class][b.MainSpec]
                        end
                    end)
                    self:UpdateRosterFrameGuildMemberListView(1, self.GuildMembers)
                    AleriInterfaceRosterFrameScrollBar:SetMinMaxValues(1, #self.GuildMembers)
                end
            else
                --something else
            end
        end
    end,
    FilterGuildMembersByOnline = function(self, online)
        if online == true or online == 1 then
            local filteredGuildMembers, result = {}, false
            for k, member in ipairs(self.GuildMembers) do
                if member.Online == true or member.Online == 1 then
                    tinsert(filteredGuildMembers, member)
                    result = true
                end
            end
            if result == true then
                self:WipeGuildMembers()
                self.GuildMembers = filteredGuildMembers
                self:UpdateRosterFrameGuildMemberListView(1, self.GuildMembers) 
                AleriInterfaceRosterFrameScrollBar:SetMinMaxValues(1, math.ceil(#self.GuildMembers / 10))
                AleriInterfaceRosterFrameScrollBar:SetValue(1)
                self:SortGuildMembers('Name')
            end
            filteredGuildMembers = nil
        else
            self:WipeGuildMembers()
            self:ScanGuildRoster()
            self:SortGuildMembers('Name')
            self:UpdateRosterFrameGuildMemberListView(1, self.GuildMembers)
            AleriInterfaceRosterFrameScrollBar:SetValue(1)
            result = false
        end
    end,
    GetMembersByProfession = function(self, profession)
        self.ProfessionMembers = nil
        self.ProfessionMembers = {}
        self.ProfessionSelected = profession
        self:ClearProfessionFrameMemberListView()
        if ALERI_LOADED() == true then
            for k, member in ipairs(self.GuildMembers) do
                if (tostring(member.Profession1):lower() == tostring(profession):lower()) or (tostring(member.Profession2):lower() == tostring(profession):lower()) then
                    tinsert(self.ProfessionMembers, member)
                    --print('found matching prof', member.Name, profession)
                end
            end
            if #self.ProfessionMembers > 0 then
                self:UpdateProfessionFrameMemberListView(1)
                --print('prof member list > 0')
            else
                DEBUG('no profession matches found')
            end
        end
    end,
    ClearProfessionFrameMemberListView = function(self)
        for i = 1, 10 do
            View.ProfessionFrameMemberListView[i]:Hide()
            View.ProfessionFrameMemberListView[i].Character = nil
        end
    end,
    UpdateProfessionFrameMemberListView = function(self, key)
        if ALERI_LOADED() == true then
            self:ClearProfessionFrameMemberListView()
            key = math.ceil(key)
            local i, lower, upper = 1, ((key * 10) - 9), ((key * 10) + 1)
            for k, member in ipairs(self.ProfessionMembers) do
                if k >= lower and k < upper then
                    View.ProfessionFrameMemberListView[i].Character = member
                    View.ProfessionFrameMemberListView[i]:Show()
                    i = i + 1
                end
            end     
        else
            DEBUG('update profession frame listview failed - database not loaded')
        end
    end,    
    SetProfessionRecipes = function(self, character)
        if ALERI_LOADED() then
            --print(character, self.ProfessionSelected)
            if type(AleriGlobalSettings[GUILD_NAME].Members[character].ProfessionRecipes) == 'table' then
                --print('found character global prof table')
                if AleriGlobalSettings[GUILD_NAME].Members[character].ProfessionRecipes[self.ProfessionSelected] ~= nil then
                    --print('found character recipe list')
                    self.SharedProfessionRecipesSelected = {}
                    for Id, _ in pairs(AleriGlobalSettings[GUILD_NAME].Members[character].ProfessionRecipes[self.ProfessionSelected]) do
                        if Id then
                            local name, link, rarity, level = GetItemInfo(tonumber(Id))
                            tinsert(self.SharedProfessionRecipesSelected, { ID = Id, Name = name, Link = link, Rarity = rarity, Level = level})
                            --print('added,', name, level)
                        end
                    end
                    if #self.SharedProfessionRecipesSelected > 0 then
                        tsort(self.SharedProfessionRecipesSelected, function(a, b)
                            if a.Rarity == b.Rarity then
                                return a.Level > b.Level
                            else
                                return a.Rarity > b.Rarity
                            end
                        end)
                        AleriInterfaceProfessionFrameRecipesListViewScrollBar:SetMinMaxValues(1, math.ceil(#self.SharedProfessionRecipesSelected / 10))
                        AleriInterfaceProfessionFrameRecipesListViewScrollBar:SetValue(1) 
                    end
                    self:UpdateProfessionFrameRecipeListView(1)
                else
                    INFO_PRINT('unable to show data for this profession', Config.PrintFontColour)
                end
            end
        end
    end,
    ClearProfessionFrameRecipeListView = function(self)
        for i = 1, 10 do
            View.ProfessionFrameMemberRecipeListView[i]:Hide()
            View.ProfessionFrameMemberRecipeListView[i].Recipe = nil
        end
    end,    
    UpdateProfessionFrameRecipeListView = function(self, key)
        if ALERI_LOADED() == true and type(key) == 'number' then
            self:ClearProfessionFrameRecipeListView()
            local c = 0
            if self.SharedProfessionRecipesSelected ~= nil and #self.SharedProfessionRecipesSelected > 0 then
                key = math.ceil(key)
                local i, lower, upper = 1, ((key * 10) - 9), ((key * 10) + 1)
                for k, recipe in ipairs(self.SharedProfessionRecipesSelected) do
                    if k >= lower and k < upper then
                        View.ProfessionFrameMemberRecipeListView[i].Recipe = recipe.ID
                        View.ProfessionFrameMemberRecipeListView[i].RecipeText:SetText(recipe.Link)
                        View.ProfessionFrameMemberRecipeListView[i]:Show()
                        i = i + 1
                    end
                end          
            end 
        else
            DEBUG('update profession frame listview failed - database not loaded')
        end
    end, 
    FilterGuildMembersByName = function(self, filter)
        local filteredGuildMembers, result = {}, false
        for k, member in ipairs(self.GuildMembers) do
            if string.find(member.Name:lower(), filter:lower()) then
                tinsert(filteredGuildMembers, member)
                result = true
            end
        end
        if result == true then
            self:WipeGuildMembers()
            self.GuildMembers = filteredGuildMembers
            self:UpdateRosterFrameGuildMemberListView(1, self.GuildMembers)
            AleriInterfaceRosterFrameScrollBar:SetMinMaxValues(1, math.ceil(#self.GuildMembers / 10))
            AleriInterfaceRosterFrameScrollBar:SetValue(1)
            self:SortGuildMembers('Name')
        end
        filteredGuildMembers = nil
    end,
    UpdateCharacterByKey = function(self, character, key, data, dataType)
        if ALERI_LOADED() == true then
            if AleriGlobalSettings[GUILD_NAME].Members[character] then
                if dataType == 'string' then
                    AleriGlobalSettings[GUILD_NAME].Members[character][key] = tostring(data)
                    INFO_PRINT(tostring('recieved and update for '..character..', '..key..': '..data))
                elseif dataType == 'number' then
                    AleriGlobalSettings[GUILD_NAME].Members[character][key] = tonumber(data)
                    INFO_PRINT(tostring('recieved and update for '..character..', '..key..': '..data))
                end
            end
        end
    end,
    ParseRecipeIDs = function(self, dataString, sender)
        if ALERI_LOADED() == true then
            local i = 1
            local data_sent = {}
            for d in string.gmatch(dataString, '[^:]+') do
                data_sent[i] = d
                i = i + 1
            end
            local profession = nil
            for k, v in pairs(Aleri.DB.Professions) do
                if v.ID == tonumber(data_sent[1]) then
                    profession = v.Name
                end
            end
            if profession then
                if profession and AleriGlobalSettings[GUILD_NAME].Members[sender] then    
                    if AleriGlobalSettings[GUILD_NAME].Members[sender].ProfessionRecipes == nil then
                        AleriGlobalSettings[GUILD_NAME].Members[sender].ProfessionRecipes = {}
                    end            
                    if AleriGlobalSettings[GUILD_NAME].Members[sender].ProfessionRecipes[profession] == nil then
                        AleriGlobalSettings[GUILD_NAME].Members[sender].ProfessionRecipes[profession] = {}
                    end
                    for i = 2, #data_sent do
                        AleriGlobalSettings[GUILD_NAME].Members[sender].ProfessionRecipes[profession][data_sent[i]] = true
                    end
                end
            end
        end
    end,
    ParseCharacterData = function(self, dataString)
        DEBUG('parsing character data')
        if ALERI_LOADED() == true then
            local i = 1
            local data_sent = {}
            for d in string.gmatch(dataString, '[^:]+') do
                data_sent[i] = d
                DEBUG(i..': '..data_sent[i])
                i = i + 1
            end
            if AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]] == nil then
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]] = { --this sets the character table
                    Name = tostring(data_sent[1]),
                    Level = tonumber(data_sent[2]),
                    ItemLevel = tonumber(data_sent[3]),
                    GearScore = tonumber(data_sent[4]),              
                    Class = tostring(data_sent[5]),          
                    Race = tostring(data_sent[6]),
                    Gender = tostring(data_sent[7]),
                    MainSpec = tostring(data_sent[8]),
                    OffSpec = tostring(data_sent[9]),
                    Fishing = tonumber(data_sent[10]),
                    Cooking = tonumber(data_sent[11]),              
                    FirstAid = tonumber(data_sent[12]),
                    Profession1 = tostring(data_sent[13]),
                    Profession1Level = tonumber(data_sent[14]),
                    Profession2 = tostring(data_sent[15]),
                    Profession2Level = tonumber(data_sent[16]),
                    INVSLOT_HEAD = tonumber(data_sent[17]),
                    INVSLOT_NECK = tonumber(data_sent[18]),
                    INVSLOT_SHOULDER = tonumber(data_sent[19]),
                    INVSLOT_CHEST = tonumber(data_sent[20]),
                    INVSLOT_WAIST = tonumber(data_sent[21]),
                    INVSLOT_LEGS = tonumber(data_sent[22]),
                    INVSLOT_FEET = tonumber(data_sent[23]),
                    INVSLOT_WRIST = tonumber(data_sent[24]),
                    INVSLOT_HAND = tonumber(data_sent[25]),
                    INVSLOT_FINGER1 = tonumber(data_sent[26]),
                    INVSLOT_FINGER2 = tonumber(data_sent[27]),
                    INVSLOT_TRINKET1 = tonumber(data_sent[28]),
                    INVSLOT_TRINKET2 = tonumber(data_sent[29]),
                    INVSLOT_BACK = tonumber(data_sent[30]),
                    INVSLOT_MAINHAND = tonumber(data_sent[31]),
                    INVSLOT_OFFHAND = tonumber(data_sent[32]),
                    INVSLOT_RANGED = tonumber(data_sent[33]),
                    Info = tostring(data_sent[34]),
                    MainCharacter = tostring(data_sent[35]),
                    ProfessionRecipes = nil,
                }
            else --this is a wall of nasty tidy it up MUST AVOID OVERWRITING THE WHOLE CHARACTER ENTRY AS THIS REMOVES THE PROFESSIONS DATA
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Name = tostring(data_sent[1])                
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Level = tonumber(data_sent[2])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].ItemLevel = tonumber(data_sent[3])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].GearScore = tonumber(data_sent[4])  
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Class = tostring(data_sent[5])      
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Race = tostring(data_sent[6])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Gender = tostring(data_sent[7])                
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].MainSpec = tostring(data_sent[8])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].OffSpec = tostring(data_sent[9])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Fishing = tonumber(data_sent[10])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Cooking = tonumber(data_sent[11])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].FirstAid = tonumber(data_sent[12])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Profession1 = tostring(data_sent[13])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Profession1Level = tonumber(data_sent[14])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Profession2 = tostring(data_sent[15])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Profession2Level = tonumber(data_sent[16])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_HEAD = tonumber(data_sent[17])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_NECK = tonumber(data_sent[18])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_SHOULDER = tonumber(data_sent[19])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_CHEST = tonumber(data_sent[20])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_WAIST = tonumber(data_sent[21])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_LEGS = tonumber(data_sent[22])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_FEET = tonumber(data_sent[23])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_WRIST = tonumber(data_sent[24])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_HAND = tonumber(data_sent[25])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_FINGER1 = tonumber(data_sent[26])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_FINGER2 = tonumber(data_sent[27])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_TRINKET1 = tonumber(data_sent[28])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_TRINKET2 = tonumber(data_sent[29])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_BACK = tonumber(data_sent[30])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_MAINHAND = tonumber(data_sent[31])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_OFFHAND = tonumber(data_sent[32])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].INVSLOT_RANGED = tonumber(data_sent[33])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].Info = tostring(data_sent[34])
                AleriGlobalSettings[GUILD_NAME].Members[data_sent[1]].MainCharacter = tostring(data_sent[35])
            end
        end
    end,
    LoadRaidGuides = function(self)
        if ALERI_LOADED() then
            self.RaidInstanceGuides = {}
            for raid, data in pairs(Aleri.Guides.WOTLK) do
                tinsert(self.RaidInstanceGuides, raid)
            end
            tsort(self.RaidInstanceGuides, function(a, b) 
                return a.Raid < b.Raid
            end)
        end
    end,
    UpdateRaidInfoFrame = function(self, raid, difficulty, guide)
        AleriInterfaceRaidInfoFrameSelectRaidInstanceGearScoreData:SetText(guide['GearScore'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceGemsData:SetText(guide['Gems'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceEnchantsData:SetText(guide['Enchants'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceTanksData:SetText(guide['Tanks'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceMeleeData:SetText(guide['Melee'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceRangedData:SetText(guide['Ranged'])
        AleriInterfaceRaidInfoFrameSelectRaidInstanceHealersData:SetText(guide['Healer'])
        AleriInterfaceRaidInfoFrameSelectedRaidInstanceHeader:SetText(raid..' '..difficulty)
    end,
    CleanGuildMember = function(self, member)
        DEBUG('cleaning character')
        for k, v in pairs(Aleri.DB.CharacterSheet) do --ignores prof table at the moment
            if member[k] == nil then
                DEBUG(tostring('missing data: '..k..' setting to default'))
                member[k] = v
            end
        end
        for k, v in pairs(member) do
            if type(k) ~= 'table' then
                if v == nil or v == '' then
                    DEBUG('found nil value for: '..k..' setting to 0 or -')
                    if string.find(k, 'INVSLOT_') then
                        member[k] = 0
                    else
                        member[k] = '-'
                    end
                end
            end
        end
    end,
    BroadcastDatabase = function(self) --need to be carefull with this, it could overwrite an old database with a fresh null data set
        if ALERI_LOADED() == true then
            INFO_PRINT('starting database broadcast')
            self:ScanGuildRoster()
            for k, member in ipairs(self.GuildMembers) do
                DEBUG('cleaning member: '..member.Name)
                self:CleanGuildMember(member)
                ds = tostring(member.Name..':'..member.Level..':'..member.ItemLevel..':'..member.GearScore..':'..member.Class..':'..member.Race..':'..member.Gender..':'..member.MainSpec..':'..member.OffSpec..':'..member.Fishing..':'..member.Cooking..':'..member.FirstAid..':'..member.Profession1..':'..member.Profession1Level..':'..member.Profession2..':'..member.Profession2Level..':'..member.INVSLOT_HEAD..':'..member.INVSLOT_NECK..':'..member.INVSLOT_SHOULDER..':'..member.INVSLOT_CHEST..':'..member.INVSLOT_WAIST..':'..member.INVSLOT_LEGS..':'..member.INVSLOT_FEET..':'..member.INVSLOT_WRIST..':'..member.INVSLOT_HAND..':'..member.INVSLOT_FINGER1..':'..member.INVSLOT_FINGER2..':'..member.INVSLOT_TRINKET1..':'..member.INVSLOT_TRINKET2..':'..member.INVSLOT_BACK..':'..member.INVSLOT_MAINHAND..':'..member.INVSLOT_OFFHAND..':'..member.INVSLOT_RANGED..':'..member.Info..':'..member.MainCharacter)
                DEBUG(ds)
                --SendAddonMessage('aleri-charData', ds, 'GUILD')
            end

        end
    end,
}










--=============================================================================================================================================================
--PLAYER - PLAYER INFO MANAGER
--=============================================================================================================================================================
local Player = {
    Character = nil,
    GetXPInfo = function(self)
        local level, xp, xpMax, xpRested = UnitLevel('player'), UnitXP('player'), UnitXPMax('player'), GetXPExhaustion() or 0
        return ('Level: '..level..' '..string.format("%.2f", (xp/xpmx) * 100)..'%, XP To next level: '..string.format('%.2f', tonumber(xpmx-xp))..'%, Rested XP: '..string.format('%.2f',tonumber(xpr)))
    end,
    GetCharacterData = function(self)
        if ALERI_LOADED() == true then
            --get basic data
            local _, class, _, race, gender, name, realm = GetPlayerInfoByGUID(UnitGUID('player'))
            if name == nil then
                StaticPopup_Show('Aleri_GetGUIDInfo')
                return
            end
            --fix return values
            if class == 'DEATH KNIGHT' then class = 'DEATHKNIGHT' end
            if gender == 3 then
                gender = 'FEMALE'
            else
                gender = 'MALE'
            end
            --create character if not exists
            if self.Character == nil then
                if AleriGlobalSettings[GUILD_NAME].Members[PLAYER_NAME] == nil then
                    self.Character = Character:New(name, UnitLevel('player'), 0.0, 0.0, class, race, gender, '-', '-', 0.0, 0.0, 0.0, '-', 0.0, '-', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, '-', '-')
                    INFO_PRINT('created new character sheet for: '..name, Config.PrintFontColour)
                else
                    self.Character = AleriGlobalSettings[GUILD_NAME].Members[PLAYER_NAME]
                    self:CleanMyCharacter()
                    INFO_PRINT(tostring('loaded character sheet for '..self.Character.Name..' scanning for new character data'), Config.PrintFontColour)
                    self.Character.Level = UnitLevel('player')
                end
            end
            --scrap skills/professions TODO: check locales for profession names?
            for s = 1, GetNumSkillLines() do
                local skill, _, _, level, _, _, _, _, _, _, _, _, _ = GetSkillLineInfo(s)
                if skill == 'Fishing' then 
                    self.Character.Fishing = level
                elseif skill == 'Cooking' then
                    self.Character.Cooking = level
                elseif skill == 'First Aid' then
                    self.Character.FirstAid = level
                else
                    for k, prof in pairs(Aleri.DB.Professions) do
                        if skill == prof.Name then
                            if (self.Character.Profession1 == '-') or (self.Character.Profession1  == skill) then
                                self.Character.Profession1 = skill
                                self.Character.Profession1Level = level
                            elseif (self.Character.Profession2 == '-') or (self.Character.Profession2  == skill) then
                                self.Character.Profession2 = skill
                                self.Character.Profession2Level = level
                            end
                        end
                    end
                end
            end
            --get gear, ilvl and gs data
            local itemlvl = 0.0
            local itemCount = 0.0
            for k, s in ipairs(Aleri.DB.InventorySlots) do
                self.Character[s.Name] = GetInventoryItemID('player', s.Id)
                if self.Character[s.Name] ~= nil then
                    local iName, iLink, iRarety, ilvl = GetItemInfo(self.Character[s.Name])
                    itemlvl = itemlvl + ilvl
                    itemCount = itemCount + 1
                else
                    self.Character[s.Name] = 0
                end
            end
            self.Character.ItemLevel = tonumber(string.format("%.2f", itemlvl/itemCount)) -- round down or format 2dp ??
            if GEARSCORE_LOADED == true then
                self.Character.GearScore = select(1, GearScore_GetScore(self.Character.Name, "player"))
            else
                INFO_PRINT('unable to get GearScore as GearScoreLite wasn\'t detected, to use this feature please install GearScoreLite.')
            end
            --get public note
            Model:ScanGuildRoster()
            for k, member in ipairs(Model.GuildMembers) do
                if member.Name == self.Character.Name then
                    self.Character.Info = member.Info
                    if self.Character.Info == nil then
                        self.Character.Info = '-'
                    end
                end
            end
        end
        self:SaveCharacterData() --commit to database
        --return self.Character
    end,
    LoadCharacterData = function(self)
        if ALERI_LOADED() == true then
            if AleriGlobalSettings[GUILD_NAME].Members[PLAYER_NAME] == nil then
                self:GetCharacterData()
                self:SaveCharacterData()
                INFO_PRINT(tostring('created character sheet for '..self.Character.Name), Config.PrintFontColour)
            else
                self:GetCharacterData() --this will get latest character data and load into Player.Character
                INFO_PRINT(tostring('loaded character sheet for '..self.Character.Name), Config.PrintFontColour)

            end
        end
    end,
    SaveCharacterData = function(self)
        if ALERI_LOADED() == true and self.Character.Name ~= nil then
            self:CleanMyCharacter()
            AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name] = self.Character
            INFO_PRINT('saved character data', Config.PrintFontColour)
            SendAddonMessage('aleri-charData', self:GenerateCharacterDataString(), 'GUILD') --broadcast newest data
        end
    end,
    CleanMyCharacter = function(self)
        DEBUG('cleaning character')
        for k, v in pairs(Aleri.DB.CharacterSheet) do --ignores prof table at the moment
            if self.Character[k] == nil then
                DEBUG(tostring('missing data: '..k..' setting to default'))
                self.Character[k] = v
            end
        end
        for k, v in pairs(self.Character) do
            if type(k) ~= 'table' then
                if v == nil or v == '' then
                    DEBUG('found nil value for: '..k..' setting to 0 or -')
                    if string.find(k, 'INVSLOT_') then
                        self.Character[k] = 0
                    else
                        self.Character[k] = '-'
                    end
                end
            end
        end
    end,
    GenerateCharacterDataString = function(self)
        if ALERI_LOADED() == true then
            self:CleanMyCharacter()
            ds = tostring(self.Character.Name..':'..self.Character.Level..':'..self.Character.ItemLevel..':'..self.Character.GearScore..':'..self.Character.Class..':'..self.Character.Race..':'..self.Character.Gender..':'..self.Character.MainSpec..':'..self.Character.OffSpec..':'..self.Character.Fishing..':'..self.Character.Cooking..':'..self.Character.FirstAid..':'..self.Character.Profession1..':'..self.Character.Profession1Level..':'..self.Character.Profession2..':'..self.Character.Profession2Level..':'..self.Character.INVSLOT_HEAD..':'..self.Character.INVSLOT_NECK..':'..self.Character.INVSLOT_SHOULDER..':'..self.Character.INVSLOT_CHEST..':'..self.Character.INVSLOT_WAIST..':'..self.Character.INVSLOT_LEGS..':'..self.Character.INVSLOT_FEET..':'..self.Character.INVSLOT_WRIST..':'..self.Character.INVSLOT_HAND..':'..self.Character.INVSLOT_FINGER1..':'..self.Character.INVSLOT_FINGER2..':'..self.Character.INVSLOT_TRINKET1..':'..self.Character.INVSLOT_TRINKET2..':'..self.Character.INVSLOT_BACK..':'..self.Character.INVSLOT_MAINHAND..':'..self.Character.INVSLOT_OFFHAND..':'..self.Character.INVSLOT_RANGED..':'..self.Character.Info..':'..self.Character.MainCharacter)
            DEBUG(ds)
            return ds
        end
    end,
}










--=============================================================================================================================================================
--MINIMAP BUTTON
--=============================================================================================================================================================
Aleri.CreateMinimapIcon = function()
    local ldb = LibStub("LibDataBroker-1.1")
    Aleri.MinimapButtonObject = ldb:NewDataObject(AleriName, {
        type = "data source",
        icon = 'Interface\\Icons\\Spell_Nature_Lightning',
        OnClick = function(self, button)
            if button == 'LeftButton' then
                if AleriInterface:IsVisible() then
                    AleriInterface:Hide()
                else
                    AleriInterface:Show()
                end
            elseif button == "RightButton" and IsAltKeyDown() then
                StaticPopup_Show ('Aleri_Reset')
            elseif button == "RightButton" then
                -- Standard workaround call OpenToCategory twice
                -- https://www.wowinterface.com/forums/showpost.php?p=319664&postcount=2
                --InterfaceOptionsFrame_OpenToCategory(AleriName)
                --InterfaceOptionsFrame_OpenToCategory(AleriName)
            end
        end,
        OnTooltipShow = function(tooltip)
            if not tooltip or not tooltip.AddLine then return end
            tooltip:AddLine("|cff0070DE"..AleriName)
            tooltip:AddLine("|cffFFFFFFLeft click:|r Open")
            tooltip:AddLine("Alt + |cffFFFFFFRight click:|r Reload database")
        end,
    })    
    Aleri.MinimapIcon = LibStub("LibDBIcon-1.0")
    if AleriCharacterSettings['MinimapButton'] == nil then
        AleriCharacterSettings['MinimapButton'] = {}
    end
    Aleri.MinimapIcon:Register(AleriName, Aleri.MinimapButtonObject, AleriCharacterSettings['MinimapButton'])
end










--=============================================================================================================================================================
--SLASH COMMANDS
--=============================================================================================================================================================
SLASH_ALERI1 = '/aleri'
SlashCmdList['ALERI'] = function(msg)
	msg = msg:lower()
	if msg == '-help' then
        INFO_PRINT(L['PrintHelp'], Aleri.PrintFontColour)  
	elseif msg == '-ui' then
        if AleriInterface:IsVisible() then
            AleriInterface:Hide()
        else
            AleriInterface:Show()
        end
    elseif msg == '-debug' then
        Aleri.Debug = not Aleri.Debug
        INFO_PRINT(tostring('debug active, '..tostring(Aleri.Debug)), Aleri.PrintFontColour)
    elseif msg == '-toggle-welcome-message' then
        if ALERI_LOADED() then
            AleriCharacterSettings['ShowWelcomeMessage'] = not AleriCharacterSettings['ShowWelcomeMessage']
            INFO_PRINT(tostring(L['WelcomeMessageToggle']..' '..tostring(AleriCharacterSettings['ShowWelcomeMessage'])))
        end    
    elseif msg == '-purge-db' then
        Model:PurgeDatabase()

	end
end










--=============================================================================================================================================================
--XML DEFINED FUNCTIONS
--=============================================================================================================================================================
function AleriInterface_OnLoad(self)
    --had issues with this so far ???
    local version = GetAddOnMetadata(AleriName, "Version")
    INFO_PRINT(tostring('loaded - version '..version), Config.PrintFontColour)
    AleriInterfaceTab1:SetText(L['Summary'])
    AleriInterfaceTab2:SetText(L['Roster'])
    AleriInterfaceTab3:SetText(L['GuildTrade'])
    AleriInterfaceTab4:SetText(L['Profile'])
    AleriInterfaceTab5:SetText(L['RaidSpecs'])
end

function AleriInterface_OnShow(self)
    if ALERI_LOADED == false then
        --Aleri.Init()
    end
    AleriInterface_Title:SetText(tostring(L['Title']..' - '..L['Summary']))
    Model:ScanGuildRoster()
    --this defaults the opening view to summary
    AleriInterfaceSummaryFrame:Show()
    AleriInterfaceRosterFrame:Hide()
    AleriInterfaceProfileFrame:Hide()
    AleriInterfaceProfessionFrame:Hide()
    AleriInterfaceRaidInfoFrame:Hide()
    PanelTemplates_SetTab(AleriInterface, OPENED_TAB)
end


---------------------------------------------------------------------------------------------------------------------------------------------------------------
--SUMMARY FRAME
function AleriInterfaceSummaryFrame_OnLoad(self)
    if GetGuildRosterMOTD() then
        AleriInterfaceSummaryFrame_Header:SetText(tostring(GetGuildRosterMOTD()))
    else
        AleriInterfaceSummaryFrame_Header:SetText(L['SummaryHeader'])
    end
    local k = 1
    for class, d in pairs(Aleri.DB.Classes) do
        local row = CreateFrame("FRAME", tostring('AleriInterfaceSummaryFrameRow'..k), AleriInterfaceSummaryFrame)
        row:EnableMouse(true)
        row:SetSize(Config.ClassSummaryListView.ListViewItem.Width, Config.ClassSummaryListView.ListViewItem.Height)
        row:SetPoint('TOPLEFT', 20, (((k*-1)*Config.ClassSummaryListView.ListViewItem.Height) - Config.ClassSummaryListView.ListViewItem.VerticalOffset))
        row.Icon = Aleri.SDK.UI.NewIcon(row, tostring('AleriInterfaceSummaryFrame'..'_'..class..'_Icon'), 'LEFT', Config.ClassSummaryListView.ListViewItem.IconPosX, 0, Config.ClassSummaryListView.ListViewItem.IconSize, Config.ClassSummaryListView.ListViewItem.IconSize, Aleri.DB.Classes[class].Icon, true)
        row.StatusBar = Aleri.SDK.UI.NewStatusBar(row, tostring('AleriInterfaceSummaryFrame'..'_'..class..'_StatusBar'), 'LEFT', Config.ClassSummaryListView.ListViewItem.StatusBarPosX, 1, Config.ClassSummaryListView.ListViewItem.StatusBarWidth, Config.ClassSummaryListView.ListViewItem.StatusBarHeight, Aleri.DB.Classes[class].RGB, 0, 100, 50)
        row.Text = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceSummaryFrame'..'_'..class..'_Text'), 'RIGHT', Config.ClassSummaryListView.ListViewItem.TextPosX, 2, '', nil, Config.ClassSummaryListView.ListViewItem.TextFont, Config.ClassSummaryListView.ListViewItem.TextFontSize, Config.ClassSummaryListView.ListViewItem.TextInherit)
        row.Background = row:CreateTexture(nil)
        row.Background:SetPoint('TOPLEFT', 0, 0)
        row.Background:SetPoint('BOTTOMRIGHT', 0, 2) 
        row.Background:SetTexture(d.RGB.r, d.RGB.g, d.RGB.b, 0.06)
        row.Class = class
        row.ClassData = d
        row.Count = nil
        row:SetScript('OnEnter', function(self)
            if self.Count then
                GameTooltip:SetOwner(self, "ANCHOR_NONE")
                GameTooltip:SetPoint("RIGHT", self, 'LEFT')
                GameTooltip:AddDoubleLine(self.ClassData.FontStringIcon..self.ClassData.FontColour..self.Class, self.ClassData.FontColour..self.Count)
                GameTooltip:Show()
            end
        end)
        row:SetScript('OnLeave', function(self)
            GameTooltip:ClearLines()
            GameTooltip:Hide()
        end)        
        View.SummaryFrameClassListView[class] = row
        k = k + 1
    end
end

function AleriInterfaceSummaryFrame_OnShow(self)
    if GetGuildRosterMOTD() then
        AleriInterfaceSummaryFrame_Header:SetText(tostring(GetGuildRosterMOTD()))
    else
        AleriInterfaceSummaryFrame_Header:SetText(L['SummaryHeader'])
    end
    AleriInterface_Title:SetText(tostring(L['Title']..' - '..L['Summary']))
    AleriInterfaceSummaryFrameClassSummaryShowOfflineCBText:SetText(L['ShowOfflineCB'])
    AleriInterfaceSummaryFrameClassSummaryShowOfflineCB.tooltip = '-'
    if ALERI_LOADED() == true then
        Model:UpdateClassSummary()
    end
    OPENED_TAB = 1
end

function AleriInterfaceSummaryFrameClassSummaryMinLevelSlider_OnLoad(self)
    self:SetValue(1)
    _G[self:GetName()].tooltipText = L['ClassSummaryMinLevel']
    _G[self:GetName()..'Low']:SetText('1');
    _G[self:GetName()..'High']:SetText('80');
end

function AleriInterfaceSummaryFrameClassSummaryMinLevelSlider_OnValueChanged(self)
    Model:UpdateClassSummary(self:GetValue())
    _G[self:GetName()..'Text']:SetText(self:GetValue());
end

function AleriInterfaceSummaryFrameClassSummaryShowOfflineCB_OnClick(self)

end

---------------------------------------------------------------------------------------------------------------------------------------------------------------
--ROSTER FRAME
function AleriInterfaceRosterFrame_OnLoad(self)
    AleriInterfaceRosterFrameRescanRosterButton:SetText(L['RescanRoster'])
    AleriInterfaceRosterFrameSortClass_Text:SetText(L['Class'])
    AleriInterfaceRosterFrameSortName_Text:SetText(L['Name'])
    AleriInterfaceRosterFrameSortLevel_Text:SetText(L['Level'])
    AleriInterfaceRosterFrameSortRole_Text:SetText(L['Role'])
    AleriInterfaceRosterFrameSortInfo_Text:SetText(L['Info'])
    AleriInterfaceRosterFrameSearchByNameDesc:SetText(L['SearchName'])
    AleriInterfaceRosterFrameOnlineStatusDesc:SetText(tostring(Aleri.DB.OnlineStatusFontStringIcon.Online..L['Online']..' '..Aleri.DB.OnlineStatusFontStringIcon.Offline..L['Offline']))
    AleriInterfaceRosterFrameShowOfflineCBText:SetText(L['ShowOfflineCB'])
    AleriInterfaceRosterFrameShowOfflineCB:SetChecked(false)
    --AleriInterfaceRosterFrameSearchMemberBySpecDD_Init()
    for i = 1, 10 do
        local row = CreateFrame("FRAME", tostring('AleriInterfaceSummaryFrameRow'..i), AleriInterfaceRosterFrame)
        row:EnableMouse(true)
        row:SetSize(Config.GuildMemberListView.ListViewItem.Width, Config.GuildMemberListView.ListViewItem.Height)
        row:SetPoint('TOPLEFT', 15, (((i*-1)*Config.GuildMemberListView.ListViewItem.Height) - Config.GuildMemberListView.ListViewItem.VerticalOffset))
        row.ClassIcon = Aleri.SDK.UI.NewIcon(row, tostring('AleriInterfaceRosterFrame'..i..'_Icon'), 'LEFT', Config.GuildMemberListView.ListViewItem.ClassIconPosX, 0, Config.GuildMemberListView.ListViewItem.ClassIconSize, Config.GuildMemberListView.ListViewItem.ClassIconSize, '', true)
        row.Name =  Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceRosterFrame'..i..'_Name'), 'LEFT', Config.GuildMemberListView.ListViewItem.NamePosX, 2, '', nil, Config.GuildMemberListView.ListViewItem.TextFont, Config.GuildMemberListView.ListViewItem.TextFontSize, Config.GuildMemberListView.ListViewItem.TextInherit)
        row.Level = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceRosterFrame'..i..'_Level'), 'LEFT', Config.GuildMemberListView.ListViewItem.LevelPosX, 2, '', nil, Config.GuildMemberListView.ListViewItem.TextFont, Config.GuildMemberListView.ListViewItem.TextFontSize, Config.GuildMemberListView.ListViewItem.TextInherit)
        row.Role = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceRosterFrame'..i..'_Level'), 'LEFT', Config.GuildMemberListView.ListViewItem.RolePosX, 2, '', nil, Config.GuildMemberListView.ListViewItem.TextFont, Config.GuildMemberListView.ListViewItem.TextFontSize, Config.GuildMemberListView.ListViewItem.TextInherit)
        row.Info = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceRosterFrame'..i..'_MainSpec'), 'LEFT', Config.GuildMemberListView.ListViewItem.InfoPosX, 2, '', nil, Config.GuildMemberListView.ListViewItem.TextFont, Config.GuildMemberListView.ListViewItem.TextFontSize, Config.GuildMemberListView.ListViewItem.TextInherit)
        row.Character = nil     
        row.Texture = row:CreateTexture(nil)
        row.Texture:SetPoint('TOPLEFT', 0, -2)
        row.Texture:SetPoint('BOTTOMRIGHT', 0, 2) 
        row:SetScript('OnShow', function(self)
            if self.Character then
                if self.Character.Class and self.Character.Name then
                    if self.Character.Class then
                        self.ClassIcon.Texture:SetTexture(Aleri.DB.Classes[self.Character.Class].Icon)
                    end
                    if self.Character.Name then
                        if self.Character.Online == true or self.Character.Online == 1 then
                            self.Name:SetText(tostring(Aleri.DB.OnlineStatusFontStringIcon.Online..' '..Aleri.DB.Classes[self.Character.Class].FontColour..self.Character.Name))
                        elseif self.Character.Online == false or self.Character.Online == nil then
                            self.Name:SetText(tostring(Aleri.DB.OnlineStatusFontStringIcon.Offline..' '..Aleri.DB.Classes[self.Character.Class].FontColour..self.Character.Name))
                        end
                    end
                    if self.Character.Level then
                        self.Level:SetText(tostring(Aleri.DB.Classes[self.Character.Class].FontColour..self.Character.Level))
                    end
                    local roleString = ''
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name] then                    
                        if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec ~= '-' then
                            roleString = tostring(roleString..Aleri.DB.RoleIcons[Aleri.DB.SpecToRole[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec]].FontStringIcon)
                        end
                        if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec ~= '-' then
                            roleString = tostring(roleString..' '..Aleri.DB.RoleIcons[Aleri.DB.SpecToRole[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec]].FontStringIcon)
                        end
                        self.Role:SetText(roleString)
                    end
                    if self.Character.Info then
                        self.Info:SetText(tostring(self.Character.Info)) -- TODO: reconfigure the listview to show note/info better
                    end
                end
            end
        end)
        row:SetScript('OnHide', function(self)
            if self.Character then
                self.ClassIcon.Texture:SetTexture(nil)
                self.Name:SetText('')
                self.Level:SetText('')
                self.Role:SetText('')
                self.Info:SetText('')
            end
        end)
        row:SetScript('OnEnter', function(self)
            if self.Character and ALERI_LOADED() then
                --self.Texture:SetTexture(1,1,1,0.1)
                GameTooltip:SetOwner(self, "ANCHOR_NONE")
                GameTooltip:SetPoint("RIGHT", self, 'LEFT')
                if self.Character.Name and self.Character.Class and self.Character.Level then
                    GameTooltip:AddDoubleLine(tostring(Aleri.DB.Classes[self.Character.Class].FontColour..self.Character.Name), tostring(Aleri.DB.Classes[self.Character.Class].FontColour..self.Character.Level..'  '..Aleri.DB.Classes[self.Character.Class].FontStringIcon))
                end
                if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name] then
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainCharacter ~= '-' then
                        GameTooltip:AddLine(' ')
                        GameTooltip:AddDoubleLine(L['MainCharacter'], AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainCharacter, 1, 1, 1, 1, 1, 1)
                    end
                    GameTooltip:AddLine(' ')
                    GameTooltip:AddDoubleLine(L['ItemLevel'], AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].ItemLevel, 1, 1, 1, 1, 1, 1)
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].GearScore then
                        GameTooltip:AddDoubleLine(L['GearScore'], AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].GearScore, 1, 1, 1, 1, 1, 1)
                    else
                        GameTooltip:AddDoubleLine(L['GearScore'], '-', 1, 1, 1, 1, 1, 1)
                    end
                    GameTooltip:AddLine(' ')
                    GameTooltip:AddLine(tostring(Aleri.DB.Classes[self.Character.Class].FontColour..L['Specializations']))
                    if  AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec ~= '-' then
                        GameTooltip:AddDoubleLine(tostring(Aleri.DB.RoleIcons[Aleri.DB.SpecToRole[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec]].FontStringIcon..'|cffffffff'..AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec..'|r'), Aleri.DB.SpecFontStringIcons[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].MainSpec])
                    else
                        GameTooltip:AddLine(L['CharacterDataMissing'], 1, 1, 1)
                    end
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec ~= '-' then
                        GameTooltip:AddDoubleLine(tostring(Aleri.DB.RoleIcons[Aleri.DB.SpecToRole[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec]].FontStringIcon..'|cffffffff'..AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec..'|r '), Aleri.DB.SpecFontStringIcons[self.Character.Class][AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].OffSpec])
                    end
                    GameTooltip:AddLine(' ')
                    GameTooltip:AddLine(tostring(Aleri.DB.Classes[self.Character.Class].FontColour..L['Professions']))
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession1 ~= '-' then
                        GameTooltip:AddDoubleLine(AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession1, AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession1Level, 1, 1, 1, 1, 1, 1)
                        GameTooltip:AddTexture(Aleri.DB.Professions[AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession1].Icon, {width = 32, height = 32})
                    end
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession2 ~= '-' then
                        GameTooltip:AddDoubleLine(AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession2, AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession2Level, 1, 1, 1, 1, 1, 1)
                        GameTooltip:AddTexture(Aleri.DB.Professions[AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Profession2].Icon, {width = 32, height = 32})
                    end                    
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Cooking ~= '-' then
                        GameTooltip:AddDoubleLine(L['Cooking'], AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Cooking, 1, 1, 1, 1, 1, 1)
                        GameTooltip:AddTexture(Aleri.DB.Professions.Cooking.Icon, {width = 32, height = 32})
                    end
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].FirstAid ~= '-' then
                        GameTooltip:AddDoubleLine(L['FirstAid'], AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].FirstAid, 1, 1, 1, 1, 1, 1)
                        GameTooltip:AddTexture(Aleri.DB.Professions.FirstAid.Icon, {width = 32, height = 32})
                    end
                    if AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Fishing ~= '-' then
                        GameTooltip:AddDoubleLine(L['Fishing'],AleriGlobalSettings[GUILD_NAME].Members[self.Character.Name].Fishing, 1, 1, 1, 1, 1, 1)
                        GameTooltip:AddTexture(Aleri.DB.Professions.Fishing.Icon, {width = 32, height = 32})
                    end
                else
                    GameTooltip:AddLine(' ')
                    GameTooltip:AddLine(L['CharacterDataMissing'], 1, 1, 1)
                end
                GameTooltip:Show()
            end
        end)
        row:SetScript('OnLeave', function(self) 
            self.Texture:SetTexture(nil)
            GameTooltip_SetDefaultAnchor(GameTooltip, UIParent) 
        end)
        View.RosterFrameGuildMembersListView[i] = row
    end
end

function AleriInterfaceRosterFrame_OnShow(self)
    AleriInterface_Title:SetText(tostring(L['Title']..' - '..L['Roster']))
    AleriInterfaceRosterFrame_Header:SetText(L['RosterHeader'])
    Model:ScanGuildRoster()
    Model:SortGuildMembers('Name') -- default option?
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    AleriInterfaceRosterFrameScrollBar:SetValue(1)
    OPENED_TAB = 2
end

function AleriInterfaceRosterFrameSortClass_OnClick(self)
    --Model:ScanGuildRoster()
    Model:SortGuildMembers('Class')
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    AleriInterfaceRosterFrameScrollBar:SetValue(1) 
end

function AleriInterfaceRosterFrameSortClass_OnEnter(self)
    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOM', self, 'TOP')
    GameTooltip:AddLine(L['SortClass'])
    GameTooltip:Show()
end

function AleriInterfaceRosterFrameSortHeader_OnLeave(self)
    GameTooltip_SetDefaultAnchor(GameTooltip, UIParent)
end

function AleriInterfaceRosterFrameSortName_OnEnter(self)
    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOM', self, 'TOP')
    GameTooltip:AddLine(L['SortName'])
    GameTooltip:Show()
end

function AleriInterfaceRosterFrameSortLevel_OnEnter(self)
    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOM', self, 'TOP')
    GameTooltip:AddLine(L['SortLevel'])
    GameTooltip:Show()
end

function AleriInterfaceRosterFrameSortRole_OnEnter(self)
    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOM', self, 'TOP')
    GameTooltip:AddLine(L['SortRole'])
    GameTooltip:Show()
end

function AleriInterfaceRosterFrameSortInfo_OnEnter(self)
    GameTooltip:SetOwner(self, 'ANCHOR_NONE')
    GameTooltip:SetPoint('BOTTOM', self, 'TOP')
    GameTooltip:AddLine(L['PlayerNotesInfo'])
    GameTooltip:Show()
end

function AleriInterfaceRosterFrameSortRole_OnClick(self)
    --Model:ScanGuildRoster()
    Model:SortGuildMembers('Role')
    AleriInterfaceRosterFrameScrollBar:SetValue(1)
end

function AleriInterfaceRosterFrameSortLevel_OnClick(self)
    --Model:ScanGuildRoster()
    Model:SortGuildMembers('Level')
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    AleriInterfaceRosterFrameScrollBar:SetValue(1) 
end

function AleriInterfaceRosterFrameSortName_OnClick(self)
    --Model:ScanGuildRoster()
    Model:SortGuildMembers('Name')
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    AleriInterfaceRosterFrameScrollBar:SetValue(1) 
end

function AleriInterfaceRosterFrameSortInfo_OnClick(self)
    --Model:ScanGuildRoster()
    Model:SortGuildMembers('Info')
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    AleriInterfaceRosterFrameScrollBar:SetValue(1) 
end

function AleriInterfaceRosterFrameSortInfo_OnClick(self)

end

function AleriInterfaceRosterFrameShowOfflineCB_OnClick(self)
    --Model:ScanGuildRoster()
    Model:FilterGuildMembersByOnline(self:GetChecked())
    Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
end

function AleriInterfaceRosterFrameSearchRosterInputBox_OnTextChanged(self)
    if self:GetText():len() > 0 then
        --Model:ScanGuildRoster()
        Model:FilterGuildMembersByName(self:GetText())
        Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
    else
        Model:ScanGuildRoster()
        Model:SortGuildMembers('Name')
        Model:UpdateRosterFrameGuildMemberListView(1, Model.GuildMembers)
        AleriInterfaceRosterFrameScrollBar:SetValue(1) 
    end
end

function AleriInterfaceRosterFrameRescanRosterButton_OnClick(self)
    Model:ScanGuildRoster()
    Model:SortGuildMembers('Name')
end

function AleriInterfaceRosterFrameScrollBar_OnValueChanged(self)
    if ALERI_LOADED() == true then
        Model:UpdateRosterFrameGuildMemberListView(self:GetValue(), Model.GuildMembers)
    end
end

--no need to update the view here as this invokes the ScrollBar_OnValueChanged function which will handle updating view
function AleriInterfaceRosterFrame_OnMouseWheel(self, delta)
    if ALERI_LOADED() == true then
        AleriInterfaceRosterFrameScrollBar:SetValue(AleriInterfaceRosterFrameScrollBar:GetValue() - delta) 
    end
end










---------------------------------------------------------------------------------------------------------------------------------------------------------------
--PROFILE FRAME
function AleriInterfaceProfileFrame_OnLoad(self)
    AleriInterface_Title:SetText(tostring(L['Title']..' - '..L['Profile']))
    AleriInterfaceProfileFrameHeader:SetText(L['EditCharacterInfo'])
    AleriInterfaceProfileFrameCharacterName:SetText(L['Name'])
    AleriInterfaceProfileFrameCharacterLevel:SetText(L['Level'])
    AleriInterfaceProfileFrameCharacterClass:SetText(L['Class'])
    AleriInterfaceProfileFrameCharacterMainSpec:SetText(L['MainSpec'])
    AleriInterfaceProfileFrameCharacterOffSpec:SetText(L['OffSpec'])
    AleriInterfaceProfileFrameCharacterProfession1:SetText(L['Profession1'])
    
    AleriInterfaceProfileFrameCharacterProfession2:SetText(L['Profession2'])
    AleriInterfaceProfileFrameCharacterFirstAid:SetText(L['FirstAid'])
    AleriInterfaceProfileFrameCharacterCooking:SetText(L['Cooking'])
    AleriInterfaceProfileFrameCharacterFishing:SetText(L['Fishing'])
    AleriInterfaceProfileFrameConfirmCharacterData:SetText(L['SaveCharacterData'])
    AleriInterfaceProfileFrameMainCharacterNameInputDesc:SetText(L['MainCharacterNameInputDesc'])

    AleriInterfaceProfileFrameSendProf1:SetScript('OnEnter', function(self)
        GameTooltip:SetOwner(self, "ANCHOR_NONE")
        GameTooltip:SetPoint("LEFT", self, 'RIGHT')
        GameTooltip:AddLine(L['ShareProfTooltip'])
        GameTooltip:Show()
    end)
    AleriInterfaceProfileFrameSendProf1:SetScript('OnLeave', function(self)
        GameTooltip:Hide()
    end)
    AleriInterfaceProfileFrameSendProf2:SetScript('OnEnter', function(self)
        GameTooltip:SetOwner(self, "ANCHOR_NONE")
        GameTooltip:SetPoint("LEFT", self, 'RIGHT')
        GameTooltip:AddLine(L['ShareProfTooltip'])
        GameTooltip:Show()
    end)
    AleriInterfaceProfileFrameSendProf1:SetScript('OnLeave', function(self)
        GameTooltip:Hide()
    end)
    AleriInterfaceProfileFrameSendProf2:SetScript('OnLeave', function(self)
        GameTooltip:Hide()
    end)    
end

function AleriInterfaceProfileFrame_OnShow(self)
    if ALERI_LOADED() == true then
        Player:GetCharacterData()
        Player:SaveCharacterData()

        AleriInterfaceProfileFrameMainSpecDD_Init()
        UIDropDownMenu_SetWidth(AleriInterfaceProfileFrameMainSpecDD, 115)
        UIDropDownMenu_SetText(AleriInterfaceProfileFrameMainSpecDD, L['Class'])
        if Player.Character.MainSpec then
            UIDropDownMenu_SetText(AleriInterfaceProfileFrameMainSpecDD, Player.Character.MainSpec)
        end
        AleriInterfaceProfileFrameOffSpecDD_Init()
        UIDropDownMenu_SetWidth(AleriInterfaceProfileFrameOffSpecDD, 115)
        UIDropDownMenu_SetText(AleriInterfaceProfileFrameOffSpecDD, L['Class'])
        if Player.Character.OffSpec then
            UIDropDownMenu_SetText(AleriInterfaceProfileFrameOffSpecDD, Player.Character.OffSpec)
        end

        AleriInterfaceProfileFrameCharacterNameData:SetText(Player.Character.Name)
        AleriInterfaceProfileFrameCharacterLevelData:SetText(Player.Character.Level)
        AleriInterfaceProfileFrameCharacterClassData:SetText(tostring(Player.Character.Class..' '..Aleri.DB.Classes[Player.Character.Class].FontStringIcon))
        AleriInterfaceProfileFrameCharacterProfession1Data:SetText(tostring(Player.Character.Profession1..' - '..Player.Character.Profession1Level))
        AleriInterfaceProfileFrameCharacterProfession2Data:SetText(tostring(Player.Character.Profession2..' - '..Player.Character.Profession2Level))
        AleriInterfaceProfileFrameCharacterFirstAidData:SetText(Player.Character.FirstAid)
        AleriInterfaceProfileFrameCharacterCookingData:SetText(Player.Character.Cooking)
        AleriInterfaceProfileFrameCharacterFishingData:SetText(Player.Character.Fishing)
        AleriInterfaceProfileFrameMainCharacterNameInputBox:SetText(Player.Character.MainCharacter)
    end

    OPENED_TAB = 4
end

function AleriInterfaceProfileFrameConfirmCharacterData_OnClick(self)
    Player:SaveCharacterData()
    if Player.Character.MainCharacter ~= nil then
        SendAddonMessage('aleri-charUKV', tostring('MainCharacter'..':'..Player.Character.MainCharacter), 'GUILD')
    else
        DEBUG('unable to update main character')
    end
    --SendAddonMessage('aleri-charData', Player:GenerateCharacterDataString(), 'GUILD')
end

function AleriInterfaceProfileFrameMainCharacterNameInputBox_OnTextChanged(self)
    if string.len(self:GetText()) > 0 then
        Player.Character.MainCharacter = tostring(self:GetText())
    else
        Player.Character.MainCharacter = '-'
    end
end

function AleriInterfaceProfileFrameSendProf1_OnShow(self)
    if ALERI_LOADED() then
        if Player.Character.Profession1 and Aleri.DB.ProfessionsThatShare[Player.Character.Profession1] then
            self:HookScript("OnClick", function(self)
                CastSpellByName(Player.Character.Profession1)
            end)
            self:SetText(L['ShareProfession'])
            self:Show();
        else
            self:Hide()
        end
    end
end

function AleriInterfaceProfileFrameSendProf1_OnClick(self)
    PROFESSION_SHARING = Aleri.DB.Professions[Player.Character.Profession1].ID
end

function AleriInterfaceProfileFrameSendProf2_OnShow(self)
    if ALERI_LOADED() then
        if Player.Character.Profession2 and Aleri.DB.ProfessionsThatShare[Player.Character.Profession2] then
            self:HookScript("OnClick", function(self)
                CastSpellByName(Player.Character.Profession2)
            end)
            self:SetText(L['ShareProfession'])
            self:Show();
        else
            self:Hide()
        end
    end
end

function AleriInterfaceProfileFrameSendProf2_OnClick(self)
    PROFESSION_SHARING = Aleri.DB.Professions[Player.Character.Profession2].ID
end










---------------------------------------------------------------------------------------------------------------------------------------------------------------
--PROFESSIONS FRAME
function AleriInterfaceProfessionFrame_OnLoad(self)
    for i = 1, 10 do
        local row = CreateFrame("FRAME", tostring('AleriInterfaceProfessionFrameMembersListView'..i), AleriInterfaceProfessionFrameMembersListView)
        row:EnableMouse(true)
        row:SetSize(Config.ProfessionsMemberListView.ListViewItem.Width, Config.ProfessionsMemberListView.ListViewItem.Height)
        row:SetPoint('TOPLEFT', 20, (((i-1)*-1)*Config.ProfessionsMemberListView.ListViewItem.Height) - 10)
        row.Name = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceProfessionFrameMembersListView'..i..'_Text'), 'LEFT', Config.ProfessionsMemberListView.ListViewItem.NamePosX, 2, '', nil, Config.ProfessionsMemberListView.ListViewItem.TextFont, Config.ProfessionsMemberListView.ListViewItem.TextFontSize, Config.ProfessionsMemberListView.ListViewItem.TextInherit)
        row.Character = nil
        row.Texture = row:CreateTexture(nil)
        row.Texture:SetPoint('TOPLEFT', 0, -2)
        row.Texture:SetPoint('BOTTOMRIGHT', 0, 2)
        row:SetScript('OnMouseDown', function(self)
            if self.Character then
                Model:ClearProfessionFrameRecipeListView()
                Model:SetProfessionRecipes(self.Character.Name)
            end
        end)
        row:SetScript('OnShow', function(self)
            if self.Character then
                self.Name:SetText(self.Character.Name)
            end
        end)   
        row:SetScript('OnEnter', function(self)
            if self.Character then
                self.Texture:SetTexture(1,1,1,0.05)
            end
        end)      
        row:SetScript('OnLeave', function(self)
            if self.Character then
                self.Texture:SetTexture(nil)
            end
        end)                 
        View.ProfessionFrameMemberListView[i] = row
    end


    for i = 1, 10 do
        local row = CreateFrame("FRAME", tostring('AleriInterfaceProfessionFrameMemberRecipeListViewItem'..i), AleriInterfaceProfessionFrameRecipesListView)
        row:EnableMouse(true)
        row:SetSize(Config.ProfessionsRecipeListView.ListViewItem.Width, Config.ProfessionsRecipeListView.ListViewItem.Height)
        row:SetPoint('TOPLEFT', 10, (((i-1)*-1)*Config.ProfessionsRecipeListView.ListViewItem.Height))
        row.RecipeText = Aleri.SDK.UI.NewFontString(row, tostring('AleriInterfaceProfessionFrameMemberRecipeListViewItem'..i..'_Text'), 'LEFT', Config.ProfessionsRecipeListView.ListViewItem.NamePosX, 0, 'Name', nil, Config.ProfessionsRecipeListView.ListViewItem.TextFont, Config.ProfessionsRecipeListView.ListViewItem.TextFontSize, Config.ProfessionsRecipeListView.ListViewItem.TextInherit)
        row.Recipe = nil
        row.Texture = row:CreateTexture(nil)
        row.Texture:SetPoint('TOPLEFT', 0, -2)
        row.Texture:SetPoint('BOTTOMRIGHT', 0, 2)        
        row:SetScript('OnEnter', function(self) 
            if self.Recipe then
                GameTooltip:SetOwner(self, "ANCHOR_NONE")
                GameTooltip:SetPoint("LEFT", self, 'RIGHT')
                GameTooltip:SetHyperlink("item:"..self.Recipe..":0:0:0:0:0:0:0")
                GameTooltip:Show()
                local iName, iLink, iRarity, iLevel = GetItemInfo(self.Recipe)
                local r, g, b, hex = GetItemQualityColor(iRarity)
                self.Texture:SetTexture(r, g, b, 0.05)                
            end
        end)
        row:SetScript('OnLeave', function(self)
            GameTooltip:ClearLines()
            GameTooltip:Hide()
            if self.Recipe then
                self.Texture:SetTexture(nil)
            end             
        end)       
        row:SetScript('OnHide', function(self)
            self.Recipe = nil
        end)     
        row:Hide()
        View.ProfessionFrameMemberRecipeListView[i] = row
    end
    UIDropDownMenu_SetWidth(AleriInterfaceProfessionFrameSelectProfessionDD, 131)
    UIDropDownMenu_SetText(AleriInterfaceProfessionFrameSelectProfessionDD, L['Professions'])

    AleriInterfaceProfessionFrameRecipesListViewHeader:SetText(L['ProfessionRecipes'])
end


function AleriInterfaceProfessionFrame_OnShow(self)
    AleriInterfaceProfessionFrameSelectProfessionDD_Init() --data required for drop down not available during OnLoad
    Model:ClearProfessionFrameMemberListView()
    Model:ClearProfessionFrameRecipeListView()
    AleriInterfaceProfessionFrameHeader:SetText(L['GuildTradeDesc'])
    AleriInterfaceProfessionFrameProfessionIconTexture:SetTexture('Interface\\Icons\\Inv_misc_book_09')
    UIDropDownMenu_SetText(AleriInterfaceProfessionFrameSelectProfessionDD, L['Professions'])
    OPENED_TAB = 3
end

function AleriInterfaceProfessionFrameMemberListViewScrollBar_OnValueChanged(self)
    Model:UpdateProfessionFrameMemberListView(self:GetValue())
end

function AleriInterfaceProfessionFrameRecipesListViewScrollBar_OnValueChanged(self)
    Model:UpdateProfessionFrameRecipeListView(self:GetValue())
end

function AleriInterfaceProfessionFrameRecipesListView_OnMouseWheel(self, delta)
    if ALERI_LOADED() == true then
        AleriInterfaceProfessionFrameRecipesListViewScrollBar:SetValue(AleriInterfaceProfessionFrameRecipesListViewScrollBar:GetValue() - delta) 
    end
end










---------------------------------------------------------------------------------------------------------------------------------------------------------------
--RAID INFO FRAME
function AleriInterfaceRaidInfoFrame_OnLoad(self)
    AleriInterfaceRaidInfoFrameSelectRaidInstanceDesc:SetText(L['RaidSelectDesc'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceGearScore:SetText(Aleri.DB.Professions.Blacksmithing.FontStringIcon..'  '..L['GearScore'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceGems:SetText(Aleri.DB.Professions.Jewelcrafting.FontStringIcon..'  '..L['Gems'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceEnchants:SetText(Aleri.DB.Professions.Enchanting.FontStringIcon..'  '..L['Enchants'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceTanks:SetText(Aleri.DB.RoleIcons.Tank.FontStringIconLARGE..' '..L['Tanks'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceMelee:SetText(Aleri.DB.RoleIcons.Melee.FontStringIconLARGE..' '..L['Melee'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceRanged:SetText(Aleri.DB.RoleIcons.Ranged.FontStringIconLARGE..' '..L['Ranged'])
    AleriInterfaceRaidInfoFrameSelectRaidInstanceHealers:SetText(Aleri.DB.RoleIcons.Healer.FontStringIconLARGE..' '..L['Healers'])
    AleriInterfaceRaidInfoFrameSelectedRaidInstanceHeader:SetText(L['-'])
end

function AleriInterfaceRaidInfoFrame_OnShow(self)
    if ALERI_LOADED() == true then
        AleriInterfaceRaidInfoFrameSelectRaidInstanceDD_Init()
        UIDropDownMenu_SetWidth(AleriInterfaceRaidInfoFrameSelectRaidInstanceDD, 100)
        UIDropDownMenu_SetText(AleriInterfaceRaidInfoFrameSelectRaidInstanceDD, L['Raids'])
    end
    OPENED_TAB = 5
end










--=============================================================================================================================================================
--DROPDOWN INIT FUNCTIONS
--=============================================================================================================================================================
function AleriInterfaceProfileFrameMainSpecDD_Init()
    if ALERI_LOADED() == true then
        UIDropDownMenu_Initialize(AleriInterfaceProfileFrameMainSpecDD, function(self, level, menuList)
            local info = UIDropDownMenu_CreateInfo()
            for i, spec in pairs(Aleri.DB.Classes[Player.Character.Class].Specializations) do
                info.text = tostring(Aleri.DB.SpecFontStringIcons[Player.Character.Class][spec]..'  '..L[spec])
                info.hasArrow = false
                info.keepShownOnClick = false
                info.func = function() 
                    UIDropDownMenu_SetText(AleriInterfaceProfileFrameMainSpecDD, L[spec]) 
                    Player.Character.MainSpec = tostring(spec)
                    Player:SaveCharacterData()
                    SendAddonMessage('aleri-charUKV', tostring('MainSpec'..':'..spec), 'GUILD')
                end
                UIDropDownMenu_AddButton(info)
            end
        end)
    end
end
function AleriInterfaceProfileFrameOffSpecDD_Init()
    if ALERI_LOADED() == true then
        UIDropDownMenu_Initialize(AleriInterfaceProfileFrameOffSpecDD, function(self, level, menuList)
            local info = UIDropDownMenu_CreateInfo()
            for i, spec in pairs(Aleri.DB.Classes[Player.Character.Class].Specializations) do
                info.text = tostring(Aleri.DB.SpecFontStringIcons[Player.Character.Class][spec]..'  '..L[spec])
                info.hasArrow = false
                info.keepShownOnClick = false
                info.func = function()
                    UIDropDownMenu_SetText(AleriInterfaceProfileFrameOffSpecDD, L[spec]) 
                    Player.Character.OffSpec = tostring(spec)
                    Player:SaveCharacterData()
                    SendAddonMessage('aleri-charUKV', tostring('OffSpec'..':'..spec), 'GUILD')
                end
                UIDropDownMenu_AddButton(info)
            end
        end)
    end
end
function AleriInterfaceRosterFrameSearchMemberBySpecDD_Init()
    UIDropDownMenu_Initialize(AleriInterfaceRosterFrameSearchMemberBySpecDD, function(self, level, menuList)
        local info = UIDropDownMenu_CreateInfo()
        if (level or 1) == 1 then
            for class, d in pairs(Aleri.DB.Classes) do
                --info.text = tostring(d.FontStringIcon..' '..d.FontColour..L[class]..'|r') --looks too small ?
                info.text = tostring(d.FontColour..L[class]..'|r')
                --info.icon = d.Icon --overlaps the arrow
                info.hasArrow = true
                info.keepShownOnClick = false
                info.menuList = d.Specializations
                UIDropDownMenu_AddButton(info)
            end
        else
            for k, spec in pairs(menuList) do
                info.text = tostring(L[spec])
                info.isNotRadio = true
                info.keepShownOnClick = false
                info.func = function() UIDropDownMenu_SetText(AleriInterfaceRosterFrameSearchMemberBySpecDD, spec) end
                UIDropDownMenu_AddButton(info, level)
            end
		end
	end)
end
function AleriInterfaceProfessionFrameSelectProfessionDD_Init()
    if ALERI_LOADED() == true then
        local profOrdered = {}
        for k, v in pairs(Aleri.DB.Professions) do
            if v.ID < 9 then
                tinsert(profOrdered, v.Name)
            end
        end
        tsort(profOrdered)
        UIDropDownMenu_Initialize(AleriInterfaceProfessionFrameSelectProfessionDD, function(self, level, menuList)
            local info = UIDropDownMenu_CreateInfo()
            for i, prof in ipairs(profOrdered) do
                info.text = tostring(Aleri.DB.Professions[prof].Name)
                info.icon = Aleri.DB.Professions[prof].Icon
                info.hasArrow = false
                info.keepShownOnClick = false
                info.func = function() 
                    Model.ProfessionSelected = tostring(Aleri.DB.Professions[prof].Name)
                    UIDropDownMenu_SetText(AleriInterfaceProfessionFrameSelectProfessionDD, Aleri.DB.Professions[prof].Name) 
                    AleriInterfaceProfessionFrameProfessionIconTexture:SetTexture(Aleri.DB.Professions[prof].Icon)
                    Model:ClearProfessionFrameRecipeListView()
                    Model:GetMembersByProfession(tostring(Aleri.DB.Professions[prof].Name))
                    AleriInterfaceProfessionFrameHeader:SetText(Aleri.DB.ProfessionDescriptions[prof])
                end
                UIDropDownMenu_AddButton(info)
            end
        end)
    end
end
function AleriInterfaceRaidInfoFrameSelectRaidInstanceDD_Init()
    if ALERI_LOADED() then
        local raids = {}
        for raid, data in pairs(Aleri.Guides.WOTLK) do
            tinsert(raids, raid)
        end
        tsort(raids)
        UIDropDownMenu_Initialize(AleriInterfaceRaidInfoFrameSelectRaidInstanceDD, function(self, level, menuList)
            local info = UIDropDownMenu_CreateInfo()
            if (level or 1) == 1 then
                for k, raid in pairs(raids) do
                    info.text = raid
                    info.hasArrow = true
                    info.keepShownOnClick = false
                    --info.menuList = Aleri.Guides.WOTLK[raid]
                    info.menuList = { Raid = raid, Difficulties = Aleri.Guides.WOTLK[raid] }
                    UIDropDownMenu_AddButton(info)
                end
            else
                local diffs = {}
                --for k, v in pairs(menuList) do
                for k, v in pairs(menuList.Difficulties) do
                    tinsert(diffs, k)
                end
                tsort(diffs)
                for k, diff in pairs(diffs) do
                    info.text = diff
                    info.isNotRadio = true
                    info.keepShownOnClick = false
                    info.func = function(raid) 
                        --UIDropDownMenu_SetText(AleriInterfaceRaidInfoFrameSelectRaidInstanceDD, tostring(raid.Raid..' '..k))
                        Model:UpdateRaidInfoFrame(menuList.Raid, diff, menuList.Difficulties[diff])
                    end
                    UIDropDownMenu_AddButton(info, level)
                end
            end
        end)
    end
end









--=============================================================================================================================================================
--ADDON INIT AND UPDATE
--=============================================================================================================================================================
function ALERI_INIT()
    if AleriGlobalSettings == nil then
        GUILD_NAME, _, _ = GetGuildInfo('player')
        AleriGlobalSettings = {
            [GUILD_NAME] = {
                Members = {},
            },
        }
        DEBUG('created global table')
    end
    
    if AleriCharacterSettings == nil then
        local _, class, _, race, gender, name, realm = GetPlayerInfoByGUID(UnitGUID('player'))
        if class == 'DEATH KNIGHT' then class = 'DEATHKNIGHT' end
        if gender == 3 then
            gender = 'FEMALE'
        else
            gender = 'MALE'
        end
        AleriCharacterSettings = {
            ['MinimapButton'] = {}
        }
        AleriCharacterSettings['Name'], PLAYER_NAME = name, name
        AleriCharacterSettings['Race'], PLAYER_RACE = race:upper(), race:upper()
        AleriCharacterSettings['Class'], PLAYER_CLASS = class:upper(), class:upper()
        AleriCharacterSettings['Gender'], PLAYER_GENDER = gender:upper(), gender:upper()
        AleriCharacterSettings['GuildName'] = GUILD_NAME
        DB_LOADED = true
        DEBUG('created character table')
    else
        if AleriCharacterSettings['Name'] ~= nil then
            PLAYER_NAME = AleriCharacterSettings['Name']
            PLAYER_RACE = AleriCharacterSettings['Race']
            PLAYER_CLASS = AleriCharacterSettings['Class']
            GUILD_NAME = AleriCharacterSettings['GuildName']
            DB_LOADED = true
            DEBUG('player data exists '..PLAYER_NAME)
        end
    end

    Player:GetCharacterData()
    SendAddonMessage('aleri-charData', Player:GenerateCharacterDataString(), 'GUILD')
    Aleri.CreateMinimapIcon() 
end

function Aleri.UpdateFix()
    if ALERI_LOADED() then
        INFO_PRINT('running update fix', Config.PrintFontColour)
        if AleriGlobalSettings['UpdateFix'] == nil or AleriGlobalSettings['UpdateFix'] == 0.0 then
            for k, member in pairs(AleriGlobalSettings[GUILD_NAME].Members) do
                if member.ProfessionRecipes ~= nil then
                    INFO_PRINT('found old profession recipe database, deleting table for, '..member.Name..' please ask this character to re-share their professions')
                    member.ProfessionRecipes = nil
                    member.ProfessionRecipes = {}
                end
            end
            AleriGlobalSettings['UpdateFix'] = 1.0
        end
        INFO_PRINT('update complete, thanks for using Aleri.', Config.PrintFontColour)
    end
end

function Aleri.GetInitDelay()
    return tonumber(INIT_DELAY_SECONDS)
end


--=============================================================================================================================================================
--DIALOGS
--=============================================================================================================================================================
StaticPopupDialogs["Aleri_Reset"] = {
    text = "This will completely wipe all data for Aleri and then run the initial setup again",
    button1 = "Wipe Database",
    button2 = 'Cancel',
    OnAccept = function()
        Player.Character = nil
        Model.GuildMembers = nil
        AleriCharacterSettings = nil
        AleriGlobalSettings = nil
        ALERI_INIT()
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,
}
StaticPopupDialogs["Aleri_Welcome"] = {
    text = 'Please wait while the recombobulator warms up, then you can load Aleri :)',
    button1 = "Ok",
    --button2 = 'Oops',
    StartDelay = Aleri.GetInitDelay,
    DelayText = 'ready in %d',
    OnShow = function(self)
        self.timeLeft = Aleri.GetInitDelay()
    end,
    OnAccept = function()
        ALERI_INIT()
        DoEmote('VOICEMACRO_5_Ta_0_FEMALE')
    end,
    OnCancel = function()
        DB_LOADED = false
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = false,
    preferredIndex = 3,
    showAlert = 1,
}










--=============================================================================================================================================================
--EVENTS
--=============================================================================================================================================================
function AleriInterface_OnEvent(self, event, ...)
    if event == "ADDON_LOADED" and tostring(select(1, ...)):lower() == "aleri" then
        Aleri.SDK.Util.MakeFrameMove(AleriInterface)
        Aleri.LoadTime = GetTime()
        --ALERI_INIT()
        StaticPopup_Show('Aleri_Welcome')

    elseif event == 'PLAYER_GUILD_UPDATE' then
        --OnShow events call a guild roster scan, is this event needed to be tracked ???
    elseif event == 'GUILD_ROSTER_UPDATE' then 
        --OnShow events call a guild roster scan, is this event needed to be tracked ???

    elseif event == 'ADDON_LOADED' and tostring(select(1, ...)):lower() == 'gearscorelite' then
        GEARSCORE_LOADED = true
        INFO_PRINT(L['GearScoreDetected'], Config.PrintFontColour)
       
    elseif event == 'CHAT_MSG_ADDON' then
        local prefix = select(1, ...)
        local msg = select(2, ...)
        local sender = select(4, ...)       
        if string.find(sender, '-') then
            local data_sent = {}
            for d in string.gmatch(sender, '[^-]+') do
                data_sent[i] = d
                i = i + 1
            end
            sender = data_sent[1]
        end
        if prefix == 'aleri-charData' then --character dataString
            DEBUG('recieved a character data string from '..sender..' '..msg)
            Model:ParseCharacterData(msg)
        end
        if prefix == 'aleri-charUKV' then --char update key value
            local i = 1
            local data_sent = {}
            for d in string.gmatch(msg, '[^:]+') do
                data_sent[i] = d
                i = i + 1
            end
            Model:UpdateCharacterByKey(sender, data_sent[1], data_sent[2], type(data_sent[2]))
            DEBUG('character update from '..sender..' - '..data_sent[1]..': '..data_sent[2]..', dataType='..type(data_sent[2]))
        end
        if prefix == 'Aleri-charRID' then --char recipe ids
            Model:ParseRecipeIDs(msg, sender)
        end

    --this should keep all online players updated whenever a player levels up a profession etc
    elseif event == 'CHAT_MSG_SKILL' then
        if ALERI_LOADED() then
            Player:GetCharacterData()
            Player:SaveCharacterData()
            SendAddonMessage('aleri-charData', Player:GenerateCharacterDataString(), 'GUILD')
        end

    elseif event == 'TRADE_SKILL_SHOW' then
        if PROFESSION_SHARING ~= nil then
            local ShareProfessionRecipeIDs = {}
            for i=1,GetNumTradeSkills() do
            local name, type, _, _, _, _ = GetTradeSkillInfo(i)
            local skillName, header, isExpanded, skillRank, numTempPoints, skillModifier, skillMaxRank, isAbandonable, stepCost, rankCost, minLevel, skillCostType, skillDescription = GetSkillLineInfo(i)
                if (name and type ~= "header") then
                    local n, l = GetItemInfo(GetTradeSkillItemLink(i))
                    if l then
                        local _, _, Color, Ltype, Id, Enchant, Gem1, Gem2, Gem3, Gem4, Suffix, Unique, LinkLvl, reforging, Name = string.find(l, "|?c?f?f?(%x*)|?H?([^:]*):?(%d+):?(%d*):?(%d*):?(%d*):?(%d*):?(%d*):?(%-?%d*):?(%-?%d*):?(%d*):?(%d*)|?h?%[?([^%[%]]*)%]?|?h?|?r?")
                        tinsert(ShareProfessionRecipeIDs, Id)
                    end
                end
            end
            if ShareProfessionRecipeIDs ~= nil and #ShareProfessionRecipeIDs > 0 then
                local recipeCount = #ShareProfessionRecipeIDs
                local msgCount = math.ceil(recipeCount / 40)
                local lastMsgEnd = math.floor(recipeCount / 40) * 40 --??? 
                INFO_PRINT(tostring(recipeCount..' recipes scanned', Config.PrintFontColour))
                local t = {}
                for i = 1, (msgCount - 1) do            
                    t[1] = PROFESSION_SHARING
                    for j = (i*40)-39, (i*40) do
                        tinsert(t, ShareProfessionRecipeIDs[j])
                    end
                    SendAddonMessage('Aleri-charRID', table.concat(t, ':'), 'GUILD')
                    t = {}
                end
                t = {}
                t[1] = PROFESSION_SHARING
                for i = lastMsgEnd + 1, #ShareProfessionRecipeIDs do
                    table.insert(t, ShareProfessionRecipeIDs[i])
                end
                SendAddonMessage('Aleri-charRID', table.concat(t, ':'), 'GUILD')     
            end   
        end
        PROFESSION_SHARING = nil
    end
end











--=============================================================================================================================================================
--UPDATE
--=============================================================================================================================================================
function AleriInterface_OnUpdate()
    if ((Aleri.LoadTime + Aleri.InitDelay) < GetTime()) and (Aleri.InitRun == false) then
        Aleri.Init()
        AleriUpdateFrame:Hide() --should reduce update calls ???
    end

    if ALERI_LOADED() then
        if AleriGlobalSettings[GUILD_NAME].Members[PLAYER_NAME].ProfessionRecipes == nil then
            --print('prof recipess = nil')
        end
    end
end

local f = CreateFrame('FRAME', 'AleriUpdateFrame', UIParent)
f:SetSize(10,10)
f:SetPoint('CENTER')
f:Show()
--f:SetScript('OnUpdate', AleriInterface_OnUpdate)