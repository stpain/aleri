--[==[
    Aleri

    All Rights Reserved 
    
    © stpain 2020
]==]--

local AleriName, Aleri = ...

Aleri.DB = {
	CharacterSheet = { 
		Name = '-', 
		Level = 1, 
		ItemLevel = 0.0, 
		GearScore = 0.0, 
		Class = '-', 
		Race = '-', 
		Gender = '-',
		MainSpec = '-',
		OffSpec = '-',
		Fishing = 0, 
		Cooking = 0, 
		FirstAid = 0, 
		Profession1 = '-', 
		Profession1Level = 0, 
		Profession2 = '-', 
		Profession2Level = 0,
		INVSLOT_HEAD = 0,
		INVSLOT_NECK = 0,
		INVSLOT_SHOULDER = 0,
		INVSLOT_CHEST = 0,
		INVSLOT_WAIST = 0,
		INVSLOT_LEGS = 0,
		INVSLOT_FEET = 0,
		INVSLOT_WRIST = 0,
		INVSLOT_HAND = 0,
		INVSLOT_FINGER1 = 0,
		INVSLOT_FINGER2 = 0,
		INVSLOT_TRINKET1 = 0,
		INVSLOT_TRINKET2 = 0,
		INVSLOT_BACK = 0,
		INVSLOT_MAINHAND = 0,
		INVSLOT_OFFHAND = 0,
		INVSLOT_RANGED = 0,  
		Info = '-',
		MainCharacter = '-',
	},
	EquipmentSlots = { 
		{ Id = 1, Name = "Head", Pos = 0 },
		{ Id = 2, Name = "Neck", Pos = 0 }, 
		{ Id = 3, Name = "Shoulder", Pos = 0 }, 
		{ Id = 15, Name = "Back", Pos = 0 }, 
		{ Id = 5, Name = "Chest", Pos = 0 }, 
		{ Id = 9, Name = "Wrist", Pos = 0 }, 
		{ Id = 10, Name = "Hands", Pos = 1 }, 
		{ Id = 6, Name = "Waist", Pos = 1 }, 
		{ Id = 7, Name = "Legs", Pos = 1 }, 
		{ Id = 8, Name = "Feet", Pos = 1 }, 
		{ Id = 11, Name = "Ring 1", Pos = 1 }, 
		{ Id = 12, Name = "Ring 2", Pos = 1 }, 
		{ Id = 13, Name = "Trinket 1", Pos = 1 }, 
		{ Id = 14, Name = "Trinket 2", Pos = 1 }, 
		{ Id = 16, Name = "Main Hand", Pos = 0 }, 
		{ Id = 17, Name = "Off Hand", Pos = 0 },
		{ Id = 18, Name = "Ranged", Pos = 0 }
	},
	InventorySlots = {
		{ Name = 'INVSLOT_HEAD', Id = 1, Pos = 0, Display = 'Head' },
		{ Name = 'INVSLOT_NECK', Id = 2, Pos = 0, Display = 'Neck' },
		{ Name = 'INVSLOT_SHOULDER', Id = 3, Pos = 0, Display = 'Shoulder' },
		{ Name = 'INVSLOT_CHEST', Id = 5, Pos = 0, Display = 'Chest' },
		{ Name = 'INVSLOT_WAIST', Id = 6, Pos = 1, Display = 'Waist' },
		{ Name = 'INVSLOT_LEGS', Id = 7, Pos = 1, Display = 'Legs' },
		{ Name = 'INVSLOT_FEET', Id = 8, Pos = 1, Display = 'Feet' },
		{ Name = 'INVSLOT_WRIST', Id = 9, Pos = 0, Display = 'Wrist' },
		{ Name = 'INVSLOT_HAND', Id = 10, Pos = 1, Display = 'Hand' },
		{ Name = 'INVSLOT_FINGER1', Id = 11, Pos = 1, Display = 'Finger 1' },
		{ Name = 'INVSLOT_FINGER2', Id = 12, Pos = 1, Display = 'Finger 2' },
		{ Name = 'INVSLOT_TRINKET1', Id = 13, Pos = 1, Display = 'Trinket 1' },
		{ Name = 'INVSLOT_TRINKET2', Id = 14, Pos = 1, Display = 'Trinket 2' },
		{ Name = 'INVSLOT_BACK', Id = 15, Pos = 0, Display = 'Back' },
		{ Name = 'INVSLOT_MAINHAND', Id = 16, Pos = 0, Display = 'Main Hand' },
		{ Name = 'INVSLOT_OFFHAND', Id = 17, Pos = 0, Display = 'Off Hand' },
		{ Name = 'INVSLOT_RANGED', Id = 18, Pos = 0, Display = 'Range' },
	},
	Professions = {
		Alchemy = { ID = 1, Name = 'Alchemy' , Icon= 'Interface\\Icons\\Trade_Alchemy' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:3:67:3:67|t',},
		Blacksmithing = { ID = 2, Name = 'Blacksmithing' , Icon= 'Interface\\Icons\\Trade_Blacksmithing' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:77:141:3:67|t', },
		Enchanting = { ID = 3, Name = 'Enchanting' , Icon= 'Interface\\Icons\\Trade_Engraving' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:153:217:3:67|t', },
		Engineering = { ID = 4, Name = 'Engineering' , Icon= 'Interface\\Icons\\Trade_Engineering' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:227:291:3:67|t', },
		Inscription = { ID = 5, Name = 'Inscription' , Icon= 'Interface\\Icons\\INV_Inscription_Tradeskill01' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:3:67:79:143|t', },
		Jewelcrafting = { ID = 6, Name = 'Jewelcrafting' , Icon= 'Interface\\Icons\\INV_MISC_GEM_01' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:77:141:79:143|t', },
		Leatherworking = { ID = 7, Name = 'Leatherworking' , Icon= 'Interface\\Icons\\INV_Misc_ArmorKit_17' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:153:217:79:143|t', },
		Tailoring = { ID = 8, Name = 'Tailoring' , Icon= 'Interface\\Icons\\Trade_Tailoring' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:227:291:79:143|t', },
		Herbalism = { ID = 9, Name = 'Herbalism' , Icon= 'Interface\\Icons\\INV_Misc_Flower_02' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:38:102:153:217|t', },
		Skinning = { ID = 10, Name = 'Skinning' , Icon= 'Interface\\Icons\\INV_Misc_Pelt_Wolf_01' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:187:251:153:217|t', },
		Mining = { ID = 11, Name = 'Mining' , Icon= 'Interface\\Icons\\Spell_Fire_FlameBlades' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:112:176:153:217|t', },
		Cooking = { ID = 12, Name = 'Cooking', Icon = 'Interface\\Icons\\inv_misc_food_15' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:2:66:226:290|t', },
		Fishing = { ID = 13, Name = 'Fishing', Icon = 'Interface\\Icons\\Trade_Fishing' , FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:151:215:226:290|t', },
		FirstAid = { ID = 14, Name = 'FirstAid', Icon = 'Interface\\Icons\\Spell_Holy_SealOfSacrifice', FontStringIcon='|TInterface\\Addons\\Aleri\\Icons\\Professions\\IconTextures:28:28:0:0:512:512:76:140:226:290|t', },
	},
	ProfessionDescriptions = {
		Alchemy = 'Mix potions, elixirs, flasks, oils and other alchemical substances into vials using herbs and other reagents. Your concoctions can restore health and mana, enhance attributes, or provide any number of other useful (or not-so-useful) effects. High level alchemists can also transmute essences and metals into other essences and metals. Alchemists can specialize as a Master of Potions, Master of Elixirs, or a Master of Transmutation.',
		Blacksmithing = 'Smith various melee weapons, mail and plate armor, and other useful trade goods like skeleton keys, shield-spikes and weapon chains to prevent disarming. Blacksmiths can also make various stones to provide temporary physical buffs to weapons.',
		Enchanting = 'Imbue all manner of equipable items with magical properties and enhancements using dusts, essences and shards gained by disenchanting (breaking down) magical items that are no longer useful. Enchanters can also make a few low-level wands, as well as oils that can be applied to weapons providing a temporary magical buff.',
		Engineering = 'Engineer a wide range of mechanical devices—including trinkets, guns, goggles, explosives and mechanical pets—using metal, minerals, and stone. As most engineering products can only be used by suitably adept engineers, it is not as profitable as the other professions; it is, however, often taken to be one of the most entertaining, affording its adherents with numerous unconventional and situationally useful abilities. Engineers can specialize as Goblin or Gnomish engineers.',
		Inscription = "Inscribe glyphs that modify existing spells and abilities for all classes, in addition to various scrolls, staves, playing cards and off-hand items. A scribe can also create vellums for the storing of an Enchanter\'s spells and scribe-only scrolls used to teleport around the world (albeit a tad randomly). Also teaches you the [Milling] ability, which crushes herbs into various pigments used, in turn, for a scribe's ink."	,
		Jewelcrafting = 'Cut and polish powerful gems that can be socketed into armor and weapons to augment their attributes or fashioned into rings, necklaces, trinkets, and jeweled headpieces. Also teaches you the [Prospecting] ability, which sifts through raw ores to uncover the precious gems needed for your craft.',
		Leatherworking = 'Work leather and hides into goods such as leather and mail armor, armor kits, and some capes. Leatherworkers can also produce a number of utility items including large profession bags, ability-augmenting drums, and riding crops to increase mount speed.'	,
		Tailoring = 'Sew cloth armor and many kinds of bags using dye, thread and cloth gathered from humanoid enemies during your travels. Tailors can also fashion nets to slow enemies with, rideable flying carpets, and magical threads which empower items they are stitched into.',
	},	
	SpecToRole = {
		DRUID = { Restoration = 'Healer', Balance = 'Damage', Cat = 'Damage',  Bear = 'Tank', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		SHAMAN = { Elemental = 'Damage', Enhancement = 'Damage', Restoration = 'Healer', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		HUNTER = { Marksmanship = 'Damage', ['Beast Master'] = 'Damage', Survival = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		PALADIN = { Holy = 'Healer', Protection = 'Tank', Retribution = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		WARRIOR = { Arms = 'Damage', Fury = 'Damage', Protection = 'Tank', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		ROGUE = { Assassination = 'Damage', Combat = 'Damage', Subtlety = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		PRIEST = { Holy = 'Healer', Discipline = 'Healer', Shadow = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		WARLOCK = { Demonology = 'Damage', Affliction = 'Damage', Destruction = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		MAGE = { Frost = 'Damage', Fire = 'Damage', Arcane = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
		DEATHKNIGHT = { Frost = 'Tank', Blood = 'Tank', Unholy = 'Damage', unknown = 'Unknown', pvp = 'PvP', ['-'] = '-' },
	},
	DamageRoleType = {
		DRUID = { Restoration = 'Healer', Balance = 'Ranged', Cat = 'Melee',  Bear = 'Tank', unknown = 'Unknown', pvp = 'PvP' },
		SHAMAN = { Elemental = 'Ranged', Enhancement = 'Melee', Restoration = 'Healer', unknown = 'Unknown', pvp = 'PvP' },
		HUNTER = { Marksmanship = 'Ranged', ['Beast Master'] = 'Ranged', Survival = 'Ranged', unknown = 'Unknown', pvp = 'PvP' },
		PALADIN = { Holy = 'Healer', Protection = 'Tank', Retribution = 'Melee', unknown = 'Unknown', pvp = 'PvP' },
		WARRIOR = { Arms = 'Melee', Fury = 'Melee', Protection = 'Tank', unknown = 'Unknown', pvp = 'PvP' },
		ROGUE = { Assassination = 'Melee', Combat = 'Melee', Subtlety = 'Melee', unknown = 'Unknown', pvp = 'PvP' },
		PRIEST = { Holy = 'Healer', Discipline = 'Healer', Shadow = 'Ranged', unknown = 'Unknown', pvp = 'PvP' },
		WARLOCK = { Demonology = 'Ranged', Affliction = 'Ranged', Destruction = 'Ranged', unknown = 'Unknown', pvp = 'PvP' },
		MAGE = { Frost = 'Ranged', Fire = 'Ranged', Arcane = 'Ranged', unknown = 'Unknown', pvp = 'PvP' },
		DEATHKNIGHT = { Frost = 'Tank', Blood = 'Tank', Unholy = 'Melee', unknown = 'Unknown', pvp = 'PvP' },
	},
	Classes = {
		DEATHKNIGHT = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:64:128:128:192|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\DEATHKNIGHT", RGB={ r = 0.77, g = 0.12, b = 0.23}, FontColour='|cffC41F3B', Specializations={'Frost','Blood','Unholy',} },
		DRUID = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:192:256:0:64|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\DRUID", RGB={ r = 1.00, g = 0.49, b = 0.04}, FontColour='|cffFF7D0A', Specializations={'Balance','Restoration','Cat','Bear',} },
		HUNTER = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\HUNTER", RGB={ r = 0.67, g = 0.83, b = 0.45}, FontColour='|cffABD473', Specializations={'Marksmanship','Beast Master','Survival',} },
		MAGE = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:64:128:0:64|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\MAGE", RGB={ r = 0.25, g = 0.78, b = 0.92}, FontColour='|cff40C7EB', Specializations={'Fire','Frost','Arcane',} },
		PALADIN = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:128:192|t",  Icon="Interface\\Addons\\Aleri\\Icons\\Class\\PALADIN", RGB={ r = 0.96, g = 0.55, b = 0.73}, FontColour='|cffF58CBA', Specializations={'Protection','Retribution','Holy',} },
		PRIEST = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:128:192:64:128|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\PRIEST", RGB={ r = 1.00, g = 1.00, b = 1.00}, FontColour='|cffFFFFFF', Specializations={'Holy','Discipline','Shadow',} },
		ROGUE = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:128:192:0:64|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\ROGUE", RGB={ r = 1.00, g = 0.96, b = 0.41}, FontColour='|cffFFF569', Specializations={'Assassination','Combat','Subtlety',} },
		SHAMAN = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:64:128:64:128|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\SHAMAN", RGB={ r = 0.00, g = 0.44, b = 0.87}, FontColour='|cff0070DE', Specializations={'Elemental','Restoration','Enhancement',} },
		WARLOCK = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:192:256:64:128|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\WARLOCK", RGB={ r = 0.53, g = 0.53, b =	0.93}, FontColour='|cff8787ED', Specializations={'Affliction','Demonology','Destruction',} },
		WARRIOR = { FontStringIcon="|TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:0:64|t", Icon="Interface\\Addons\\Aleri\\Icons\\Class\\WARRIOR", RGB={ r = 0.78, g = 0.61, b = 0.43}, FontColour='|cffC79C6E', Specializations={'Protection','Arms','Fury',} },
	},
	RoleIcons = {
		Healer = { Icon = 'Interface\\Addons\\Aleri\\Icons\\Roles\\Healer', FontStringIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t", FontStringIconLARGE = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:32:32:0:0:64:64:20:39:1:20|t" },
		Tank = { Icon = 'Interface\\Addons\\Aleri\\Icons\\Roles\\Tank', FontStringIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t", FontStringIconLARGE = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:32:32:0:0:64:64:0:19:22:41|t" },
		Melee = { Icon = 'Interface\\Addons\\Aleri\\Icons\\Roles\\Melee', FontStringIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t", FontStringIconLARGE = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:32:32:0:0:64:64:20:39:22:41|t" },
		Ranged = { Icon = 'Interface\\Addons\\Aleri\\Icons\\Roles\\Ranged', FontStringIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t", FontStringIconLARGE = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:32:32:0:0:64:64:20:39:22:41|t" },
		Damage = { Icon = 'Interface\\Addons\\Aleri\\Icons\\Roles\\Ranged', FontStringIcon = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t" },
	},
	SpecIcons = {
		DRUID = { Balance = 'Interface\\Icons\\Spell_Nature_Starfall', Feral = '', Restoration = 'Interface\\Icons\\Spell_Nature_HealingTouch' },
		DEATHKNIGHT = { Frost = '', Blood = '', Unholy = ''},
		HUNTER = { ['Beast Master'] = 'Interface\\Icons\\Ability_Hunter_BeastTaming', Marksmanship = '', Survival = ''},
		ROGUE = { Assassination = '', Combat = '', Subtlety = ''},
		MAGE = { Frost = '', Fire = '', Arcane = ''},
		PRIEST = { Holy = '', Discipline = '', Shadow = ''},
		SHAMAN = { Elemental = 'Interface\\Icons\\Spell_Nature_Lightning', Enhancement = 'Interface\\Icons\\Spell_Nature_LightningShield', Restoration = 'Interface\\Icons\\Spell_Nature_MagicImmunity' },
		WARLOCK = { Demonology = '', Affliction = 'Interface\\Icons\\Spell_Shadow_DeathCoil', Destruction = ''},
		WARRIOR = { Arms = '', Fury = '', Protection = ''},
		PALADIN = { Retribution = '', Holy = '', Protection = ''}, 
	},
	SpecFontStringIcons = { 
		DRUID = { ['-'] = '', Balance = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:189:252:0:63|t", Bear = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:189:252:63:126|t", Cat = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:189:252:63:126|t", Feral = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:189:252:63:126|t", Restoration = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:189:252:126:188|t" },
		DEATHKNIGHT = { ['-'] = '', Frost = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:567:640:63:126|t", Blood = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:567:640:0:63|t", Unholy = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:567:640:126:188|t"},
		HUNTER = { ['-'] = '', ['Beast Master'] = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:252:315:0:63|t", Marksmanship = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:252:315:63:126|t", Survival = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:252:315:126:188|t"},
		ROGUE = { ['-'] = '', Assassination = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:126:188:0:63|t", Combat = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:126:188:63:126|t", Subtlety = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:126:188:126:188|t"},
		MAGE = { ['-'] = '', Frost = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:63:126:126:188|t", Fire = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:63:126:63:126|t", Arcane = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:63:126:0:63|t"},
		PRIEST = { ['-'] = '', Holy = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:378:441:63:126|t", Discipline = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:378:441:0:63|t", Shadow = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:378:441:126:188|t"},
		SHAMAN = { ['-'] = '', Elemental = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:315:378:0:63|t", Enhancement = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:315:378:63:126|t", Restoration = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:315:378:126:188|t" },
		WARLOCK = { ['-'] = '', Demonology = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:441:504:63:126|t", Affliction = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:441:504:0:63|t", Destruction = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:441:504:126:188|t"},
		WARRIOR = { ['-'] = '', Arms = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:0:63:0:63|t", Fury = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:0:63:63:126|t", Protection = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:0:63:126:188|t"},
		PALADIN = { ['-'] = '', Retribution = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:504:567:126:188|t", Holy = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:504:567:0:63|t", Protection = "|TInterface\\Addons\\Aleri\\Icons\\Specialization\\Textures:24:24:0:0:1024:256:504:567:63:126|t"},
	},
	Roles = {
		Tank = { DEATHKNIGHT = 0, WARRIOR = 0, DRUID = 0, PALADIN = 0 },
		Healer = { DRUID = 0, SHAMAN = 0, PRIEST = 0, PALADIN = 0 },
		Ranged = { DRUID = 0, SHAMAN = 0, PRIEST = 0, MAGE = 0, WARLOCK = 0, HUNTER = 0 },
		Melee = { DRUID = 0, SHAMAN = 0, PALADIN = 0, WARRIOR = 0, ROGUE = 0, DEATHKNIGHT = 0 }
	},
	RaceIcons = {
		FEMALE = {
			HUMAN = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\HUMAN',
			DWARF = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\DWARF',
			NIGHTELF = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\NIGHTELF',
			GNOME = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\GNOME',
			DRAENEI = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\DRAENEI',
			ORC = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\ORC',
			TROLL = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\TROLL',
			TAUREN = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\TAUREN',
			SCOURGE = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\SCOURGE',
			BLOODELF = 'Interface\\Addons\\Aleri\\Icons\\Race\\FEMALE\\BLLODELF',
		},
		MALE = {
			HUMAN = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\HUMAN',
			DWARF = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\DWARF',
			NIGHTELF = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\NIGHTELF',
			GNOME = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\GNOME',
			DRAENEI = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\DRAENEI',
			ORC = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\ORC',
			TROLL = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\TROLL',
			TAUREN = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\TAUREN',
			SCOURGE = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\SCOURGE',
			BLOODELF = 'Interface\\Addons\\Aleri\\Icons\\Race\\MALE\\BLLODELF',
		},
	},
	OnlineStatusFontStringIcon = {
		Offline = "|TInterface\\Addons\\Aleri\\Icons\\OBJECTICONS:24:24:0:0:256:256:160:192:0:32|t",
		Online = "|TInterface\\Addons\\Aleri\\Icons\\OBJECTICONS:24:24:0:0:256:256:224:256:0:32|t",
	},
	ClassList = { 
		'DEATHKNIGHT', 
		'DRUID', 
		'HUNTER', 
		'MAGE', 
		'PALADIN', 
		'PRIEST', 
		'SHAMAN', 
		'ROGUE', 
		'WARLOCK', 
		'WARRIOR' 
	},
	ProfessionsThatShare = {
		Alchemy = true,
		Blacksmithing = true,
		Enchanting = true,
		Engineering = true,
		Inscription = true,
		Jewelcrafting = true,
		Leatherworking = true,
		Tailoring = true,
	}
}
















