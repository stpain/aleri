--[==[
    Aleri

    All Rights Reserved 
    
    © stpain 2020
]==]--


--[==[

keep this for reference

function AleriInterfaceMemberDetailFrameOffSpecDD_Init()
    UIDropDownMenu_Initialize(AleriInterfaceMemberDetailFrameOffSpecDD, function(self, level, menuList)
        local info = UIDropDownMenu_CreateInfo()
        if (level or 1) == 1 then
            for class, d in pairs(Aleri.DB.Classes) do
                --info.text = tostring(d.FontStringIcon..' '..d.FontColour..L[class]..'|r') --looks too small ?
                info.text = tostring(d.FontColour..L[class]..'|r')
                --info.icon = d.Icon --overlaps the arrow
                info.hasArrow = true
                info.keepShownOnClick = false
                info.menuList = d.Specializations
                UIDropDownMenu_AddButton(info)
            end
        else
            for k, spec in pairs(menuList) do
                info.text = tostring(L[spec])
                info.isNotRadio = true
                info.keepShownOnClick = false
                info.func = function() UIDropDownMenu_SetText(AleriInterfaceMemberDetailFrameOffSpecDD, spec) end
                UIDropDownMenu_AddButton(info, level)
            end
		end
	end)
end
]==]--

local AleriName, Aleri = ...

Aleri.Functions = {}

function Aleri.Functions.BroadcastPlayerUpdate(name, class, key, value, isNum)
	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		if player.Name == name then --and player.Class == class then
			if isNum == true then
				player[key] = tonumber(value)
			else
				player[key] = value
			end
			print(Aleri.PrintColour..'Aleri - '..name..' updated '..key)
		end
	end
end

function Aleri.Functions.ChackDatabaseForNumberErrors()
	for k, p in ipairs(AleriCharacterSettings.GuildDatabase) do
		p.Level = tonumber(p.Level)
		p.MainSpecGearScore = tonumber(p.MainSpecGearScore)
		p.OffSpecGearScore = tonumber(p.OffSpecGearScore)
		p.PrimaryProfLevel = tonumber(p.PrimaryProfLevel)
		p.SecondaryProfLevel = tonumber(SecondaryProfLevel)
		p.DKP = tonumber(p.DKP)
	end
end

function Aleri.Functions.UpdatePlayerInfoByKey(info)
	local data = {}
	local i = 1
	for d in string.gmatch(info, '[^:]+') do
		data[i] = d
		i = i + 1
	end
	--print(data[1], data[2], data[3], data[4])
	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		if player.Name == data[1] then -- and player.Class == data[2] then
			player[data[3]] = data[4]
			--print(Aleri.PrintColour..'Aleri - setting '..player.Name..' '..data[3]..' to new value: '..data[4])
			player.PrimaryProfLevel = tonumber(player.PrimaryProfLevel)
			player.SecondaryProfLevel = tonumber(player.SecondaryProfLevel)
			player.MainSpecGearScore = tonumber(player.MainSpecGearScore)
			player.OffSpecGearScore = tonumber(player.OffSpecGearScore)
		end
	end
end


function Aleri.Functions.ResetRoleTable()
Aleri.Database.Roles = {
	Tank = { DEATHKNIGHT = 0, WARRIOR = 0, DRUID = 0, PALADIN = 0 },
	Healer = { DRUID = 0, SHAMAN = 0, PRIEST = 0, PALADIN = 0 },
	Ranged = { DRUID = 0, SHAMAN = 0, PRIEST = 0, MAGE = 0, WARLOCK = 0, HUNTER = 0 },
	Melee = { DRUID = 0, SHAMAN = 0, PALADIN = 0, WARRIOR = 0, ROGUE = 0, DEATHKNIGHT = 0 }
}
end

function Aleri.Functions.RefreshRoleTable(level, level_max)
	--if level <= level_max then
		for k, p in ipairs(AleriCharacterSettings.GuildDatabase) do
			--if p.Level >= level and p.Level <= level_max then
				if p.MainSpec ~= 'Unknown' then
					if p.MainSpec == 'Beast Master' then
						Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class]['BeastMaster']][p.Class] = Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class]['BeastMaster']][p.Class] + 1
					else
						Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class][p.MainSpec]][p.Class] = Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class][p.MainSpec]][p.Class] + 1
					end
					if p.SecondarySpec == 'Beast Master' then
						Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class]['BeastMaster']][p.Class] = Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class]['BeastMaster']][p.Class] + 1
					else
						Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class][p.SecondarySpec]][p.Class] = Aleri.Database.Roles[Aleri.Database.SpecToRole[p.Class][p.SecondarySpec]][p.Class] + 1
					end
				end
			--end
		end
		for class, count in pairs(Aleri.Database.Roles.Healer) do
			Aleri.Guild.SpecRoleCountFontString.Healer[class]:SetText(count)
		end
		for class, count in pairs(Aleri.Database.Roles.Tank) do
			Aleri.Guild.SpecRoleCountFontString.Tank[class]:SetText(count)
		end
		for class, count in pairs(Aleri.Database.Roles.Melee) do
			Aleri.Guild.SpecRoleCountFontString.Melee[class]:SetText(count)
		end
		for class, count in pairs(Aleri.Database.Roles.Ranged) do
			Aleri.Guild.SpecRoleCountFontString.Range[class]:SetText(count)
		end
	--end
end

function Aleri.Functions.ResetClassCountTable()
Aleri.Database.ClassCount = {
	{ Class = 'DEATHKNIGHT', Count = 0 },
	{ Class = 'DRUID', Count = 0 },
	{ Class = 'HUNTER', Count = 0 },
	{ Class = 'MAGE', Count = 0 },
	{ Class = 'PALADIN', Count = 0 },
	{ Class = 'PRIEST', Count = 0 },
	{ Class = 'ROGUE', Count = 0 },
	{ Class = 'SHAMAN', Count = 0 },
	{ Class = 'WARLOCK', Count = 0},
	{ Class = 'WARRIOR', Count  = 0 },
	{ Class = 'Total', Count = 0 }
}
end

function Aleri.Functions.RefreshClassCountTable(level, level_max)
	if level <= level_max then
		local MaxPercent = 0
		for k, p in ipairs(AleriCharacterSettings.GuildDatabase) do
			if p.Level >= level and p.Level <= level_max then
				for k, v in ipairs(Aleri.Database.ClassCount) do
					if v.Class == p.Class then
						v.Count = v.Count + 1
						MaxPercent = MaxPercent + 1
					end
				end
			end
		end

		for k, v in pairs(Aleri.Database.ClassCount) do
			if v.Class == 'Total' then
				v.Count = MaxPercent
			end
		end
		
		table.sort(Aleri.Database.ClassCount, function(a, b) if a.Count ~= nil and b.Count ~= nil then return a.Count > b.Count end end)
		
		for k, v in ipairs(Aleri.Database.ClassCount) do
			Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetText(v.Class)
			Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetText(v.Count)
			Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetText(string.format("%.2f", ((v.Count/ MaxPercent) * 100))..'%')
			Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameClassStatusBars[k]:SetValue((v.Count/ MaxPercent) * 100)
			Aleri.Guild.PlayerFrameClassStatusBars[k]:SetStatusBarColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
		end
	else
		for k, v in ipairs(Aleri.Database.ClassCount) do
			Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetText(v.Class)
			Aleri.Guild.PlayerFrameFontStrings_ColumnOne[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetText(0)
			Aleri.Guild.PlayerFrameFontStrings_ColumnTwo[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetText('0.00')
			Aleri.Guild.PlayerFrameFontStrings_ColumnThree[k]:SetTextColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
			Aleri.Guild.PlayerFrameClassStatusBars[k]:SetValue(0)
			Aleri.Guild.PlayerFrameClassStatusBars[k]:SetStatusBarColor(Aleri.Database.ClassColours[v.Class].r, Aleri.Database.ClassColours[v.Class].g, Aleri.Database.ClassColours[v.Class].b)
		end
	end
end

function Aleri.Functions.RefreshRaidStats(sort, level)

	if sort == 'refresh' then
		sort = Aleri.Guild.RaidStatsSort
	end
	
	if Aleri.Guild.PlayerFrame.RaidStatsGridView ~= nil then
		local row = nil
		for i = #Aleri.Guild.PlayerFrame.RaidStatsGridView, 1, -1 do
			row = Aleri.Guild.PlayerFrame.RaidStatsGridView[i]			
			row.ClassIcon:Hide()
			row.Level:SetText(' ')
			row.Name:SetText(' ')
			row.MainSpec:SetText(' ')
			row.SecondarySpec:SetText(' ')
			row.PrimaryProf:SetText(' ')
			row.PrimaryProfLevel:SetText(' ')
			row.SecondaryProf:SetText(' ')
			row.SecondaryProfLevel:SetText(' ')
		end
	end
	
	if sort == 'none' then
	
	elseif sort == 'Class' or sort == 'Name' or sort == 'MainSpec' or sort == 'SecondarySpec' or sort == 'PrimaryProf' or sort == 'SecondaryProf' then
		table.sort(AleriCharacterSettings.GuildDatabase, function(a,b) if a[sort] ~= nil and b[sort] ~= nil then return a[sort] < b[sort] end end) -- a -> z print( a.Name, a[sort], b.name, b[sort])
	else
		table.sort(AleriCharacterSettings.GuildDatabase, function(a,b) if a[sort] ~= nil and b[sort] ~= nil then return a[sort] > b[sort] end end)	-- 9 -> 0 print(sort, a.Name, a[sort], b.name, b[sort])
	end
	
	--update listview display
	local row_id = 1
	local row_height = 25
	local vertical_row_offset = 8
	for r, player in ipairs(AleriCharacterSettings.GuildDatabase) do
			if player.Level >= level then
				--if fontstring exist - update it
				if Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id] ~= nil then
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].Level:SetText(player.Level)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].Level:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)					
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].ClassIcon:Show()				
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].ClassIcon.Texture:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..player.Class)				
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].Name:SetText(player.Name)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].Name:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].MainSpec:SetText(player.MainSpec)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].MainSpec:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)

					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondarySpec:SetText(player.SecondarySpec)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondarySpec:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)

					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].PrimaryProf:SetText(player.PrimaryProf)
					if player.PrimaryProf == 'z' then
						Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].PrimaryProf:SetText(' ')
					end
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].PrimaryProf:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].PrimaryProfLevel:SetText(player.PrimaryProfLevel)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].PrimaryProfLevel:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)				
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondaryProf:SetText(player.SecondaryProf)
					if player.SecondaryProf == 'z' then
						Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondaryProf:SetText(' ')
					end
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondaryProf:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)
					
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondaryProfLevel:SetText(player.SecondaryProfLevel)
					Aleri.Guild.PlayerFrame.RaidStatsGridView[row_id].SecondaryProfLevel:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)				
									
				else
					--create new fonstring if not exist
					local row = { ClassIcon = nil, ClassIconTexture = nil, Name = nil, Level = nil, MainSpec = nil, SecondarySpec = nil, PrimaryProf = nil, PrimaryProfLevel = nil, SecondaryProf = nil, SecondaryProfLevel = nil } 
					for k, v in ipairs(Aleri.Guild.PlayerFrame.RaidStatsColumns) do
						--if not icon make fontstring
						local fs = nil
						local fs_name = nil
						if v ~= 'ClassIcon' then
							fs_name = '_'..v..k
							fs = Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent:CreateFontString('AleriGuildPlayerFrameRaidStatsGridView'..fs_name, 'OVERLAY', 'GameFontNormal')
							fs:SetText(player[v])
							fs:SetTextColor(Aleri.Class.Colours[player.Class].r, Aleri.Class.Colours[player.Class].g, Aleri.Class.Colours[player.Class].b)
							row[v] = fs
						end
						-- set class icon
						if v == 'ClassIcon' then						
							local iconFrame = CreateFrame('FRAME', 'AleriGuildPlayerFrameRaidStatsIcon'..k, Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent)
							iconFrame:SetPoint('TOPLEFT', 158, ((row_id-1) * - row_height) - (vertical_row_offset - 4))
							iconFrame:SetSize(24, 24)
							iconFrame.Texture = iconFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
							iconFrame.Texture:SetAllPoints(iconFrame)
							iconFrame.Texture:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..player.Class)
							row['ClassIcon'] = iconFrame
						--set listview row id
						elseif v == 'Level' then
							fs:SetPoint('TOPLEFT', 0, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(40)
							fs:SetJustifyH('CENTER')
						elseif v == 'Name' then
							fs:SetPoint('TOPLEFT', 40, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(150)
							fs:SetJustifyH('LEFT')
						elseif v == 'MainSpec' then
							fs:SetPoint('TOPLEFT', 190, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(100)
							fs:SetJustifyH('LEFT')
						elseif v == 'SecondarySpec' then
							fs:SetPoint('TOPLEFT', 290, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(100)
							fs:SetJustifyH('LEFT')
						elseif v == 'PrimaryProf' then
							fs:SetPoint('TOPLEFT', 390, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(100)
							fs:SetJustifyH('LEFT')
						elseif v == 'PrimaryProfLevel' then
							fs:SetPoint('TOPLEFT', 490, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(50)
							fs:SetJustifyH('CENTER')	
						elseif v == 'SecondaryProf' then
							fs:SetPoint('TOPLEFT', 540, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(100)
							fs:SetJustifyH('LEFT')
						elseif v == 'SecondaryProfLevel' then
							fs:SetPoint('TOPLEFT', 640, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(50)
							fs:SetJustifyH('CENTER')						
						elseif v == 'Alt' then
						
						end					
					end
					table.insert(Aleri.Guild.PlayerFrame.RaidStatsGridView, row)
				end
				row_id = row_id + 1
			end
			

	end
	Aleri.Guild.PlayerFrame.RaidStatsFrameScrollFrameContent:SetHeight((row_id - 1) * (row_height))
end

function Aleri.Functions.RefreshProfessions(prof, sort)

	local temp_table = {}
	
	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		if player.PrimaryProf == prof then
			--print(player.Name, prof)
			table.insert(temp_table, { Name = player.Name, Class = player.Class, Prof = player.PrimaryProf, Level = player.PrimaryProfLevel } )
			--print(player.PrimaryProf)
		elseif player.SecondaryProf == prof then
			--print(player.Name, prof)
			table.insert(temp_table, { Name = player.Name, Class = player.Class, Prof = player.SecondaryProf, Level = player.SecondaryProfLevel } )
			--print(player.SecondaryProf)
		end
	end
	
	table.sort(temp_table, function(a, b) return a.Level > b.Level end)
	
	if Aleri.Guild.PlayerFrame.ProfessionRows ~= nil then
		local row = nil
		for i = #Aleri.Guild.PlayerFrame.ProfessionRows, 1, -1 do
			row = Aleri.Guild.PlayerFrame.ProfessionRows[i]			
			row.ClassIcon:Hide()
			row.Name:SetText(' ')
			row.Prof:SetText(' ')
			row.Level:SetText(' ')
			-- row.SecondaryProf:SetText(' ')
			-- row.SecondaryProfLevel:SetText(' ')
		end
	end
	
	-- if sort == 'PrimaryProf' then
		-- table.sort(AleriGlobalSettings.Database, function(a,b) if a.PrimaryProfLevel ~= nil and b.PrimaryProfLevel ~= nil then return a.PrimaryProfLevel > b.PrimaryProfLevel end end) -- a -> z print( a.Name, a[sort], b.name, b[sort])
	-- elseif sort == 'SecondaryProf' then
		-- table.sort(AleriGlobalSettings.Database, function(a,b) if a.SecondaryProfLevel ~= nil and b.SecondaryProfLevel ~= nil then return a.SecondaryProfLevel > b.SecondaryProfLevel end end) -- a -> z print( a.Name, a[sort], b.name, b[sort])
	-- end
	
	--update listview display
	local row_id = 1
	local row_height = 25
	local vertical_row_offset = 8
	for r, player in ipairs(temp_table) do
		--print(player.Name, player.Prof, player.Level)
			--if player[sort] == prof then
				--if fontstring exist - update it
				if Aleri.Guild.PlayerFrame.ProfessionRows[row_id] ~= nil then
					
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].ClassIcon:Show()				
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].ClassIcon.Texture:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..player.Class)				
					
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Name:SetText(player.Name)
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Name:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)
									
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Prof:SetText(player.Prof)
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Prof:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)
					
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Level:SetText(player.Level)
					Aleri.Guild.PlayerFrame.ProfessionRows[row_id].Level:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)				
					
					-- Aleri.Guild.PlayerFrame.ProfessionRows[row_id].SecondaryProf:SetText(player.SecondaryProf)
					-- Aleri.Guild.PlayerFrame.ProfessionRows[row_id].SecondaryProf:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)
					
					-- Aleri.Guild.PlayerFrame.ProfessionRows[row_id].SecondaryProfLevel:SetText(player.SecondaryProfLevel)
					-- Aleri.Guild.PlayerFrame.ProfessionRows[row_id].SecondaryProfLevel:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)				
									
				else
					--create new fonstring if not exist
					local row = { Id = nil, ClassIcon = nil, ClassIconTexture = nil, Name = nil, Prof = nil, Level = nil } 
					for k, v in ipairs(Aleri.Guild.PlayerFrame.ProfessionColumns) do
						--print(v, player[v])
						--if not icon make fontstring
						local fs = nil
						local fs_name = nil
						if v ~= 'ClassIcon' then
							fs_name = '_'..v..k
							fs = Aleri.Guild.PlayerFrame.ProfessionsFrameScrollFrameContent:CreateFontString('AleriGuildPlayerFrameProfessionsFrame'..fs_name, 'OVERLAY', 'GameFontNormal')
							fs:SetText(player[v])
							fs:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)
							row[v] = fs
						end
						-- set class icon
						if v == 'ClassIcon' then						
							local iconFrame = CreateFrame('FRAME', 'AleriGuildPlayerFrameRaidStatsIcon'..k, Aleri.Guild.PlayerFrame.ProfessionsFrameScrollFrameContent)
							iconFrame:SetPoint('TOPLEFT', 145, ((row_id-1) * - row_height) - (vertical_row_offset - 4))
							iconFrame:SetSize(24, 24)
							iconFrame.Texture = iconFrame:CreateTexture('$parent_Texture', 'BACKGROUND')
							iconFrame.Texture:SetAllPoints(iconFrame)
							iconFrame.Texture:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..player.Class)
							row['ClassIcon'] = iconFrame
						--set listview row id
						elseif v == 'Name' then
							fs:SetPoint('TOPLEFT', 30, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(120)
							fs:SetJustifyH('LEFT')
							--fs:SetText(player.Name)
						elseif v == 'Prof' then
							fs:SetPoint('TOPLEFT', 180, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(100)
							fs:SetJustifyH('LEFT')
							--fs:SetText(player.Prof)
						elseif v == 'Level' then
							fs:SetPoint('TOPLEFT', 280, ((row_id-1) * - row_height) - vertical_row_offset)
							fs:SetWidth(50)
							fs:SetJustifyH('CENTER')
							--fs:SetText(player.Level)
						-- elseif v == 'SecondaryProf' then
							-- fs:SetPoint('TOPLEFT', 330, ((row_id-1) * - row_height) - vertical_row_offset)
							-- fs:SetWidth(100)
							-- fs:SetJustifyH('LEFT')
						-- elseif v == 'SecondaryProfLevel' then
							-- fs:SetPoint('TOPLEFT', 430, ((row_id-1) * - row_height) - vertical_row_offset)
							-- fs:SetWidth(50)
							-- fs:SetJustifyH('CENTER')						
						elseif v == 'Alt' then
						
						end					
					end
					table.insert(Aleri.Guild.PlayerFrame.ProfessionRows, row)
				end
				row_id = row_id + 1
			--end
			

	end
	Aleri.Guild.PlayerFrame.ProfessionsFrameScrollFrameContent:SetHeight((row_id - 1) * (row_height))
end



-- Aleri.Guild.PlayerFrame.RaidStatsFrame:SetBackdrop({bgFile = "Interface/Tooltips/UI-Tooltip-Background", 
                                            -- edgeFile = "Interface/Tooltips/UI-Tooltip-Border", 
                                            -- tile = true, tileSize = 8, edgeSize = 8, 
                                            -- insets = { left = 2, right = 2, top = 2, bottom = 2 }});
-- Aleri.Guild.PlayerFrame.RaidStatsFrame:SetBackdropColor(1,1,1,0.1)

function Aleri.Functions.ParsePlayerData(data)
	local i = 1
	local data_sent = {}
	for d in string.gmatch(data, '[^:]+') do
		data_sent[i] = d
		i = i + 1
	end	
	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		if player.Name == data_sent[1] then
			player.Level = tonumber(data_sent[3])
			player.ilvl = tonumber(data_sent[4])
			player.Gender = data_sent[5]
			player.Race = data_sent[6]
			player.Cooking = tonumber(data_sent[7])
			player.Fishing = tonumber(data_sent[8])
			player.FirstAid = tonumber(data_sent[9])
			player.PrimaryProf = data_sent[10]
			player.PrimaryProfLevel = tonumber(data_sent[11])
			player.SecondaryProf = data_sent[12]
			player.SecondaryProfLevel = tonumber(data_sent[13])
			player.INVSLOT_HEAD = tonumber(data_sent[14])
			player.INVSLOT_NECK = tonumber(data_sent[15])
			player.INVSLOT_SHOULDER = tonumber(data_sent[16])
			player.INVSLOT_CHEST = tonumber(data_sent[17])
			player.INVSLOT_WAIST = tonumber(data_sent[18])
			player.INVSLOT_LEGS = tonumber(data_sent[19])
			player.INVSLOT_FEET = tonumber(data_sent[20])
			player.INVSLOT_WRIST = tonumber(data_sent[21])
			player.INVSLOT_HAND = tonumber(data_sent[22])
			player.INVSLOT_FINGER1 = tonumber(data_sent[23])
			player.INVSLOT_FINGER2 = tonumber(data_sent[24])
			player.INVSLOT_TRINKET1 = tonumber(data_sent[25])
			player.INVSLOT_TRINKET2 = tonumber(data_sent[26])
			player.INVSLOT_BACK = tonumber(data_sent[27])
			player.INVSLOT_MAINHAND = tonumber(data_sent[28])
			player.INVSLOT_OFFHAND = tonumber(data_sent[29])
			player.INVSLOT_RANGED = tonumber(data_sent[30])
			player.CurrentGearScore = tonumber(data_sent[31])
			player.CurrentSpec = data_sent[32]
			player.MainSpec = data_sent[33]
			player.SecondarySpec = data_sent[34]
			player.AchievementPoints = tonumber(data_sent[35])
		end
	end
end


function Aleri.Functions.PopulateMemberList()

	GuildRoster()

	local online = {}
	--Aleri.UI.MemberListPop = {}
	local TotalMembers, OnlineMembers, _ = GetNumGuildMembers()
	for i = 1, TotalMembers do
		local name, rankName, rankIndex, level, classDisplayName, zone, publicNote, officerNote, isOnline, status, class, achievementPoints, achievementRank, isMobile, canSoR, repStanding, guid = GetGuildRosterInfo(i)
		if isOnline == 1 then
			--print('adding', name, 'to temp')
			table.insert(online, { Name = name, Level = level })
		end
	end
	
	Aleri.UI.MemberFrame.OnlineCount:SetText('Online Members '..#online)

	local b = 0
	if Aleri.UI.MemberListPop ~= nil then
		for k, frame in ipairs(Aleri.UI.MemberListPop) do
			frame.PlayerFrame:Hide()
			frame.PlayerFrame:SetText(nil)
			--print('hidden button', k)
			b = b + 1
		end
	end

	table.sort(online, function(a, b) return a.Name < b.Name end)
	table.sort(AleriCharacterSettings.GuildDatabase, function(a, b) return a.Name < b.Name end)
	
	local row = 1
	for k, player in ipairs(AleriCharacterSettings.GuildDatabase) do
		for j, p in ipairs(online) do
			if p.Name == player.Name then
				--print('db entry of', player.Name, 'online entry of', p.Name, 'row id', row)
				if Aleri.UI.MemberListPop[row] ~= nil then
					--print('editing button', row)
					Aleri.UI.MemberListPop[row].PlayerFrame:Show()
					Aleri.UI.MemberListPop[row].PlayerFrame:SetText(player.Name)
					Aleri.UI.MemberListPop[row].PlayerFrame:SetScript('OnClick', function(self)
						--Aleri.Guild.ScanGuildRoster()
						--print('clicked', player.Name)
						--Aleri.RequestDataFrom = player.Name
						SendAddonMessage('aleri-g-p-data', 'Data request', 'GUILD')
						Aleri.Functions.UpdateMemberFrameFromlist(player.Name) 
					end)
				else
					--print('adding button', row)
					local f = CreateFrame('BUTTON', 'AleriMembersList'..k, Aleri.UI.MemberFrame.MemberListScrollFrameContent, "UIPanelButtonTemplate")
					f:SetPoint('TOPLEFT', 12, (((row - 1) * 22) * -1) -4)
					f:SetSize(140, 20)
					f:SetText(player.Name)
					f:SetScript('OnClick', function(self)
						--Aleri.Guild.ScanGuildRoster()
					--print('clicked', player.Name)
						--Aleri.RequestDataFrom = player.Name
						--SendAddonMessage('aleri-request', tostring('data:'..player.Name), 'GUILD')
						Aleri.Functions.UpdateMemberFrameFromlist(player.Name) 
					end)
					-- local fs = f:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
					-- fs:SetText(player.Name)
					-- fs:SetTextColor(Aleri.Database.ClassColours[player.Class].r, Aleri.Database.ClassColours[player.Class].g, Aleri.Database.ClassColours[player.Class].b)
					-- fs:SetPoint('TOPLEFT', 2, -2)
					table.insert(Aleri.UI.MemberListPop, { Id = k, PlayerFrame = f } )
				end
				row = row + 1
			end
		end
	end
	
	--print('member list update finished', #online, 'online count', b, 'buttons', row, 'rows')
	
	Aleri.UI.MemberFrame.MemberListScrollFrameContent:SetHeight(row * 20)
end

function Aleri.Functions.UpdateMemberFrameFromlist(player)

	for k, fs in ipairs(Aleri.UI.MemberFrame.EquipemntLinks) do
		fs.FontString:SetText(' ')
	end
	
	Aleri.UI.MemberFrame.CookingLevel:SetText(' ')
	Aleri.UI.MemberFrame.FirstAidLevel:SetText(' ')
	Aleri.UI.MemberFrame.FishingLevel:SetText(' ')
	Aleri.UI.MemberFrame.ProfessionA:SetText('-')
	Aleri.UI.MemberFrame.ProfessionALevel:SetText('-')
	Aleri.UI.MemberFrame.ProfessionAIcon.Texture:SetTexture(nil)
	Aleri.UI.MemberFrame.ProfessionBIcon.Texture:SetTexture(nil)
	
	Aleri.UI.MemberFrame.RaceIcon.Texture:SetTexture(nil)
	Aleri.UI.MemberFrame.PlayerClassIcon.Texture:SetTexture(nil)
	
	Aleri.UI.MemberFrame.PrimarySpecName:SetText(' ')
	Aleri.UI.MemberFrame.PrimarySpecIcon.Texture:SetTexture(nil)
	
	Aleri.UI.MemberFrame.PlayerAchievePoints:SetText(' ')
	
	for k, p in ipairs(AleriCharacterSettings.GuildDatabase) do
		if p.Name == player then
			
			for k, fs in ipairs(Aleri.UI.MemberFrame.EquipemntLinks) do				
				fs.FontString:SetText(' ')				
				if p[fs.Slot] ~= 0 and p[fs.Slot] ~= nil then
					local link = select(2, GetItemInfo(p[fs.Slot]))
					--local itemString = string.match(link, "item[%-?%d:]+")				
					fs.FontString:SetText(link) --('|Hitem:'..itemString..'|h'..link..'|h')
				end
			end
			
			if p.ilvl ~= nil then
				Aleri.UI.MemberFrame.Playerilvl:SetText('Current ilvl :  '..p.ilvl)
			else
				Aleri.UI.MemberFrame.Playerilvl:SetText('Current ilvl : Unknown')
			end
			if p.CurrentGearScore ~= nil then
				Aleri.UI.MemberFrame.PlayerGS:SetText('GearScore : '..p.CurrentGearScore)
			else
				Aleri.UI.MemberFrame.PlayerGS:SetText('GearScore : Unknown')
			end
			-- Aleri.UI.MemberFrame.Playerilvl:SetText('Current ilvl :  '..p.ilvl)
			-- Aleri.UI.MemberFrame.PlayerGS:SetText('GearScore : '..p.CurrentGearScore)
			
			if p.Gender ~= nil and p.Race ~= nil then
			Aleri.UI.MemberFrame.RaceIcon.Texture:SetTexture("Interface\\Addons\\Aleri\\RaceIcons\\"..p.Gender.."\\"..p.Race)
			end
			
			Aleri.UI.MemberFrame.CookingLevel:SetText(p.Cooking)
			Aleri.UI.MemberFrame.FirstAidLevel:SetText(p.FirstAid)
			Aleri.UI.MemberFrame.FishingLevel:SetText(p.Fishing)
			Aleri.UI.MemberFrame.ProfessionA:SetText(p.PrimaryProf)
			Aleri.UI.MemberFrame.ProfessionALevel:SetText(p.PrimaryProfLevel)
			if p.PrimaryProf ~= '-' then
				Aleri.UI.MemberFrame.ProfessionAIcon.Texture:SetTexture(Aleri.Database.ProfessionIcons[p.PrimaryProf])
			end				
			Aleri.UI.MemberFrame.ProfessionB:SetText(p.SecondaryProf)
			Aleri.UI.MemberFrame.ProfessionBLevel:SetText(p.SecondaryProfLevel)
			if p.SecondaryProf ~= '-' then
				Aleri.UI.MemberFrame.ProfessionBIcon.Texture:SetTexture(Aleri.Database.ProfessionIcons[p.SecondaryProf])
			end

			Aleri.UI.MemberFrame.PrimarySpecName:SetText(p.CurrentSpec)
			if p.CurrentSpec == 'Beast Mastery' then
				Aleri.UI.MemberFrame.PrimarySpecIcon.Texture:SetTexture(Aleri.Database.SpecIcons[p.Class]['BeastMastery'])
			else
				Aleri.UI.MemberFrame.PrimarySpecIcon.Texture:SetTexture(Aleri.Database.SpecIcons[p.Class][p.CurrentSpec])
			end
			
			Aleri.UI.MemberFrame.PlayerName:SetText(p.Name)
			Aleri.UI.MemberFrame.PlayerName:SetTextColor(Aleri.Database.ClassColours[p.Class].r, Aleri.Database.ClassColours[p.Class].g, Aleri.Database.ClassColours[p.Class].b)
			
			Aleri.UI.MemberFrame.PlayerLevel:SetText('Level '..p.Level)
			
			Aleri.UI.MemberFrame.PlayerClassIcon.Texture:SetTexture("Interface\\Addons\\Aleri\\ClassIcons\\"..p.Class)
			
			Aleri.UI.MemberFrame.PlayerAchievePoints:SetText('Achievement Points '..p.AchievementPoints)
			
		end
	end


	
	
	
	
end

function Aleri.Functions.GetPlayerData()

	local _, class, _, race, gender, name, realm = GetPlayerInfoByGUID(UnitGUID('player'))
	race = race:Upper()
	if gender == 3 then
		gender = 'FEMALE'
	else
		gender = 'MALE'
	end	
	for s = 1, GetNumSkillLines() do
		local skill, _, _, level, _, _, _, _, _, _, _, _, _ = GetSkillLineInfo(s)
		if skill == 'Fishing' then 
			myCharacter.Fishing = level
		elseif skill == 'Cooking' then
			myCharacter.Cooking = level
		elseif skill == 'First Aid' then
			myCharacter.FirstAid = level
		else
			for k, prof in ipairs(Aleri.Database.Professions) do
				--if skill == 'Leatherworking' then print('found LW') end
				if skill == prof.Name then
					if myCharacter.ProfA == '-' then
						myCharacter.ProfA = skill
						myCharacter.ProfALevel = level
					elseif myCharacter.ProfB == '-' then
						myCharacter.ProfB = skill
						myCharacter.ProfBLevel = level
					end
				end
			end
		end
	end	
	local itemlvl = 0
	local itemCount = 0
	for k, s in ipairs(Aleri.DB.InventorySlots) do
		myCharacter[s.Name] = GetInventoryItemID('player', s.Id)
		if myCharacter[s.Name] ~= nil then
			local iName, iLink, iRarety, ilvl = GetItemInfo(myCharacter[s.Name])
			itemlvl = itemlvl + ilvl
			itemCount = itemCount + 1
		else
			myCharacter[s.Name] = '-'
		end
	end	
	myCharacter.ilvl = math.floor(itemlvl/itemCount)
	--print(myCharacter.ilvl)
	--myCharacter.ilvl = string.format("%.0f", itemlvl/itemCount)	
	myCharacter.AchievementPoints = GetTotalAchievementPoints()
	--print('points', myCharacter.AchievementPoints)
	
	if Aleri.GearScoreLiteDetected == true then
		myCharacter.CurrentGearScore = Aleri.Functions.GetCurrentGearScore()
	else
		myCharacter.CurrentGearScore = '-'
	end
	
	--get dual spec info?
	local ds = GetNumTalentGroups()
	if ds == 1 or ds == '1' then 
		ds = false
	elseif ds == 2 or ds == '2' then
		ds = true
	else
		ds = 'nil'
	end
	--print('ds',ds)
	
	local current_talents = {}
	for i = 1, 3 do
		table.insert( current_talents, { Name = select(1, GetTalentTabInfo(i, nil, nil, GetActiveTalentGroup())), Icon = select(2, GetTalentTabInfo(i, nil, nil, GetActiveTalentGroup())), Points = select(3, GetTalentTabInfo(i, nil, nil, GetActiveTalentGroup())), Background = select(4, GetTalentTabInfo(i, nil, nil, GetActiveTalentGroup())) } )
	end
	table.sort(current_talents, function(a, b) return a.Points > b.Points end)
	myCharacter.CurrentSpec = current_talents[1].Name
	--myCharacter.CurrentSpecIcon = current_talents[1].Icon
	
	local primary_talents = {}
	for i = 1, 3 do
		table.insert( primary_talents, { Name = select(1, GetTalentTabInfo(i, nil, nil, 1)), Icon = select(2, GetTalentTabInfo(i, nil, nil, 1)), Points = select(3, GetTalentTabInfo(i, nil, nil, 1)), Background = select(4, GetTalentTabInfo(i, nil, nil, 1)) } )
	end
	table.sort(primary_talents, function(a, b) return a.Points > b.Points end)
	myCharacter.MainSpec = primary_talents[1].Name	
	--myCharacter.MainSpecIcon = primary_talents[1].Icon

	local secondary_talents = {}
	for i = 1, 3 do
		table.insert( secondary_talents, { Name = select(1, GetTalentTabInfo(i, nil, nil, 2)), Icon = select(2, GetTalentTabInfo(i, nil, nil, 2)), Points = select(3, GetTalentTabInfo(i, nil, nil, 2)), Background = select(4, GetTalentTabInfo(i, nil, nil, 2)) } )
	end
	table.sort(secondary_talents, function(a, b) return a.Points > b.Points end)
	myCharacter.SecondarySpec = secondary_talents[1].Name	
	--myCharacter.SecondarySpecIcon = secondary_talents[1].Icon
	
	--dual spec fix
	if ds == false then
		myCharacter.MainSpec = myCharacter.CurrentSpec
		myCharacter.SecondarySpec = '-'
		--print('character not DS')
	end
	
	--print(myCharacter.CurrentSpecIcon, myCharacter.MainSpec, myCharacter.SecondarySpec)
	
	--myCharacterDataString = tostring(myCharacter.Name = '-' ,Class..':'..myCharacter.Level..':'..myCharacter.ilvl..':'..myCharacter.Gender..':'..myCharacter.Race..':'..myCharacter.Cooking..':'..myCharacter.Fishing..':'..myCharacter.FirstAid..':'..myCharacter.ProfA..':'..myCharacter.ProfALevel..':'..myCharacter.ProfB..':'..myCharacter.ProfBLevel..':'..myCharacter.INVSLOT_HEAD..':'..myCharacter.INVSLOT_NECK..':'..myCharacter.INVSLOT_SHOULDER..':'..myCharacter.INVSLOT_CHEST..':'..myCharacter.INVSLOT_WAIST..':'..myCharacter.INVSLOT_LEGS..':'..myCharacter.INVSLOT_FEET..':'..myCharacter.INVSLOT_WRIST..':'..myCharacter.INVSLOT_HAND..':'..myCharacter.INVSLOT_FINGER1..':'..myCharacter.INVSLOT_FINGER2..':'..myCharacter.INVSLOT_TRINKET1..':'..myCharacter.INVSLOT_TRINKET2..':'..myCharacter.INVSLOT_BACK..':'..myCharacter.INVSLOT_MAINHAND..':'..myCharacter.INVSLOT_OFFHAND..':'..myCharacter.INVSLOT_RANGED..':'..myCharacter.CurrentGearScore..':'..myCharacter.CurrentSpec..':'..myCharacter.MainSpec..':'..myCharacter.SecondarySpec..':'..myCharacter.AchievementPoints)

	--return myCharacterDataString
end

function Aleri.Functions.GetCurrentGearScore()
	local gs = 0
	if Aleri.GearScoreLiteDetected == true then
		gs = select(1, GearScore_GetScore(UnitName("player"), "player")) -- borrowed from gearscore addon
		--print(Aleri.PrintColour..'Aleri - GearScoreLite detected, current gear score: '..gs)
	end
	return tonumber(gs)
end
