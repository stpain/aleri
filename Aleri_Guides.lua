--[==[
    Aleri

    All Rights Reserved 
    
    © stpain 2020
]==]--

local AleriName, Aleri = ...

Aleri.Guides = {
    WOTLK = {
       ['Naxxaramus'] = {
           ['10 Normal'] = {
               GearScore = 4200,
               Gems = 'None',
               Enchants = 'None',
               Tanks = '20k HP Unbuffed, 540 Defense, 8% Hit Rating',
               Melee = '3.5k DPS, 8% Hit Rating, 26 Expertise',
               Ranged = '3.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
               Healer = '300 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
           },
           ['25 Normal'] = {
                GearScore = 3800,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '20k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '3.2k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '3.2k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '300 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },           
       },
       ['Eye of Eternity'] = {
            ['10 Normal'] = {
                GearScore = 4500,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '300 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Normal'] = {
                GearScore = 4000,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '3.8k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '3.8k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '300 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
       },
       ['Ulduar'] = {
           ['10 Easy'] = {
                GearScore = 4100,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
           },
           ['10 Hard'] = {
                GearScore = 5500,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '6.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '6.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
           },
           ['25 Easy'] = {
                GearScore = 3800,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
           },
           ['25 Hard'] = {
                GearScore = 5500,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '6.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '6.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
           },
       },
       ['Obsidian Sanctum'] = {
            ['10 0 Drakes'] = {
                GearScore = 4200,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 1 Drake'] = {
                GearScore = 4200,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 2 Drakes'] = {
                GearScore = 4700,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating 26 Expertise',
                Melee = '5.5k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '5.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 3 drakes'] = {
                GearScore = 4700,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating 26 Expertise',
                Melee = '7.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '7.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 0 Drakes'] = {
                GearScore = 4200,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 1 Drake'] = {
                GearScore = 4200,
                Gems = 'None',
                Enchants = 'None',
                Tanks = '25k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 2 Drakes'] = {
                GearScore = 5200,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating 26 Expertise',
                Melee = '5.5k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '5.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 3 drakes'] = {
                GearScore = 5500,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating 26 Expertise',
                Melee = '7.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '7.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            }            
       },
       ['Trail of the Crusader'] = {
            ['10 Normal'] = {
                GearScore = 4700,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Normal'] = {
                GearScore = 4900,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.2k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.2k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 Heroic'] = {
                GearScore = 5800,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '8.5k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '8.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Heroic'] = {
                GearScore = 5700,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '8.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '8.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
       },
       ['Onyxia'] = {
            ['10 Normal'] = {
                GearScore = 4500,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.2k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Normal'] = {
                GearScore = 4700,
                Gems = 'Some Rares',
                Enchants = 'Some Enchants',
                Tanks = '30k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '4.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '4.2k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '400 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            }, 
       },
       ['Icecrown Citadel'] = {
            ['10 Normal'] = {
                GearScore = 5400,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '6.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '6.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Normal'] = {
                GearScore = 5200,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '5.5k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '5.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 Heroic'] = {
                GearScore = 5700,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '42k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '8.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '8.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Heroic'] = {
                GearScore = 6000,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '42k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '9.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '9.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
       },
       ['Ruby Sanctum'] = {
            ['10 Normal'] = {
                GearScore = 5000,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '5.0k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '5.0k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Normal'] = {
                GearScore = 5200,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '35k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '5.3k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '5.3k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['10 Heroic'] = {
                GearScore = 5700,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '42k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '7.2k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '7.2k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
            ['25 Heroic'] = {
                GearScore = 5800,
                Gems = 'Full Epics',
                Enchants = 'Full Enchants + Rep',
                Tanks = '42k HP Unbuffed, 540 Defense, 8% Hit Rating',
                Melee = '7.5k DPS, 8% Hit Rating, 26 Expertise',
                Ranged = '7.5k DPS, 17% Hit Rating (8% for |cffABD473Hunters|r |TInterface\\WorldStateFrame\\ICONS-CLASSES:28:28:0:0:256:256:0:64:64:128|t)',
                Healer = '500 MP5 while casting (-20% for each mana regen trinket) OR |cffF58CBAHoly Paladins|r: 14k Mana',
            },
       },
    },
}