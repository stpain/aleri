--[==[
    Aleri

    All Rights Reserved 
    
    © stpain 2020
]==]--

local AleriName, Aleri = ...

Aleri.SDK = {}
Aleri.SDK.Util = {
    Print = function(msg, fontColour)
        if fontColour == nil then fontColour = '|cffffffff' end
        print(tostring(fontColour..AleriName..'|r: '..msg))
    end,
    Debug = function(msg)
        if Aleri.Debug == true then
            print(tostring('|cffC41F3B'..AleriName..' DEBUG: |r'..msg))
        end
    end,
    MakeFrameMove = function(frame)
        frame:SetMovable(true)
        frame:EnableMouse(true)
        frame:RegisterForDrag("LeftButton")
        frame:SetScript("OnDragStart", frame.StartMoving)
        frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
    end,
    GetArgs = function(...)
        for i=1, select("#", ...) do
            arg = select(i, ...)
            print(i.." "..tostring(arg))
        end
    end,
    RgbToPercent = function(t)
        if type(t) == 'table' then
            if type(t[1]) == 'number' and type(t[2]) == 'number' and type(t[3]) == 'number' then
                local r = tonumber(t[1] / 256.0)
                local g = tonumber(t[2] / 256.0)
                local b = tonumber(t[3] / 256.0)
                return {r, g, b}
            end
        end
    end,
}

Aleri.SDK.UI = {
    NewIcon = function(parent, name, anchor, x, y, w, h, texture, visible)
        if (parent == nil) or (name == nil) then Aleri.SDK.Util.Debug('creating button - parent or name missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') or (type(w) ~= 'number') or (type(h) ~= 'number') then Aleri.SDK.Util.Debug('creating icon - x,y,w or h not numbers') return end
        local f = CreateFrame('FRAME', name, parent)
        f:SetSize(w, h)
        f:SetPoint(anchor, x, y)
        f.Texture = f:CreateTexture('$parent_Texture', 'BACKGROUND')
        f.Texture:SetAllPoints(f)
        f.Texture:SetTexture(texture)
        if visible == true then
            f:Show()
        elseif visible == false then
            f:Hide()
        end
        return f
    end,
    NewButton = function(parent, name, anchor, x, y, w, h, text, inherits, visible)
        if (parent == nil) or (name == nil) or (inherits == nil) then Aleri.SDK.Util.Debug('creating button - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') or (type(w) ~= 'number') or (type(h) ~= 'number') then Aleri.SDK.Util.Debug('creating button - x,y,w or h not numbers') return end
        local b = CreateFrame('BUTTON', name, parent, inherits) -- 'UIPanelButtonTemplate')
        b:SetPoint(anchor, x, y)
        b:SetSize(w, h)
        b:SetText(text)
        if visible == true then
            b:Show()
        elseif visible == false then
            b:Hide()
        end
        return b
    end,
    NewFontString = function(parent, name, anchor, x, y, text, rgb, font, fontSize, inherits)
        if (parent == nil) or (name == nil) or (inherits == nil) then Aleri.SDK.Util.Debug('creating fontstring - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') then Aleri.SDK.Util.Debug('creating fontstring - x or y not numbers') return end
        local fs = parent:CreateFontString(name, 'OVERLAY', inherits)
        fs:SetPoint(anchor, x, y)
        fs:SetText(text)
        if rgb ~= nil then
            fs:SetTextColor(rgb.r, rgb.g, rgb.b)
        else
            fs:SetTextColor(1,1,1)
        end
        if (font ~= nil) and (fontSize ~= nil) then
            fs:SetFont(font, fontSize)
        end        
        return fs
    end,
    NewStatusBar = function(parent, name, anchor, x, y, w, h, rgb, minValue, maxValue, startValue)
        if (parent == nil) or (name == nil) then Aleri.SDK.Util.Debug('creating status bar - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') or (type(w) ~= 'number') or (type(h) ~= 'number') then Aleri.SDK.Util.Debug('creating status bar - x,y,w or h not numbers') return end
        local sb = CreateFrame("StatusBar", name, parent)
        sb:SetStatusBarTexture("Interface\\TargetingFrame\\UI-StatusBar")
        sb:GetStatusBarTexture():SetHorizTile(false)
        sb:SetMinMaxValues(minValue, maxValue)
        sb:SetValue(startValue)
        sb:SetSize(w, h)
        sb:SetPoint(anchor, x, y)
        sb:SetStatusBarColor(rgb.r, rgb.g, rgb.b)
        return sb
    end,
    NewSlider = function(parent, name, anchor, x, y, w, h, rgb, minValue, maxValue, startValue, stepValue)
        if (parent == nil) or (name == nil) or (inherits == nil) then Aleri.SDK.Util.Debug('creating slider - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') or (type(w) ~= 'number') or (type(h) ~= 'number') then Aleri.SDK.Util.Debug('creating slider - x,y,w or h not numbers') return end
        local slider = CreateFrame("Slider", name, parent, "OptionsSliderTemplate")
        --slider:SetBackdrop({bgFile = "Interface\\Buttons\\UI-SliderBar-Background", 
                                            -- edgeFile = "Interface\\Buttons\\UI-SliderBar-Border", 
                                            -- tile = true, tileSize = 8, edgeSize = 8, 
                                            -- insets = { left = 3, right = 3, top = 6, bottom = 6 }})
        slider:SetThumbTexture("Interface\\Buttons\\UI-SliderBar-Button-Horizontal")
        slider:SetSize(w, h)
        slider:SetOrientation('HORIZONTAL')
        slider:SetPoint(anchor, x, y)
        slider:SetValue(startValue)
        slider:SetMinMaxValues(minValue, maxValue)
        slider:SetValueStep(stepValue)
    end,
    RoleIcon = function(role)
        if (role ~= 'DPS') or (role ~= 'Healer') or (role ~= 'Tank') then Aleri.SDK.Util.Debug('role must be - DPS, Healer or Tank') return end
        local roles = {
            DPS = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:22:41|t",
            Healer = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:20:39:1:20|t",
            Tank = "|TInterface\\LFGFrame\\UI-LFG-ICON-PORTRAITROLES:16:16:0:0:64:64:0:19:22:41|t",
        }
        return tostring(roles[role])
    end,
}
